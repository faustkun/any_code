unit About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons,ShellApi;

type
  TForm2 = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    Label4: TLabel;
    Label5: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Label4MouseEnter(Sender: TObject);
    procedure Label4MouseLeave(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses Main;

{$R *.dfm}

procedure TForm2.BitBtn1Click(Sender: TObject);
begin
  Form2.Close;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  Label2.Caption:='������ '+Ver;
end;

procedure TForm2.Label4Click(Sender: TObject);
begin
  ShellExecute(handle,'open','mailto:4kirill@gmail.com?subject="MAsm32 Support"',nil,nil,SW_SHOW);
end;

procedure TForm2.Label4MouseEnter(Sender: TObject);
begin
  Label4.Font.Style:=[];
end;

procedure TForm2.Label4MouseLeave(Sender: TObject);
begin
  Label4.Font.Style:=[fsUnderline];
end;

end.
