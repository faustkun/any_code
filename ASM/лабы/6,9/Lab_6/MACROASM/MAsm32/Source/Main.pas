unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls, Buttons,masm,About,ShellApi;

type
  TForm1 = class(TForm)
    OpenDialog1: TOpenDialog;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Open1: TMenuItem;
    Exit1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    Memo1: TMemo;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    N1: TMenuItem;
    N2: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    { Private declarations }
    procedure AppClose;
  public
    { Public declarations }
  end;

const
  Ver: string = '0.1';

var
  Form1: TForm1;
  flFOpen: boolean;
  RdFile: TextFile;
  FilePath: string;

implementation

{$R *.dfm}

procedure TForm1.AppClose;
begin
  Close;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
  if (OpenDialog1.Execute) then
    Edit1.Text:=OpenDialog1.FileName;
end;

procedure TForm1.BitBtn3Click(Sender: TObject);
begin
  AppClose;
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
  AppClose;
end;

procedure TForm1.About1Click(Sender: TObject);
begin
  Form2.ShowModal;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Form1.Caption:='�������������� UUM32 v'+Ver;

  Memo1.Lines.Clear;
  Memo1.Lines.Add('����� ����������!');
  Memo1.Lines.Add('MAsm32 v'+Ver+' (c) kave, 2006');
  Memo1.Lines.Add('�� ������ ������� ����...');

  flFOpen:= false;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  Memo1.Lines.Clear;
  Memo1.Lines.Add('����� ����������!');
  Memo1.Lines.Add('Masm32 v'+Ver+' (c) kave, 2006');
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
var
  lab,com,op,cm,ln: string;
  code: byte;
begin
  {ln:='rdbuff macro &qwer,&asdf,&zxcv .rdbuf sdjkah kjs asd hk';
  code:=ExtractLine(ln,lab,com,op,cm);
  Memo1.Lines.Add(ln);
  Memo1.Lines.Add(lab);
  Memo1.Lines.Add(com);
  Memo1.Lines.Add(op);
  Memo1.Lines.Add(cm);
  Memo1.Lines.Add(IntToStr(code));}

  MacroStart;
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
begin
  if edit1.Text<>'' then
  begin
    FilePath:=Edit1.Text;
    Memo1.Lines.Add('');
    Memo1.Lines.Add('�������� ���� '+FilePath+'...');

    AssignFile(RdFile,FilePath);
    {$I-}
    Reset(RdFile);
    {$I+}

    if (IOResult=0) then
    begin
      Memo1.Lines.Append('OK');
      flFOpen:=true;
    end
    else
      Memo1.Lines.Append('������!');
  end
  else
    Memo1.Lines.Add('������ ������!');
end;

procedure TForm1.Open1Click(Sender: TObject);
begin
  SpeedButton1Click(self);
  BitBtn4Click(self);
end;

procedure TForm1.N2Click(Sender: TObject);
begin
  BitBtn2Click(self);
end;

end.
