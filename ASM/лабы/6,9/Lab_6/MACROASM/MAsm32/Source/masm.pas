unit masm;

interface

uses Dialogs,SysUtils,Classes,StrUtils;

  procedure MacroStart;
  procedure GetLine;
  procedure ProcessLine;
  procedure Define;
  procedure Expand;

  function ExtractArgs: boolean;
  function ExtractLine(sLine: string; var sLabel: string;
            var sCommand: string; var sOper: string; var sComment: string):byte;
  function NTFind(mn: string): Pointer;

type
  PDefTabRec = ^DefTabRec;
  DefTabRec = record
    MDef: TStrings;
  end;

  PNameTabRec = ^NameTabRec;
  NameTabRec = record
    MName: string [20];
    DefStart: PDefTabRec;
  end;

var
  flExpanding,
  flEnd: boolean;
  WrFile: TextFile;

  crLine,
  crLabel,
  crCommand,
  crOper,
  crComment: string;
  crCode: byte;

  NTRec,
  crNTRec: PNameTabRec;
  DTRec,
  crDTRec: PDefTabRec;
  NameTab: TList;

  DTRecIndex: integer;

  Args: array of string;

  //Counters
  CntStrings,
  CntMacros,
  CntMacroCalls: integer;

const
  Delimiters: string = Chr(32)+Chr(9);
  Comments: string = ';.';
  ArgSeparate: string = ',';

implementation

uses Main;

procedure MacroStart;
var
  WRPath: string;

begin
  flExpanding:=false;
  flEnd:=false;

  crLabel:='';
  crCommand:='';
  crOper:='';
  crComment:='';

  NameTab:=TList.Create;

  SetLength(Args,0);

  if flFOpen then
  begin
    WRPath:=FilePath+'.asm';
    AssignFile(WrFile,WRPath);
    {$I-}
    Rewrite(WrFile);
    {$I+}
    if IOResult=0 then
    begin
      while (not Eof(RDFile)) do
      begin
        GetLine;
        ProcessLine;
      end;
      Memo1.Lines.Add('');
      Memo1.Lines.Add('������ �������� ���� '+WRPath+'.');
      CloseFile(WrFile);
    end
    else
      Form1.Memo1.Lines.Add('�� ���� ������� ���� '+WRPath+'!')
  end
  else
    Form1.Memo1.Lines.Add('�� ������ ������� ����...');

  NameTab.Free;
  with Form1.Memo1.Lines do
  begin
    Add('');
    Add('���������� �����: '+IntToStr(CntStrings));
    Add('���������� ��������: '+IntToStr(CntMacros));
    Add('���������� ������������: '+IntToStr(CntMacroCalls));
    Add('������... ��');
  end;
end;

procedure GetLine;
var
  i: integer;
begin
  if flExpanding then
  begin
    crLine:='';
    crLabel:='';
    crCommand:='';
    crOper:='';
    crComment:='';

    crLine:=crDTRec^.MDef[DTRecIndex];
    for i:=0 to Length(Args)-1 do
    begin
      if AnsiContainsText(crLine,'?'+IntToStr(i)) then
          crLine:=AnsiReplaceText(crLine,'?'+IntToStr(i),Args[i]);
    end;
    Inc(DTRecIndex);
  end
  else
  begin
    crLine:='';
    crLabel:='';
    crCommand:='';
    crOper:='';
    crComment:='';

    ReadLn(RdFile,crLine);
    crCode:=ExtractLine(crLine,crLabel,crCommand,crOper,crComment);
    Inc(CntStrings);

    //if AnsiLowerCase(crLabel)='end' then flEnd:=true;
  end;
end;

procedure ProcessLine;
var
  pnt: PNameTabRec;

begin
  pnt:=NTFind(crCommand);

  if pnt<>nil then
  begin
    crNTRec:=pnt;
    crDTRec:=crNTRec^.DefStart;
    Expand;
  end
  else if (AnsiLowerCase(crCommand)='macro') then
    Define
  else
    WriteLn(WrFile,crLine);
end;

procedure Define;
var
  Level: byte;
  i: integer;
  flMend: boolean;

begin
  New(NTRec);
  NTRec^.MName:=crLabel;
  New(DTRec);
  DTRec^.MDef:=TStringList.Create;
  DTRec^.MDef.Append(crLabel+' '+crOper);

  ExtractArgs;
  flMend:=false;
  Level:=1;
  while (Level>0) do
  begin
    GetLine;

    if (crCode<4) then
    begin
      for i:=0 to Length(Args)-1 do
      begin
        if AnsiContainsText(crLine,Args[i]) then
          crLine:=AnsiReplaceText(crLine,Args[i],'?'+IntToStr(i));
      end;

      if AnsiLowerCase(crCommand)='macro' then
        Inc(Level) 
      else if AnsiLowerCase(crCommand)='mend' then
      begin
        flMend:=true;
        Dec(Level);
      end;

      if not flMend then DTRec^.MDef.Add(crLine);

    end;
  end;
  
  Inc(CntMacros);
  NTRec^.DefStart:=DTRec;
  NameTab.Add(NTRec);
//  Dispose(NTRec);
//  Dispose(DTRec);
end;

procedure Expand;

begin
  flExpanding:=true;
  DTRecIndex:=0;

  ExtractArgs;

  crLine:='.'+crLine;
  writeln(WRFile,crLine);
  Inc(DTRecIndex);
  while DTRecIndex<=crDTRec^.MDef.Count-1 do
  begin
    GetLine;
    ProcessLine;
  end;
  flExpanding:=false;
  Inc(CntMacroCalls);
end;

function ExtractArgs: boolean;
var
  i,n,k: integer;

begin
  i:=1;
  n:=0;
  Result:=false;

  SetLength(Args,0);

  if crOper<>'' then
  begin
    while i<=Length(crOper) do
    begin
      if IsDelimiter(ArgSeparate,crOper,i) then
        Inc(i)
      else
      begin
        k:=i;
        repeat
          Inc(i);
        until (i>Length(crOper)) or (IsDelimiter(ArgSeparate,crOper,i));
        SetLength(Args,n+1);
        Args[n]:=Copy(crOper,k,i-k);
        Inc(n);
      end;
    end;

    if Length(Args)>0 then
      Result:=true;
  end;
end;

function NTFind(mn: string): Pointer;
var
  i: integer;
  pnt: PNameTabRec;

begin
  Result:=nil;

  if NameTab.Count>0 then
    for i:=0 to NameTab.Count-1 do
    begin
      pnt:=NameTab.Items[i];
      if pnt^.MName=mn then
      begin
        Result:=pnt;
        Break;
      end;
    end;
end;

function ExtractLine(sLine: string; var sLabel: string;
          var sCommand: string; var sOper: string; var sComment: string): byte;
//Returning values:
//0 - string with label,command,operands and may be comments at the end
//1 - string with command and operands and may be comments at the end
//2 - string of comments
//3 - string with label/instruction and may be comments at the end
//4 - empty string (also string with delimiters only)
//5 - wrong string

var
  i,k,n: integer;
  Words: array of string;
  flComm: boolean;

begin
  i:=1;
  n:=0;
  flComm:=false;
  sLine:=TrimLeft(sLine);

  while (i<=Length(sLine)) do
  begin
    if IsDelimiter(Comments,sLine,i) then
    begin
      SetLength(Words,n+1);
      Words[n]:=Copy(sLine,i,Length(sLine)-i+1);
      flComm:=true;
      i:=Length(sLine)+1;
    end
    else if IsDelimiter(Delimiters,sLine,i) then
    begin
      Inc(i);
    end
    else
    begin
      k:=i;
      repeat
        Inc(i);
      until (IsDelimiter(Delimiters,sLine,i)) or (i>Length(sLine)) or (IsDelimiter(Comments,sLine,i));
      SetLength(Words,n+1);
      Words[n]:=Copy(sLine,k,i-k);
      Inc(n);
    end;
  end;

  if flComm then
    case Length(Words) of
    1:
    begin
      sComment:=Words[0];
      Result:=2;
    end;
    2:
    begin
      sCommand:=Words[0];
      sComment:=Words[1];
      Result:=3;
    end;
    3:
    begin
      sCommand:=Words[0];
      sOper:=Words[1];
      sComment:=Words[2];
      Result:=1;
    end;
    4:
    begin
      sLabel:=Words[0];
      sCommand:=Words[1];
      sOper:=Words[2];
      sComment:=Words[3];
      Result:=0;
    end;
    else
      Result:=5
    end
  else
    case Length(Words) of
    0:
    begin
      Result:=4;
    end;
    1:
    begin
      sCommand:=Words[0];
      Result:=3;
    end;
    2:
    begin
      sCommand:=Words[0];
      sOper:=Words[1];
      Result:=1;
    end;
    3:
    begin
      sLabel:=Words[0];
      sCommand:=Words[1];
      sOper:=Words[2];
      Result:=0;
    end;
    else
      Result:=5
    end;
end;

end.
