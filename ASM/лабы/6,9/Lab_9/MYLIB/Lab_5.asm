                             ;               ???????????? ?????? ?4
;              ????????? ????? ???-1-10
;		(????? ????????????? ????????? ???????)
lab4    start   0
		extref	vivod,obrmas,choice,getmas
		extdef	mass,head,dhead,temp,kon1,dkon1,kon4,dkon4,raz
		
		;??????? ?????
		ldt		#head
		ldb		#dhead
		jsub	vivod

run1	clear	a
		clear	b
		clear	s
		clear	x
		clear	l
		clear	t
		
		ldt		#vvm
		ldb		#dvvm
		jsub	vivod

		jsub	getmas
		
		lda		#10
		wd		#0
		lda		#13
		wd		#0
		
		ldt		#outm
		ldb		#doutm
		jsub	vivod
		
		jsub	obrmas
		;?????? ? ????????
		lda		#10
		wd		#0
		lda		#13
		wd		#0
		jsub	choice
		lda		#10
		wd		#0
		lda		#13
		wd		#0
		
		ldt		#endl
		ldb		#dendl
		jsub	vivod
		
run2	rd		#248
		comp	#'Y'
		jeq		run1
		comp	#'y'
		jeq		run1
		comp	#'N'
		jeq		fin
		comp	#'n'
		jeq		fin
		j		run2
		
fin		+j		#x'FFFF'

;----------------------------------------------------------------

head	byte	'Lab_0',10,13,'Afanacev Anton ',10,13,'ITO-1-10',10,13,0
dhead	equ		*-head
kon1	byte	'Net polojitelnih eltmentov',10,13,0
dkon1	equ		*-kon1
kon2	byte	'Oshibka programmi',10,13,0
dkon2	equ		*-kon2
kon3	byte	'Summa polojitelnih >9',10,13,0
dkon3	equ		*-kon3
kon4	byte	'Summa polojitelnih =',0
dkon4	equ		*-kon4
vvm		byte	'Vvedite	massiv:',10,13,0
dvvm	equ		*-vvm
endl	byte	'Povtorim Y/N',10,13,0
dendl	equ		*-endl
outm	byte	'itogoviy massiv:',10,13,0
doutm	equ		*-outm
oldb	word	0
olda	word	0
oldx	word	0
oldt	word	0
olds	word	0
str    byte   0,0,0,0,0

raz		word	0
shift	word	48
nine	word	9
tree	word	3
temp 	word 	0
leng	word	0
res		word 	0		
nil		word	0
one 	word  	1		
Mass 	resw   	100
znak	word	0
num1	word	0



;----------------------------------------------------------------

vivod	csect
        clear	a
        clear	x
		addr	t,b
		rmo	x,t
		
str1	ldch	0,x
		wd	#0
		tixr	b
		jlt	str1
		+rsub	

;----------------------------------------------------------------
;????????? ???????
obrmas	csect
		;ldx		raz
		
		stl		oldl
		clear		b
		clear       t
		clear       x
		ldt		#2
		
		extref	mass,raz,intout
main2	stb		temp
         
		ldb		mass,X;?????
		jsub	        intout
		lda		#32
		wd		#0
		ldb		temp
		lda		mass,X
		comp	#0
		jgt		plus
		j		prover

plus	addr	a,b
		j		prover

prover	addr	t,x
		tix		raz
		jlt		main2
		lda		#0
		ldl		oldl
		+rsub
temp 	word	0
oldl	word	0	
;----------------------------------------------------------------
;????? ????????????? ?????
intout  csect

		sta		olda
		stb		oldb       
        stx    oldx
        sts    olds
        stt    oldt
        clear  	x
		clear	a
		;clear	b
		clear	t
		clear	s
		
        ldt    #10 
        rmo    a,b
        comp   #0
        jlt    neg
        j      listart
neg     clear  b
        subr   a,b
        lda    #45
        wd     x'01'
        j      listart
        
lintout tix    #0
listart rmo    a,b
        divr   t,b
        mulr   t,b
        subr   b,a
        lds    #48
        addr   s,a
        stch   str,x       
        divr   t,b
        lds    #0
        compr  b,s
        jgt    lintout
        lds    #-1
        ldt    #0
liprint ldch   str,x
        wd     x'01'
        addr   s,x
        compr  x,t
        jlt    liexit
        j      liprint
liexit  ldx    oldx
        lds    olds
        ldt    oldt
        ;clear 	b
        ldb	oldb
		lda	olda
        
		+rsub

oldb	word	0
olda	word	0
oldx	word	0
oldt	word	0
olds	word	0
str    byte   0,0,0,0,0
;----------------------------------------------------------------
choice	csect
		extref	kon1,dkon1,kon4,dkon4,intout
		stb		temp
		lda 	temp
		comp	#0
		ldx		#0
		lds		#dkon1
		jeq		case1; ���� ����� ����
		lds		#dkon4
		j		case4; ���� ������ ������
		
		case1	lda		#0
		ldch	kon1,X; ��� �������������
		wd		#0
		tixr	S
		jlt		case1
		+rsub
		
		case4	lda		#0
		ldch	kon4,X; �� ��
		wd		#0
		tixr	S
		jlt		case4

case4_1	lda		#0
		lda 	temp
		add 	#48
		stl		oldl
		jsub	intout
		ldl		oldl
		+rsub
		
temp	word	0	
oldl	word	0

;----------------------------------------------------------------     
getmas	csect
		stl		vgl
		clear	x
		extref	raz,toarr
		extdef	num1,znak

g5		clear	s
		clear	t

		lda		#1
		sta		znak
		clear	a
		sta		num1

g2		clear	a
		rd		#248
		
		comp	#'-'         
		jeq		g1

		comp	#' '        
		jeq		g3
		
		comp	#13         
		jeq		g4
		
		comp	#'0'
		jlt		g2
		comp	#'9'
		jgt		g2
		
		lds		#2
		compr	t,s
		jeq		g2
		
		comp	#'0'
		jgt		g6
		clear	s
		compr	t,s
		jeq		g7
		
g6		ldt		#1
		
		sub		#48        
		rmo		s,a
		lda		num1
		rmo		b,a
		mul		#10
		addr	s,a
		compr	a,b
		jlt		g2
		sta		num1
		rmo		a,s
		add		#48
		wd		#0
		j		g2
		
g7		lda		znak
		comp	#0
		jlt		g2
		ldt		#2
		lda		#'0'
		wd		#0
		j		g2
		
g1		lda		znak
		comp	#0
		jlt		g2          
		clear	s
		compr	t,s
		jgt		g2          
		lda		#-1
		sta		znak
		lda		#'-'
		wd		#0
		j		g2
		
g3		clear	s
		compr	t,s
		jeq		g2        
		wd		#0
		jsub	toarr
		j		g5
		
g4		clear	s
		compr	t,s
		jeq		g2
		jsub	toarr
		stx		raz
		
		ldl		vgl
		+rsub
znak	word	1
num1	word	0
vgl		word	0
;----------------------------------------------------------------
toarr	csect                                                    
		extref	mass,znak,num1                                   
		clear	a                                                
		lds		znak                                             
		ldt		num1                                             
		mulr	s,t                                              
		stt		mass,x                                           
		tixr	a                                                
		tixr	a                                                
		tixr	a                                                
                                                                 
		+rsub                                                    
;----------------------------------------------------------------

