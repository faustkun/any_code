otrel	start	0

prints    macro  &string

;------����� �뢮�� ��ப� �� �����--------
          ldx   &string
aw        ldch  0,x
          comp  #0
          jeq   as
          wd    out 
          tix   #0
          j     aw
as        mend   



clrscr    macro
;------����� ���⪨ ��࠭�-----------
         
clrscr1   ldx   #0
          ldch  #13
          wd    out
          ldch  #10
          wd    out
begclr    ldch  #32
          wd    out
          tix   #2000
          jeq   endclr
          j     begclr
endclr    mend  


outsym    macro  &sm
;-------����� �뢮�� ��ப� ᨬ�����---------
          
outsym1   lda   &sm
          sta   iks
          ldx   #0
b1        ldch  iks+2
          wd    out
          tix   #78
          jeq   e1
          j     b1
e1        mend 



nextln    macro
;-------����� ���室� �� ����� ��ப�--------
         
nextln1   ldch  #13
          wd    out
          ldch  #10
          wd    out   
          mend



vivod     macro  &xxx
;-------����� �뢮�� ��������筮�� �᫠----------
vivod1    lda   &xxx
          sta   chislo   ;
          comp  #0       ;
          jeq   ifzero   ;
          j     xx       ;
ifzero    ldch  #'0'     ;
          wd    out      ;
          j     cycend   ;
xx        lda   chislo   ;
          comp  #0       ;
          jlt   ifnuml   ;
          j     ifnumb   ;
ifnuml    mul   #-1      ; �஢�ઠ �� ����⥫쭮� �᫮ � ����
          sta   chislo   ;
          ldch  #'-'     ;
          wd    out      ;

ifnumb    lda   #0
          sta   nzero     ;����塞 䫠� ������ �������. ����
          sta   rang      ;����塞 ����稪 ������⢠ �뢥������ ���
vyvcyc    lda   chislo       
          div   status    ;�᫮ ����� �� ���冷�
          sta   nch       ;���������� ���� 
          
          comp  #0        ;
          jgt   ifnchb    ;
          j     ifnch0    ;
ifnchb    lda   #1        ;
          sta   nzero     ;�஢�ઠ ���� �� 0. �᫨ ������騩 ���� 
          j     toekr     ;��। �᫮� (0034), � �� �뢮��� �� ��࠭
                          ;�᫨ ���� ����� �᫠, ���ਬ�� 2035, �
ifnch0    lda   nzero     ;�뢮���.
          comp  #0        ;
          jgt   toekr     ;
          j     prodol    ;

toekr     lda   nch       ;
          add   #48       ;�����।�⢥��� �뢮� �� ��࠭
          wd    out       ;

prodol    lda   rang      ;
          add   #1        ;�����稢��� ����稪 ���-�� �뢥������
          sta   rang      ;�� ��࠭ �ᥫ

          lda   nch       ;
          mul   status    ;
          sta   nch       ;
          lda   chislo    ;��楤���, �����. ��� �뤥����� ᫥���饩 ����
          sub   nch       ;
          mul   #10       ;
          sta   chislo    ;
         
          lda   rang      ;
          comp  #5        ;
          jeq   cycend    ;�஢�ઠ �� �, �� �뢥���� �� ����騥 
          j     vyvcyc    ;���� �᫠
cycend    mend            ;
;-------����� �뢮�� ��������筮�� �᫠----------


;-----------------------------------------------------------
;  
;               ������� �ணࠬ��
;
;-----------------------------------------------------------


     

begin	stl	retadr

          clrscr
          nextln   
          prints  #str1
nextr     nextln
          outsym  #196  
          prints  #str4
          nextln
          prints  #str3

          ldx   #0
          lda   #0
zer       sta   massiv,x
          tix   #0
          tix   #0
          tix   #60
          jgt   input
          j     zer


;------���� � ����������------
input     ldx   #0
          
          stx   chislo
          lda   #1
          sta   minus
          sta   iks
dalshe    lda   #0
          rd    inp
          sta   ks
          comp  #13
          jeq   dalee
          comp  #44
          jeq   zpt
          j     aftzpt 
zpt       lds   #1
          sts   minus

          tix   #0
          tix   #0
          tix   #60
          lda   iks
          add   #1
          sta   iks
          lda   #0
          sta   chislo
          
          j     dalshe
aftzpt    lda   ks
          comp  #45
          jeq   ifminu
          j     ifnmin 
ifminu    lds   #-1
          sts   minus
          j     dalshe
ifnmin    comp  #48
          jlt   dalshe
          lda   ks
          comp  #57
          jgt   dalshe
          sub   #48
          add   chislo
          mul   minus 
          sta   massiv,x
          mul   minus
          mul   #10
          sta   chislo 
          jeq   dalee 
          j     dalshe

dalee     nextln 
          prints  #str2 
          
;---������� ���-�� �������. ��-⮢ ���ᨢ�----
          lda   iks
          mul   #3
          sub   #3
          sta   lenmas 
          
          lda   #0
	ldx   #0
          sta   rez
         
cycle     lda   massiv,x
          comp  #0
          jgt   increm
          j     nextx

increm    clear a
          lds   rez
          lda   #1
          addr  s,a 
          sta   rez

nextx     tix   #0
          tix   #0
          tix   lenmas
          jgt   p
          j     cycle
         
         
p         vivod  rez  

;---------������騩 ࠧ--------

          nextln
          prints  #str5
          rd     inp
          comp  #13
          jeq   nextr
          


          nextln
          outsym  #205
          outsym  #32  

kon     	ldl   retadr   
          rsub            ; ����� �ணࠬ��

rang      resw      3
minus     resw      3
nzero     resw      3
lenmas    resw      3
status    word      10000
retadr	word      0
;massiv	word      4,5,6,7,6,-8,7,9,6,5,3,0,12,2,1,45,23,18,19  ;massiv(17 �� 19)
;massiv	word      4,5,0,7,6,-8,7,9,-6,5,3,7,12,2,1  ;massiv (12 �� 15)
;massiv	word      -4,-5,-6,0,-6,-8,1,1,2,2 ;massiv (0 �� 6)
massiv	word      4,5,6,7,6,8,78,9,6,5,1,68,12,2,1,45,23,18,19,8  ;massiv(20 �� 20)
tri	word	3
iks       resw      3
nch       resw      3
ks        resw      3 
rez	resw	3
chislo    resw      3    
out	byte	x'01'
inp       byte      x'00'
str1	byte  c'   ������ୠ� ࠡ�� N3 ��㤥�� ����㪮�� �. ��㯯� ��-2-99, ��ਠ�� 2.'
	byte  0
str2	byte  c'������⢮ ������⥫��� ������⮢ ���ᨢ� '
	byte  0
str3	byte  c'������ �१ ������� �������� ���ᨢ�: '
	byte  0
str4	byte  c'                              (�⮡� �������� ���� ������ ������� ENTER)'
	byte  0
str5	byte  c'������ ENTER �⮡� ����� ��㣮� ���ᨢ, �� ��㣠� ������ - ��室'
	byte  0

	end	begin
