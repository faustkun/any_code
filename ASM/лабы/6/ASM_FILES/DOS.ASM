uumdos	start   0A000h
base	off	; �⪫���� ������ �� ���� ��-㬮�砭��

. ��।��塞 ��饤���㯭� ��楤���

	lda	#Exec         
	+sta	0F000h       ; F000h - ������� �ணࠬ�� 
                             ; B - ���� ASCIIZ-��ப� ����� 䠩��
	                     ; ���祭�� ��� ॣ���஬ ����� ����������!!!

	lda	#Read
	+sta	0F003h	     ; F003h - ���ਧ�஢���� ���� � ����������
	                     ; ASCIIZ ��ப�. B - ���� (S+1) ᨬ���쭮��
			     ; ����

	lda	#write
	+sta	0F006h       ; F006h - �뢮� ASCIIZ ��ન �� ��࠭
	                     ; B - ���� ASCIIZ ��ப�

	ldb	#CoRi
	stl	ret
	jsub	write

. ����� ࠡ���

work1	ldch	#3           ;  ������� ⥪�騩 ��⠫��
	wd	#250
	ldch	#6
	wd	#249
	clear	x
	clear	a
	wd	#254
work2	rd	#255         ; ����� ��� � ���ன�⢠ � �뢥�� ��� �� ��࠭
	comp	#0
	jeq	work3
	wd	#1
	j	work2

work3	ldch	#'>'         ; �뢥�� ���� ">"
	wd	#1

	ldb	#bufer       ; �맢��� ��楤��� �⥭��, ����ந� ॣ�����
	lds	#76	
	jsub	read
	ldch    #13
	wd	#1
	ldch	#10
	wd	#1

	clear	x            ; �������� ���� �㪢� ����訬�
work4	ldch	bufer,x
	comp	#0
	jeq	work5
	comp	#'a'
	jlt	work41
	comp	#'z'
	jgt	work41
	sub	#'a'-'A'
	stch	bufer,x
work41	tix	#76
	j	work4

work5	lda	#3
	sta	count
	clear	x
	clear	b

work6	ldch	coms,x,b
	rmo	s,a
	ldch	bufer,x
	compr	s,a
	jeq	work7

	lda	count
	sub	#1
	comp	#0
	jeq	work8
        sta     count
	rmo	a,b
	add	#6
	rmo	b,a
	clear	x
	j	work6

work7	comp	#0
	jeq	work9
	tix	#6
	jlt	work6

work9	lda	count
	comp	#1
	jgt	work91      ; Exit
	ldl	ret
	rsub

work91	comp	#2
	jgt	work92      ; DIR
        ldch    #3fh
        wd      #251
        clear   a
        wd	#254
        ldch    #'*'
	wd	#255
	ldch    #'.'
	wd	#255
	ldch	#'*'
	wd	#255
	clear	a
	wd	#255
        wd	#252
	lda	#Table
        ldx	#7
        wd	#253
        shiftr	a,x
        wd	#253
	lda	#7
dir1	wd	#249
	rd      #249
	comp	#0
	jgt	work1
	ldx	#1Eh
dir3	ldch    Table,x
	wd	#1
	comp	#0
	jeq	dir4
	tix	#0
	j	dir3
dir4	ldch	#13
	wd	#1
	ldch	#10
	wd	#1
	
	ldch	#8
	j	dir1        

work92	j	work1       ; Type



work8	ldb	#bufer
	jsub	exec
	j	work1
	
CoRi	byte	'UUM OS  Version 1.00  (C) Evstigneev D.W.',13,10,10,10,0
ret	resw	1

count	resw	1
coms	byte	'TYPE', 0,0
	byte	'DIR',0,0,0
	byte	'EXIT', 0,0

Table   resb    43




Exec  ; ��楤�� ����᪠ UUM-䠩��.
	���� �맮�� F000h
	���� ASCIIZ-��ப� ����� ����᪠����� 䠩��: B
	clear	x
	clear	a
        clear   s
        wd      #251
	stch	exext
	wd	#254
ex1	ldch	0,B,X
        wd      #255
        comp    #0
        jeq     ex4
        comp    #'.'
        jeq     ex2
ex3     tix     #127
        jlt     ex1

ex4     ldch    exext
        comp    #1
        jeq     ex5
        rd      #254
        sub     #1
        wd      #254
        ldch    #'.'
        wd      #255
        ldch    #'u'
        wd      #255
        wd      #255
        ldch    #'m'
        wd      #255
        clear   a
        wd      #255

ex5     lda     #1
        clear   x
        wd      #249
        rd      #249
        comp    #0
        jeq     ex6
; ���� �� ������
ex7     ldch    exer1,x
        wd      #1
        tix     #exer1l
        jlt     ex7
        rsub
exer1   byte    'Bad command or file name',10,13
exer1l  equ     *-exer1

ex6     rd      #251
        stch    discr
ex8     td      discr
        jeq     ex9
        rd      discr
        stch    stAddr,x
        tix     #6
        jlt     ex8

ex10    td      discr
        jeq     ex11
        rd      discr
        stch    @StAddr
        lda     StAddr
        add     #1
        sta     StAddr
        j       ex10

ex11    ldch    #2
        wd      #249
	stl	retAdr
	jsub	@EnAddr
	ldl	retadr
	ldch	#13
	wd	#1
	ldch	#10
	wd	#1
	rsub


ex2     lda     #1
        stch    exext
        j       ex3
ex9     ldch    #2
        wd      #249
        clear   x
ex91    ldch    exer2,x
        wd      #1
        tix     #exer2l
        jlt     ex91
        rsub

exer2   byte    'Error in UUM-file',10,13
exer2l  equ     *-exer2

exext	resb	1
Discr   resb    1
StAddr  resw    1
EnAddr  resw    1
RetAdr  resw    1

Stack1	resw	1
Stack2	resw	1
stack3	resw	1


Read	;  ��楤�� �⥭�� ��ப�
        sta	stack1
	stx	stack2
	clear	a
	clear	x
rd1	rd	#248
	comp	#8
	jeq	rd2

	comp	#0
	jeq	rd3

	comp	#13
	jeq	rd4
	
	comp	#31
	jlt	rd1
	wd	#1
	stch	0,b,x

	tixr	s
	jlt	rd1
	ldch	#7
	wd	#1
	rmo	x,s
	j	rd1
rd2	rmo	a,x
	comp	#0
	jeq	rd1
	sub	#1
	rmo	x,a

        ldch    #8
        wd      #1
	ldch	#32
	wd	#1
	ldch	#8
	wd	#1
	j	rd1
rd3	rd	#248
	j	rd1
rd4	clear	a
	stch	0,b,x
	lda	stack1
	ldx	stack2
	rsub
	


Write	sta	stack1
	stx	stack2
	clear	x
	clear	a
w1	ldch	0,b,x
	comp	#0
	jeq	w2
	wd	#1
	tix	#7FFh
	jlt	w1
w2	lda	stack1
	ldx	stack2
	rsub	



bufer	resb	77
	end