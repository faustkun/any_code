        . ���� ������ ��᫥����� ����⥫쭮��  ������� ���ᨢ�
          C�㤥�� ZAYCTHENKO V. PMW-1-00

        SCREEN  EQU     x'01'
        ASCII   EQU     x'30'
        MSIZE   EQU     45
        MARKER  EQU     -1

        pl3     START   0
                rmo     b,l

                ldx     #TITLE
                jsub    PRINTS

                clear   x
                lds     #MARKER
        LAB_1   lda     MASSIV,x
                comp    #-1
                jgt     LAB_2
                rmo     s,x
        LAB_2   jsub    PRINTN
                ldch    #c' '
                wd      #SCREEN
                wd      #SCREEN
                tixr    x
                tixr    x
                tix     #MSIZE
                jlt     LAB_1

                ldx     #MESS1
                jsub    PRINTS

                rmo     a,s
                comp    #MARKER
                jeq     LAB_3
                div     #3
                add     #1
                jsub    PRINTN
                j       LAB_4

        LAB_3   ldx     #MESS2
                jsub    PRINTS
        LAB_4
                rmo     l,b
                ldx     #MESS3
                j       PRINTS
       
     ;   �தᥤ�� ���� ��ப� 
      ;             PRINTS          
       
      ;   INPUT REG X = STRING PTR
       ;  OUTPUT DEVICE = SCREEN  
        ;      USES REG A, X      
         ;END CODE = '0' ( ZERO ) 
       
        PRINTS  sta     SSAVEA
                stx     SSAVEX

                clear   a
        PR_L    ldch    0,x
                comp    #0
                jeq     PR_OUT
                wd      #SCREEN
                tixr    x
                j       PR_L

        PR_OUT  lda     SSAVEA
                ldx     SSAVEX
          rsub
        SSAVEA  WORD    0
        SSAVEX  WORD    0
      ;-----------------------------

       
       ;  �தᥤ�� ���� �᫠  
        ;          PRINTN          
                                  
        ; INPUT REG A = � INTNUMBER 
         ; OUTPUT DEVICE = SCREEN   
          ;  USES REG A, S, X, T    
       
        PRINTN  sta     NSAVEA
                sts     NSAVES
                stt     NSAVET
                stx     NSAVEX

                comp    #-1
                jgt     PN_1
                rmo     s,a
                ldch    #c'-'
                wd      #SCREEN
                clear   a
                subr    s,a
	
        PN_1    clear   x
                sta     DIVS,x
                j       PN_21
        PN_2
                div     #10
                sta     DIVS,x
                comp    #0
                jeq     PN_3
        PN_21   tixr    x
                tixr    x
                tixr    x
                j       PN_2

        PN_3    lds     #4
        PN_4    lda     DIVS,x
                mul     #10
                rmo     t,a
                subr    s,x
                tix     #0
                jlt     PN_5
                lda     DIVS,x
                subr    t,a
                add     #ASCII
                wd      #SCREEN
                j       PN_4

        PN_5    lda     NSAVEA
                lds     NSAVES
                ldt     NSAVET
                ldx     NSAVEX

                rsub

        DIVS    RESW    9
        NSAVEA  WORD    0
        NSAVES  WORD    0
        NSAVET  WORD    0
        NSAVEX  WORD    0
       ;---------------------------------

        TITLE   BYTE    c'������ୠ� ࠡ�� �3 '
                BYTE    c'��㤥�� ��㯯� ��-2-98 '
                BYTE    c'���� �.�.',13,10,10
                BYTE    c'���ᨢ : ',0
        MESS1   BYTE    13,10,c'����� ��᫥����� ����⥫쭮�� ������� : ',0
        MESS2   BYTE    7,c'����� ������� ���������.',0
        MESS3   BYTE    13,10,0

        MASSIV  WORD    -505,41,79,49,-5,28,3,9,5,1024,84,45,-13000

                END     pl3

