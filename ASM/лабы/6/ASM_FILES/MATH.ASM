test    start   0
                extref  exp,P10,ln,lg,sh,ch,th,cth
test    stl     retadr
        ldf     x
        jsub    exp
        ldl     retadr
        rsub
x       dword   1.0
retadr  resw    1


.�㭪�� EXP x
.�室 : ������p F - x , ��室 : ������p F - exp(x)
exp     csect
                extref  int,frc,abs,_th
exp     stl     retadr
        divf    ln10  . ����塞 ��p冷� �� �p�㫥 INT(x/ln 10)
        stf     arg
        jsub    int
        stf     por
        ldf     arg   . ����塞 ������ �� �p�㫥 x' = FRC(x/ln 10) * ln 10
        jsub    frc
        mulf    ln10
        stf     arg
        jsub    abs
        compf   c1    . ��� ����襭�� �筮�� p������ ���᫥��� ���p���
        jgt     m1    . ��������� �p�㬥�� x' [0; ln 10] p���������� �� ���
        ldf     arg   . ����� ���p���� [0; 1] � [1; ln 10]
        divf    c2    . H� ��p��� ���p���� ���祭�� ������� �㭪樨 ��p�����-
        jsub    _th   . ���� �� �p�㫥 M = (1 + th(x'/2))/(1 - th(x'/2))
        stf     arg
        ldf     c1
        subf    arg
        stf     temp
        ldf     c1
        addf    arg
        divf    temp
        j       m2
m1      ldf     ln10  . H� ��p�� ���p���� - �� �p�㫥 M = 10/e^(ln 10 - x') =
        subf    arg   . = 10(1 - th((ln 10 - x')/2))/(1 + th((ln 10 - x')/2))
        divf    c2
        jsub    _th
        stf     arg
        addf    c1
        stf     temp
        ldf     c1
        subf    arg
        mulf    c3
        divf    temp
m2      stf     arg   . ��⠭�������� ��p冷�
        ldf     por
        compf   c4
        jlt     m3
        subf    c1
        stf     por
        ldf     arg
        mulf    c3
        j       m2
m3      jeq     m4
        addf    c1
        stf     por
        ldf     arg
        divf    c3
        stf     arg
        ldf     por
        compf   c4
        j       m3
m4      ldf     arg
        ldl     retadr
        rsub

temp    resw    2
arg     resw    2
por     resw    2     . ��p冷� p������
ln10    dword   2.302585092994
c1      dword   1.0
c2      dword   2.0
c3      dword   10.0
c4      dword   0.0
retadr  resw    1

.�㭪�� p10 x
.������ 10^x
.�室 : ������p F - x , ��室 : ������p F - P10(x)
P10      csect
                extref  int,frc,abs,_th
P10     stl     retadr
        stf     arg   . ����塞 ��p冷� �� �p�㫥 INT(x)
        jsub    int
        stf     por
        ldf     arg   . ����塞 ������ �� �p�㫥 x' = FRC(x) * ln 10
        jsub    frc
        mulf    ln10
        stf     arg
        jsub    abs
        compf   c1    . ��� ����襭�� �筮�� p������ ���᫥��� ���p���
        jgt     m1    . ��������� �p�㬥�� x' [0; ln 10] p���������� �� ���
        ldf     arg   . ����� ���p���� [0; 1] � [1; ln 10]
        divf    c2    . H� ��p��� ���p���� ���祭�� ������� �㭪樨 ��p�����-
        jsub    _th   . ���� �� �p�㫥 M = (1 + th(x'/2))/(1 - th(x'/2))
        stf     arg
        ldf     c1
        subf    arg
        stf     temp
        ldf     c1
        addf    arg
        divf    temp
        j       m2
m1      ldf     ln10  . H� ��p�� ���p���� - �� �p�㫥 M = 10/e^(ln 10 - x') =
        subf    arg   . = 10(1 - th((ln 10 - x')/2))/(1 + th((ln 10 - x')/2))
        divf    c2
        jsub    _th
        stf     arg
        addf    c1
        stf     temp
        ldf     c1
        subf    arg
        mulf    c3
        divf    temp
m2      stf     arg   . ��⠭�������� ��p冷�
        ldf     por
        compf   c4
        jlt     m3
        subf    c1
        stf     por
        ldf     arg
        mulf    c3
        j       m2
m3      jeq     m4
        addf    c1
        stf     por
        ldf     arg
        divf    c3
        stf     arg
        ldf     por
        compf   c4
        j       m3
m4      ldf     arg
        ldl     retadr
        rsub

temp    resw    2
arg     resw    2
por     resw    2     . ��p冷� p������
ln10    dword   2.302585092994
c1      dword   1.0
c2      dword   2.0
c3      dword   10.0
c4      dword   0.0
retadr  resw    1

.�㭪�� LN x
.�室 : ������p F - x , ��室 : ������p F - ln(x)
ln      csect
                extref  int,frc,abs,arth
ln      stl     retadr
. ����p�� �� ����� ���� ��p��⥫��
        stf     arg   . ��p����塞 ��p冷� �᫠
        ldf     c4
        stf     por
        ldf     arg
        compf   c1
        jlt     m1
m3      ldf     arg
        compf   c3
        jlt     m2
        divf    c3
        stf     arg
        ldf     por
        addf    c1
        stf     por
        j       m3
m1      ldf     arg
        compf   c1
        jgt     m2
        mulf    c3
        stf     arg
        ldf     por
        subf    c1
        stf     por
        j       m1
m2      compf   c5    . ��� ����襭�� �筮�� p������ ���᫥��� ���p���
        jgt     m4    . ��������� �p�㬥�� [1; 10) p���������� �� ���
        addf    c1    . ����� ���p���� [1; 3.1975309] � [3.197531; 10)
        stf     temp  . H� ��p��� ���p���� �㭪�� ln x �������� �� �p�㫥
        ldf     arg   . ln x = 2 arth ((x - 1)/(x + 1))
        subf    c1
        divf    temp
        jsub    arth
        mulf    c2
        j       m5
m4      addf    c3    . H� ��p�� ���p���� - �� �p�㫥
        stf     temp  . ln x = ln 10 - ln(10/x) = ln 10 - 2 arth((10-x)/(10+x))
        ldf     c3
        subf    arg
        divf    temp
        jsub    arth
        mulf    c2
        stf     temp
        ldf     ln10
        subf    temp
m5      stf     arg
        ldf     por   . ������塞 ��p冷�
        mulf    ln10
        addf    arg
        ldl     retadr
        rsub

temp    resw    2
arg     resw    2
por     resw    2     . ��p冷� p������
ln10    dword   2.302585092994
c1      dword   1.0
c2      dword   2.0
c3      dword   10.0
c4      dword   0.0
c5      dword   3.1975309
retadr  resw    1

.�㭪�� LG x
.�室 : ������p F - x , ��室 : ������p F - lg(x)
lg      csect
                extref  ln
lg      stl     retadr
. ����p�� ������� lg x = ln x/ ln 10
        jsub    ln
        divf    ln10
        ldl     retadr
        rsub
ln10    dword   2.302585092994
retadr  resw    1

.�㭪�� SH x
.����p�����᪨� ᨭ��
.�室 : ������p F - x , ��室 : ������p F - Sh(x)
sh      csect
                extref  exp
sh      stl     retadr
        stf     arg
        ldf     c1
        subf    arg
        jsub    exp
        stf     temp
        ldf     arg
        jsub    exp
        subf    temp
        divf    c2
        ldl     retadr
        rsub
temp    resw    2
arg     resw    2
c1      dword   0.0
c2      dword   2.0
retadr  resw    1

.�㭪�� CH x
.����p�����᪨� ��ᨭ��
.�室 : ������p F - x , ��室 : ������p F - Ch(x)
ch      csect
                extref  exp
ch      stl     retadr
        stf     arg
        ldf     c1
        subf    arg
        jsub    exp
        stf     temp
        ldf     arg
        jsub    exp
        addf    temp
        divf    c2
        ldl     retadr
        rsub
temp    resw    2
arg     resw    2
c1      dword   0.0
c2      dword   2.0
retadr  resw    1

.�㭪�� TH x
.����p�����᪨� ⠭����
.�室 : ������p F - x , ��室 : ������p F - Th(x)
th      csect
                extref  sh,ch
th      stl     retadr  . th = sh/ch
        stf     arg
        jsub    ch
        stf     temp
        ldf     arg
        jsub    sh
        divf    temp
        ldl     retadr
        rsub
arg     resw    2
temp    resw    2
retadr  resw    1

.�㭪�� CTH x
.����p�����᪨� ��⠭����
.�室 : ������p F - x , ��室 : ������p F - Cth(x)
cth     csect
                extref  sh,ch
cth     stl     retadr  . cth = ch/sh
        stf     arg
        jsub    sh
        stf     temp
        ldf     arg
        jsub    ch
        divf    temp
        ldl     retadr
        rsub
arg     resw    2
temp    resw    2
retadr  resw    1

.�㭪�� ABS x
.��p�������� ��᮫�⭮�� ���祭�� �᫠
.�室 : ������p F - x, ��室 : ������p F - ABS(x)
abs     csect
abs     compf   c1
        jgt     m1
        stf     temp
        ldf     c1
        subf    temp
m1      rsub
c1      dword   0.0
temp    resw    2

.�㭪�� INT x
.��p��뢠�� �p����� ����
.�室 : ������p F - x, ��室 : ������p F - INT(x)
int     csect
int     stf     temp
        compf   c1
        jlt     m1
        byte    x'C4' . FIX (��� !!!)�p���p�������� � 楫��
        compf   temp
        jlt     m2
        jeq     m2
        subf    c2
        j       m2
m1      byte    x'C4'
        compf   temp
        jgt     m2
        jeq     m2
        addf    c2
m2      rsub
temp    resw    2
c1      dword   0.0
c2      dword   1.0


.�㭪�� FRC x
.�뤥����� �p����� ���
.�室 : ������p F - x, ��室 : ������p F - FRC(x)
frc     csect
                extref  int
frc     stl     retadr
        stf     temp
        jsub    int
        stf     temp1
        ldf     temp
        subf    temp1
        ldl     retadr
        rsub
temp    resw    2
temp1   resw    2
retadr  resw    1

.�㭪�� _th x, ���p��ᨬ�p㥬�� 楯�묨 �p��ﬨ
._th x = _x_   _x^2_   _x^2_         ___x^2___
.         1  +   3   +   5   + ... +  (2n + 1)  + ...
.�室 : ������p F - x , ��室 : ������p F - _th(x)
.H�� 横��, ��� �᪮p���� p����� �㭪樨
_th     csect
_th     stf     x1
        mulf    x1
        stf     x2
        divf    c1
        addf    c2
        stf     temp
        ldf     x2
        divf    temp
        addf    c3
        stf     temp
        ldf     x2
        divf    temp
        addf    c4
        stf     temp
        ldf     x2
        divf    temp
        addf    c5
        stf     temp
        ldf     x2
        divf    temp
        addf    c6
        stf     temp
        ldf     x2
        divf    temp
        addf    c7
        stf     temp
        ldf     x2
        divf    temp
        addf    c8
        stf     temp
        ldf     x2
        divf    temp
        addf    c9
        stf     temp
        ldf     x2
        divf    temp
        addf    c10
        stf     temp
        ldf     x1
        divf    temp
        rsub
x1      resw    2
x2      resw    2
temp    resw    2
c1      dword   19.0
c2      dword   17.0
c3      dword   15.0
c4      dword   13.0
c5      dword   11.0
c6      dword   9.0
c7      dword   7.0
c8      dword   5.0
c9      dword   3.0
c10     dword   1.0

.�㭪�� arth x, ���p��ᨬ�p㥬�� 楯�묨 �p��ﬨ
.arth x = _x_   _x^2_   _4x^2_   _9x^2_         __n^2x^2__
.          1  -   3   -    5   -    7   - ... -  (2n + 1)  - ...
.�室 : ������p F - x , ��室 : ������p F - arth(x)
.H�� 横��, ��� �᪮p���� p����� �㭪樨
arth    csect
arth    stf     x1
        mulf    x1
        stf     x2
        divf    c1
        mulf    c11
        stf     temp
        ldf     c2
        subf    temp
        stf     temp
        ldf     x2
        divf    temp
        mulf    c12
        stf     temp
        ldf     c3
        subf    temp
        stf     temp
        ldf     x2
        divf    temp
        mulf    c13
        stf     temp
        ldf     c4
        subf    temp
        stf     temp
        ldf     x2
        divf    temp
        mulf    c14
        stf     temp
        ldf     c5
        subf    temp
        stf     temp
        ldf     x2
        divf    temp
        mulf    c15
        stf     temp
        ldf     c6
        subf    temp
        stf     temp
        ldf     x2
        divf    temp
        mulf    c16
        stf     temp
        ldf     c7
        subf    temp
        stf     temp
        ldf     x2
        divf    temp
        mulf    c17
        stf     temp
        ldf     c8
        subf    temp
        stf     temp
        ldf     x2
        divf    temp
        mulf    c18
        stf     temp
        ldf     c9
        subf    temp
        stf     temp
        ldf     x2
        divf    temp
        stf     temp
        ldf     c10
        subf    temp
        stf     temp
        ldf     x1
        divf    temp
        rsub
x1      resw    2
x2      resw    2
temp    resw    2
c1      dword   19.0
c2      dword   17.0
c3      dword   15.0
c4      dword   13.0
c5      dword   11.0
c6      dword   9.0
c7      dword   7.0
c8      dword   5.0
c9      dword   3.0
c10     dword   1.0
c11     dword   81.0
c12     dword   64.0
c13     dword   49.0
c14     dword   36.0
c15     dword   25.0
c16     dword   16.0
c17     dword   9.0
c18     dword   4.0
