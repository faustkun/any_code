ttt     Start	0h
Modul   CSect
        Extdef  StAdr1, StAdr2, Pos1, MaxBuf, Device
        Extdef  Write, Read, Pos, Str

.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.
.                   �������� ��६����                 .
.                                                         .
.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.

StAdr1	resw	1	; ���� ��ࢮ� ��ப�
StAdr2	resw	1	; ����	��ன	��ப�
pos1	resw	1	; ��ࢠ� ������
MaxBuf	word	255	; ��࠭�祭�� �� ����� ��ப  -1

Device	byte	1	; ���ன�⢮ ��� �뢮�� ��ப


.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.
.                  ������� ��६����                   .
.                                                         .
.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.


; �⥪ ��� �࠭���� ॣ���஢
stackA	resw	1
stackX	resw	1
stackS	resw	1
stackB	resw	1

stack1	resw	1
stack2	resw	1


.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.
.                    ���� ��楤�� � �㭪権                     .
.                                                                .
.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.

	Base	off	; �⪫���� ���ᮢ���� �⭮�⥫쭮 B �� 㬮�砭��
; ��楤�� Write. �뢮��� ��ப� <StAdr1> �� ���ன�⢮ Device
Write	sta	stacka	; ��࠭��� ॣ�����
	stb	stackb
	stx	stackx

	clear	a
	clear	x
	ldb	stAdr1
write1	ldch	0,b,x	; ����㧨�� ᨬ���
	comp	#0	; 0?
	jeq	write2	; �� - ��३�
	wd	Device
	tix	maxbuf
	jlt	write1

write2	lda	stacka
	ldb	stackb
	ldx	stackx
	rsub



; ��楤�� READ. ��楤�� �⠥� ��ப� <StAdr1> � ����������.
; Pos1 = 1 �᫨ ���� ESC ���� pos1=0
READ	sta	stacka
	stb	stackb
	stx	stackx

	clear	x
	clear	a
	sta	pos1
	ldb	stAdr1
con1	rd	#248	; ���� � ���������� � ��������� ������
	comp	#8	; �஢�ઠ "�����"
	jeq	con2
	comp	#13	; �஢�ઠ �� ENTER
	jeq	con8
	comp	#27	; �஢�ઠ ESC
	jeq	con7

	comp	#32	; �஢�ઠ �� �, �⮡� ��� ᨬ��� �� �� ����� 32
	jlt	con1
	wd	#1	; �뢥�� �� ��࠭
	stch	0,b,x	; �������� ᨬ��� � ��ப�
	tix	maxbuf
	jlt	con1

	lda	maxbuf	; �᫨ ����� ��ப� ����� ���ᨬ��쭮�, �
	sub	#1
	rmo	x,a
	lda	#8	; ��।� � �����饬� ᨬ����
	wd	#1
	ldch	#7
	wd	#1	; ������ ��㪮��� ᨣ���
	j	con1

con2	rmo	a,x	; �����
	comp	#0	; �஢����, � �᫨ �� ��������?
	jeq	con1	; ��� - ��३�
	sub	#1	; 㬥����� X �� 1
	rmo	x,a

	lda	#8	; ��३� � �।��饬� ᨬ���� ��࠭�, ����� ���
	wd	#1	; �஡���� � ᭮�� ��३� � �।��饬�
	ldch	#32
	wd	#1
	ldch	#8
	wd	#1
	j	con1

con7	lda	#1	; ESC
	sta	pos1
con8			; Enter
	ldch	#13	; ����� ���⨬ �� ��ப� ����
	wd	#1
	ldch	#10
	wd	#1
	clear	a
	stch	0,b,x	; ������� 0 � ���� ��ப�
	lda	stacka
	ldx	stackx
	ldb	stackb
	rsub


; ��楤�� STR. ��ॢ���� 楫�� �᫮ �� POS1 � ��ப� � ����頥� � <stAdr1>
STR	sta	stackA
	stb	stackB
	stx	stackX
	sts	stackS

	clear	x
	ldb	stAdr1

	comp	#0	; �᫨ �᫮ ����⥫쭮�, � �뢥�� "-" �
	jgt	str1	; �८�ࠧ����� � ������⥫쭮�
	jeq	str1
	ldch	#'-'
	stch	0,b,x
	tix	MaxBuf
	jgt	str9
	jeq	str9
	clear	a
	sub	pos1
	sta	pos1

str1	lda	#1
str2	mul	#10
	comp	pos1
	jlt	str2
	jeq	str2
	div	#10

str3	sta	stack2
	lda	pos1
	div	stack2
	rmo	s,a
	add	#48
	stch	0,b,x
	tix	maxbuf
	jgt	str9
	jeq	str9
	rmo	a,s
	mul	stack2
	rmo	s,a
	lda	pos1
	subr	s,a
	sta	pos1

	lda	stack2
	div	#10
	comp	#0
	jgt	str3
str9	clear	a
	stch	0,b,x
	lda	stackA
	ldb	stackb
	lds	stacks
	ldx	stackx
	rsub




; ��楤�� POS. ��楤�� ��室�� � �����頥� � POS1 ��ࢮ� �宦�����
; ��ப� <stAdr1> � ��ப� <stAdr2>. �᫨ ������ �� �������, � �����頥�
; � POS1 ���.
POS	sta	stackA
	stb	stackB
	stx	stackX
	sts	stackS

	ldb	stAdr1
	clear	x
	stx	pos1
	lda	stAdr2
	sta	stack1
ps1	clear	a	; ������� ���� �� �᭮���� ��ப�
	ldch	@stack1
	comp	#0
	jeq	ps8	; �� �� ����� - �� ��室

	rmo	s,a
	lda	stack1	; 㢥����� ���稪
	add	#1
	sta	stack1
	sta	stack2	; ��������� ��� ���祭��
	sta	pos1	; ��������� �� ���������

	clear	x
ps2	clear	a	; ��������� ���� �� �����ப�
	ldch	0,b,x
	comp	#0	; �� �� �����?
	jeq	ps7	; �� - �⠢�� ��諨 - �� ��室
	compr	s,a	; �஢�ਬ ᮢ������� ᨬ�����
	jeq	ps3	; ᮢ���� - �த����� 横�
	clear	a
	sta	pos1
	j	ps1	; ��� - �᪠�� ᮢ������� � ᫥���饣� ����
ps3	ldch	@stack2	; ������� ᫥���騩 ���� �� �᭮���� ��ப�
	rmo	s,a
	lda	stack2	; 㢥����� ���稪�
	add	#1
	sta	stack2

	tix	maxbuf
	jlt	ps2	; ���� �஢����� ᫥���騩 ����
	j	ps7
	clear	a
	sta	pos1
	j	ps8
ps7	lda	pos1
	sub	stAdr2
	sta	pos1
ps8	lda	stackA
	ldx	stackX
	ldb	stackB
	rsub

	end	Modul