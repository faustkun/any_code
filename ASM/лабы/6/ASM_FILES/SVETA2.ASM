sveta2	start	100h
	stl	ret

	clear	x	; �뢥��� ��ப� st1 � CopyRight
work01	ldch	st1,x
	wd	#1
	tix	#leng1
	jlt	work01
	
work0	clear	x	; ������塞 ���ᨢ. ����� T-����� ������⮢
	stx	summa	; ���㫨� �㬬�
	ldt	#1	; X - �� ���� � ���ᨢ� ARRAY
work1	stx	stack

	clear	x	; �뢥��� ��ப� st2
work2	ldch	st2,x
	wd	#1
	tix	#leng2
	jlt	work2

	stt	number	; �맮��� ��楤��� �뢮�� �᫠ � ��।���� �� �����
	jsub	write	; �������

	ldch	#':'	; �������⥫쭮 �뢥��� ���� ":"
	wd	#1

	jsub	read	; �맮��� ��楤��� �⥭�� �᫠

	lda	number	; ����� �����⨬ �᫮ �� ��࠭������ ����� X
	ldx	stack
	sta	array,x

	rmo	a,t	; 㢥��稬 ���稪 ����஢
	add	#1
	rmo	t,a

	tix	#8*3	; ��३��� � ᫥���饬� ��������
	tix	#8*3
	tix	#8*3
	jlt	work1

	ldch	#10
	wd	#1
	ldch	#13
	wd	#1
; �᭮���� 横�: �騬 ������⥫�� �������� � ��室�� �� �㬬�
	clear	x
work3	lda	array,x
	comp	#0
	jgt	work4	; ������� ������⥫��, � ��३� 
work44	tix	#8*3
	tix	#8*3
	tix	#8*3
	jlt	work3
	j	work5
work4	add	summa
	sta	summa
	j	work44


work5	clear	x	; �뢥��� ᮮ�饭�� � �㬬�
work6	ldch	st4,x
	wd	#1
	tix	#leng4
	jlt	work6
	
	lda	summa
	sta	number
	jsub	write	; �뢥��� �����
	ldch	#10

wEnd	clear	x	; �뢥��� ����� �� ����� ����᪠
	clear	a
work9	ldch	st6,x
	wd	#1
	tix	#leng6
	jlt	work9

work10	rd	#248
	comp	#'Y'
	jeq	work0
	comp	#'y'
	jeq	work0
	comp	#'N'
	jeq	work11
	comp	#'n'
	jeq	work11
	comp	#27
	jeq	work11
	j	work10

work11	ldch	#10
	wd	#1
	ldch	#13
	wd	#1
	ldl	ret
	rsub

array	resw	8	; ���ᨢ �� 8 ������⮢
summa	resw	1	; �㬬� ������⮢ ���ᨢ�

number	resw	1	; �⠥������ �᫮
degree	resw	1	; ��� �⥯���
sign	resb	1	; �ᯮ����⥫쭮� �᫮


st1	byte	'������ୠ� ࠡ�� #2 �� UUM/DC',10,13
	byte	'�ணࠬ�� ������ � ���������� ���ᨢ �� 8 �ᥫ, ��⥬',10,13
	byte	'��室�� �㬬� ������⥫��� ������⮢ ���ᨢ�.',10,13
	byte	'(C) ��蠪��� �.�. ��-1-94 (�����)',13,10,10
leng1	equ	*-st1	; ����� ��ப� st1

st2	byte	13,10
	byte	'������ ������� N� '
leng2	equ	*-st2	; ����� ��ப� st2

st4	byte	'�㬬� ������⥫��� ������⮢ ���ᨢ� ࠢ��: '
leng4	equ	*-st4

st6	byte	10,13
        byte	'        ---==== ���� ����? [Y/N] ====----'
leng6	equ	*-st6

ret	resw	1
stack	resw	1









read	; ����ணࠬ�� �⥭�� �᫠ � NUMBER
	clear	a
	sta	number
	stch	sign

r1	rd	#248	; �⥭�� � ���������� ��� ��

	comp	#27	; ESC?
	jeq	work11	; �� - �� ��室
	
	comp	#8	; �����?
	jeq	r2	; �� - �� �����
	
	comp	#'-'	; ���� "-"?
	jeq	r3	; �� - ��३�
	
	comp	#13	; Enter?
	jeq	r10	; �� - ��३�

	comp	#'0'	; � �।��� "0".."9" ?
	jlt	r1	; ��� - ���� ᭮��
	comp	#'9'
	jgt	r1	; ��� - ���� ᭮��
	; �� � �।���

	rmo	s,a	; �६���� ��࠭��� A � S
	lda	number
	comp	#12
	jgt	r1	; �᫮ �㤥� ᫨誮� ����訬, �� �㤥� �ਭ����� ���
	rmo	a,s	; ��୥� A ��� ᮤ�ন���

	wd	#1	; �뢥�� �᫮
	sub	#48	; �� ASCII � �᫮

	rmo	s,a	; ॠ�������� ����: �᫮=�᫮*10 + ���
	lda	number
	mul	#10
	addr	s,a	; a=a+s
	sta	number
	j	r1	; ��横����

r2	lda	number	; �����
	comp	#0
	jgt	r23	; ᨬ���� ��� ����� ����, ��� "��������"
	clear	a
	ldch	sign
	comp	#0
	jeq	r1	; ᨬ����� ���, ���� �����
	clear	a
	stch	sign

	j	r22
r23	div	#10
	sta	number
	
r22	ldch	#8
	wd	#1
	ldch	#32
	wd	#1
	ldch	#8
	wd	#1
	j	r1

r3	lda	number  ; ���� "-"
	comp	#0	; �஢���� ����������� ���⠭����
	jgt	r1
	clear	a
	ldch	sign
	comp	#0
	jgt	r1
	; �⠢��� �����
	ldch	#1
	stch	sign
	ldch	#'-'
	wd	#1
	j	r1
	
r10	clear	a	; ENTER
	ldch	sign	; �஢���� �� ���� "-"
	comp	#0
	jeq	r101
	clear	a	; ��, ����
	sub	number
	sta	number

r101	rsub
	

Write	; ����ணࠬ�� ��� �뢮�� �᫠ �� ��࠭. �᫮ � NUMBER
	lda	number		; �஢�ਬ �� ����⥫쭮��� �
	comp	#0		; �뢥��� "-" � ��砥 ����⥫쭮��,
	jgt	w1		; � ᠬ� �᫮ ᤥ���� ������⥫��
	jeq	w1
	clear	a
	sub	number
	sta	number
	ldch	#'-'
	wd	#1

w1	lda	#10
	; ��।���� ���冷� �᫠
w2	comp	number
	jgt	w3
	mul	#10
	j	w2
w3	div	#10
	sta	degree	; ���冷� ��।����

w4	lda	number
	div	degree
	rmo	s,a
	add	#48	; �뢥�� ����
	wd	#1	; �� ��࠭
	rmo	a,s

	mul	degree	; ॠ���㥬 ����: �᫮ = �᫮ - ���*���冷�
	rmo	s,a
	lda	number
	subr	s,a	; a=a-s
	sta	number
	
	lda	degree  ; ������� ���冷� � ��३��� � ᫥���饩 ���
	div	#10
	sta	degree
	comp	#0
	jgt	w4
	rsub
	end