;                        ������ୠ� ࠡ�� �1
; 
;  ��ਠ��: 6
;  �������: ��।����� ���-�� �㫥��� ������⮢ � 楫��᫥���� ���ᨢ�
;           �� 12 ������⮢
;
Lab1	start	 0
begin   j	 go
; 
; /���������H�� �����/
retadr  resw	1   ; ���� ������ 
zero    word	 0   ; ����
one     word 	 1   ; ������
ascii   word 	 48  ; ascii-��� ��� 
res     resw 	 1   ; १����
title   byte 10,13,c'������ୠ� ࠡ�� �1. �-�: �������� �.�. ��: ��-12-00, ���: 6'
        byte 13
        byte 10
        byte 13
        byte 10 
        byte 0     ; �ਧ��� ���� ��ப�
rezt    byte c'���-�� �㫥��� ������⮢ � ���ᨢ� = '
        byte 0    
displ   byte x'01' ; ��ᯫ��
maxn    word 33    ; max � ������  ([�᫮ ��-⮢-1]*3)
mass    word 1,2,3,0,0,-5,1,9,-2,0,4,0   ; �⢥�: 4
;mass    word 1,2,3,2,1,-5,1,9,-2,9,4,-5   ; �⢥�: ��� �㫥���
error   byte c'H㫥��� ������⮢ ���.'
        byte 0 

;  /�����H����� �����/
go      stl   retadr
        lda   zero  ; ���㫥��� ॣ���஢ 
        ldx   zero
        sta   res   ; ���㫥��� १����
;                   /����� ���������/
outtl   ldch  title,x
        comp  zero
        jeq   eotl
        wd    displ
        tix   zero
        j     outtl
; 
eotl    lda   zero
        ldx   zero 
;                  /����� H������ ��-���/
search  lda   mass,x
        comp  zero
        jeq   incres
next    tix   maxn   ; ���室 � ᫥�. ��-��
        tix   maxn
        tix   maxn
        jgt   outres ; ��室�� �� ������ ����� ���ᨬ��쭮��
        j     search
;
incres  lda   res
        add   one
        sta   res
        j     next
;                   /����� ����������/
outres  lda   res
        comp  zero
        ldx   zero
        jeq   outerr     ; �� �뢮� ᮮ�饭��, �� ��� �㫥��� ��-⮢
prnrez  ldch  rezt,x
        comp  zero
        jeq   prnres
        wd    displ
        tix   zero
        j     prnrez
; 
prnres  lda   res
        add   ascii
        wd    displ
        j     finish
;
outerr  ldch  error,x
        comp  zero
        jeq   finish 
        wd    displ
        tix   zero
        j     outerr 
;                         /��H��/
finish  ldl   retadr 
        rsub
        end   begin 
