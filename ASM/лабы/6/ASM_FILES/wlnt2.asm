;                        ������ୠ� ࠡ�� �2 
; 
;  ��ਠ��: 6
;  �������: ��।����� ���-�� �㫥��� ������⮢ � 楫��᫥���� ���ᨢ�
;           �� 12 ������⮢
;
        start 0
begin   j go
; 
; /���������H�� �����/
retadr  word  0   ; ���� ������ 
ascii   word  48  ; ascii-��� ��� 
res     resw  1   ; १����
title   byte c'������ୠ� ࠡ�� �2. �-�: �������� �.�. ��: ��-12-00, ���: 6'
        byte 13
        byte 10
        byte 13
        byte 10 
        byte 0     ; �ਧ��� ���� ��ப�
rezt    byte c'���-�� �㫥��� ������⮢ � ���ᨢ� = '
        byte 0    
displ   byte x'01' ; ��ᯫ��
maxn    word 33    ; max � ������  ([�᫮ ��-⮢-1]*3)
mass    word 1,2,3,0,0,-5,1,9,-2,0,4,0   ; �⢥�: 4
;mass    word 1,2,3,2,1,-5,1,9,-2,9,4,-5   ; �⢥�: ��� �㫥���
error   byte c'H㫥��� ������⮢ ���.'
        byte 0 

;  /�����H����� �����/
go      stl   retadr
        clear a     ; ���㫥��� ॣ���஢
        clear x 
        sta   res   ; ���㫥��� १����
;                   /����� ���������/
outtl   ldch  title,x
        comp  #0 
        jeq   eotl
        wd    displ
        tix   #0 
        j     outtl
; 
eotl    clear a 
        clear x 
;                  /����� H������ ��-���/
search  lda   mass,x
        comp  #0 
        jeq   incres
next    tix   maxn   ; ���室 � ᫥�. ��-��
        tix   maxn
        tix   maxn
        jgt   outres ; ��室�� �� ������ ����� ���ᨬ��쭮��
        j     search
;
incres  lda   res
        add   #1 
        sta   res
        j     next
;                   /����� ����������/
outres  lda   res
        comp  #0 
        clear x 
        jeq   outerr     ; �� �뢮� ᮮ�饭��, �� ��� �㫥��� ��-⮢
prnrez  ldch  rezt,x
        comp  #0 
        jeq   prnres
        wd    displ
        tix   #0 
        j     prnrez
; 
prnres  lda   res
        add   ascii
        wd    displ
        j     finish
;
outerr  ldch  error,x
        comp  #0 
        jeq   finish 
        wd    displ
        tix   #0 
        j     outerr 
;                         /��H��/
finish  j     @retadr 
        rsub
        end   begin 


