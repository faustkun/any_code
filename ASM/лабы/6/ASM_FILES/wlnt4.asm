;                        ������ୠ� ࠡ�� �4 
; 
;  ��ਠ��: 6
;  �������: ��।����� ���-�� �㫥��� ������⮢ � 楫��᫥���� ���ᨢ�
;           �� 12 ������⮢
;
        start 0
        extdef title, rezt, mass, error 
        extref outtl_, searc_, outre_ 
begin   stl   retadr
        clear a     ; ���㫥��� ॣ���஢
        clear x 
        clear s     
        +jsub outtl_ 
        +jsub searc_ 
        +jsub outre_ 
        ldl   retadr
        rsub 
; /���������H�� �����/
retadr  word  0   ; ���� ������ 
title   byte c'������ୠ� ࠡ�� �4. �-�: �������� �.�. ��: ��-12-00, ���: 6'
        byte 13
        byte 10
        byte 13
        byte 10 
        byte 0     ; �ਧ��� ���� ��ப�
rezt    byte c'���-�� �㫥��� ������⮢ � ���ᨢ� = '
        byte 0    
mass    word 1,2,3,0,0,-5,1,9,-2,0,4,0   ; �⢥�: 4
;mass    word 1,2,3,2,1,-5,1,9,-2,9,4,-5   ; �⢥�: ��� �㫥���
error   byte c'H㫥��� ������⮢ ���.'
        byte 0 

;                   /����� ���������/
outtl_  csect
        extref title
outtl   +ldch  title,x
        comp  #0 
        jeq   eotl
        wd    =01 
        tix   #0 
        j     outtl
; 
eotl    clear a 
        clear x
        rsub 
;                  /����� H������ ��-���/
searc_  csect
        extref mass 
search  +lda   mass,x
        comp  #0 
        jeq   incres
next    tix   #33   ; ���室 � ᫥�. ��-��
        tix   #33 
        tix   #33 
        jgt   out    ; ��室�� �� ������ ����� ���ᨬ��쭮��
        j     search
;
incres  rmo   a,s     ;㢥��祭�� १���� �� 1
        add   #1
        rmo   s,a
        j     next
out     rsub 
;                   /����� ����������/
outre_  csect
        extref rezt, error 
outres  rmo   a,s 
        comp  #0 
        clear x 
        jeq   outerr     ; �� �뢮� ᮮ�饭��, �� ��� �㫥��� ��-⮢
prnrez  +ldch  rezt,x
        comp  #0 
        jeq   prnres
        wd    =01
        tix   #0 
        j     prnrez
; 
prnres  rmo   a,s 
        add   #48 
        wd    =01
        j     finish
;
outerr  +ldch  error,x
        comp  #0 
        jeq   finish
        wd    =01
        tix   #0 
        j     outerr
finish  rsub


