;                        ������ୠ� ࠡ�� �5 
; 
;  ��ਠ��: 6
;  �������: ��।����� ���-�� �㫥��� ������⮢ � 楫��᫥���� ���ᨢ�
;           �� 12 ������⮢
;
        start 0
begin   j go
; 
; /���������H�� �����/
retadr  word  0   ; ���� ������ 
res     resw  1   ; १����
retind  resw  1   ; ������ ������
maxn    resw  1   ; max � ������+1  ([�᫮ ��-⮢1]*3)
mass    resw  20  ; ���ᨢ
znak    resw 1    ; ���� �᫠ 
title   byte c'������ୠ� ࠡ�� �5. �-�: �������� �.�. ��: ��-12-00, ���: 6'
        byte 13
        byte 10
        byte 13
        byte 10
        byte c'������ �᫮ ������⮢ ���ᨢ� � ������ �஡��: '
        byte 0     ; �ਧ��� ���� ��ப�
rezt    byte 13
        byte 10
        byte 13
        byte 10
        byte c'���-�� �㫥��� ������⮢ � ���ᨢ� = '
        byte 0
massiv  byte 13
        byte 10
        byte c'������ �१ �஡�� �������� ���ᨢ�:'
        byte 13
        byte 10
        byte 0 
ein     byte 13
        byte 10 
        byte c'�訡�� �����!'
        byte 0
;maxn    equ 33     ; max � ������  ([�᫮ ��-⮢-1]*3)
;mass    word 123,2000,3,0,0,-5,17,9,-289,0,4,0   ; �⢥�: 4
;mass    word 167,209,3786,2,1,-5,1,9,-790,9,4,-500   ; �⢥�: ��� �㫥���
error   byte 13
        byte 10
        byte 13
        byte 10
        byte c'H㫥��� ������⮢ ���.'
        byte 0
;  /������������/
; ����� 4-� � ��H�� �H��H��� ����� H� ����H 
outnum  rmo   s,a      ; ��࠭���� ��室���� ࠡ�祣� �᫠
        ldt   #1000    ; ���� ���冷� - �����
        clear x        ; x � b ࠢ�� ����� ᮡ��, � ࠢ�� ���
        clear b
loop    divr  t,a      ; ���᫥��� ��।��� ����
        comp  #0       ; �஢�ઠ �� �� ����
        jeq   notyet
        J     began
notyet  compr x,b      ; �஢�ઠ, ����騩 �� ����
        jeq   nxtpor   ; �த������� ���᪠ ��ࢮ� ����饩 ����
began   ldx   #1       ; ��ࢠ� ������ ��� �������
        add   =48      ; ���������� ascii-���� ���
        wd    =01      ; �뢮� ��।��� ����
        sub   =48
        mulr  t,a
        mul   =-1
        addr  s,a      ; ���᫥��� ������ ࠡ�祣� �᫠
        rmo   s,a      ; ��࠭���� ࠡ�祣� �᫠
nxtpor  rmo   a,t      ; ���᫥��� ᫥���饣� ���浪�
        div   =10
        comp  #1       ; ��諨 �� �� ���浪� ������?
        jeq   lstdig   ; �뢮� ��᫥���� ����
        rmo   t,a
        rmo   a,s      ; ����㧪� ࠡ�祣� �᫠
        j     loop
lstdig  rmo   a,s
        add   #48
        wd    =01
        ldch  =c' '
        wd    =01
        rsub
; 
; ����  4-� � ��H�� �H��H��� ����� � ����������
readn   clear   s        ;���㫥��� ॣ���� १����
        lda     #1       ;����㧪� ��室���� ����� �᫠           
        sta     znak
        ldt     #10      ;����㧪� �᫠ 10
readlp  clear   a        ;���⪠ ࠡ�祣� ॣ����
        rd      =01      ;���뢠��� ��।���� ᨬ����
        comp    #' '     ;�஢�ઠ �� ����� �᫠
        jeq     checkz
        comp    #'-'     ;�஢�ઠ �� ����⥫쭮� �᫮
        jeq     chngz 
        comp    #48      ;�஢�ઠ �� �ࠢ��쭮��� �����
        jlt     errinp
        comp    #57      ;�஢�ઠ �� �ࠢ��쭮��� �����
        jgt     errinp
        mulr    t,s
        sub     #48
        addr    a,s
        j       readlp
chngz   lda     znak
        mul     #-1
        sta     znak
        j       readlp
checkz  lda     znak
        comp    #1
        jeq     readfn
        rmo     a,s
        mul     #2
        subr    a,s
readfn  rsub
; 
; 
;  /�����H����� �����/
go      stl   retadr
        clear a     ; ���㫥��� ॣ���஢
        clear x 
        sta   res   ; ���㫥��� १����
;            /����� ��������� � ��������H�� ������� ����� �����H���/
outtl   ldch  title,x
        comp  #0 
        jeq   eotl
        wd    =01
        tix   #0 
        j     outtl
;       
;               /������ ����� �����H���/
eotl    jsub  readn
        clear t
        compr  s,t
        jeq   finish
        rmo   a,s
        mul   #3
        sta   maxn
        clear a 
        clear x
;                 /����� ��������H�� � ����� �������/
inmass  ldch  massiv,x
        comp  #0 
        jeq   eomass 
        wd    =01
        tix   #0 
        j     inmass
         
;                 /�����H�H�� �������/
eomass  clear x
fulmas  jsub    readn 
        sts     mass,x  ; ������ ��।���� �������
        tix     maxn
        tix     maxn  
        tix     maxn  
        jlt     fulmas
;                  /����� H������ ��-���/
        clear a
        clear x 
search  lda   mass,x
        comp  #0 
        jeq   incres
next    tix   #maxn   ; ���室 � ᫥�. ��-��
        tix   #maxn
        tix   #maxn
        jgt   outres  ; ��室�� �� ������ ����� ���ᨬ��쭮��
        j     search
;
incres  lda   res
        add   #1 
        sta   res
        j     next
;                   /����� ����������/
outres  lda   res
        comp  #0 
        clear x 
        jeq   outerr     ; �� �뢮� ᮮ�饭��, �� ��� �㫥��� ��-⮢
prnrez  ldch  rezt,x
        comp  #0 
        jeq   prnres
        wd    =01
        tix   #0 
        j     prnrez
; 
prnres  lda   res
        add   =48 
        wd    =01
        j     finish
;
outerr  ldch  error,x
        comp  #0 
        jeq   finish 
        wd    =01
        tix   #0 
        j     outerr
;                      /����� ������H�� �� ������ �����/
errinp  clear   x
        clear   a
errlp   ldch    ein,x 
        comp    #0
        jeq     finish
        wd      =01   
        tix     #0
        j       errlp
;                         /��H��/
finish  ldl   retadr 
;       j     @retadr
        rsub
        end   begin 


