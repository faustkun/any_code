LAB3    START    1000
        CLEAR    X  
        LDA      FI 
        RMO      A,T
        LDS      #105
L1      LDCH     ZAG,X             
        WD       =X'00'
        TIX      #ZAG1
        JLT      L1
        J        L
L       CLEAR    B
        CLEAR    T
        CLEAR    X      
        LDS      #24
L2      COMPR    S,X
        JEQ      K           
L3      LDA      MAS,X      - 
        TIXR      S         
        TIXR      S
        TIXR      S 
        COMPR    A,B        
        JLT      SUM
        J        L2 
SUM     MUL      RO          
        ADDR     A,T 
        J        L2
K       CLEAR    X        
        CLEAR    S
        LDS      #105
        COMPR    B,T      	  
        JEQ      OSH
        J        NOSH1
OSH     
        LDCH     SIGN,X
        WD       =X'00'
        TIX      #SIGN1
        JLT      OSH
        J        E
NOSH1   
        CLEAR    X                 
        CLEAR    S            
        CLEAR    A         . .
        CLEAR    B
        LDB      TZ
        STT      RESS
NOSH    LDA      RESS
        RMO      T,A
        LDA      T
        DIVR     A,B
        LDA      #1           
        COMPR    A,B
        JEQ      DE
        DIVR     B,T
        CLEAR    A
        COMPR    A,T
        JEQ      NUL
        J        NULOFF
NUL                 
        COMPR    A,S
        JEQ      NOSH
        ADD      P           .   RES1 
        WD       =X'00'
        J        PRG
NULOFF               
        COMPR    A,S
        JEQ      ZN
        J        NULOF
ZN      LDCH     MI,X         
        WD       =X'00'    
        LDS      #1
.                    
        
NULOF   LDX      P
        ADDR     X,T
        RMO      A,T 
        WD       =X'00'     
PRG     SUB      P
        MULR     B,A
        SUB      RESS
        MUL      RO
        STA      RESS
        J        NOSH
DE      
        CLEAR    A
        COMPR    S,A
        JEQ      DEO
        J        DET
DEO     CLEAR    X   
        LDCH     MI,X         .  
        WD       =X'00'    
DET     LDA      P
        ADDR     A,T
        RMO      A,T
        WD       =X'00'     
        J        E
E       RSUB
MAS    WORD      0         
       WORD      -2000
       WORD      0
       WORD      0
       WORD      -1
       WORD      -10
       WORD      0
       WORD      -2
       USE       DIMI
X      RESW      1
RES1   RESW      1
RESS   RESW      1
D      RESW      1
Z      RESW      1
DEL    RESW      1
I      WORD      33
ZERO   WORD      0
ONE    WORD      1
FIFE   WORD      24
FI     WORD      105
RO     WORD      -1 
P      WORD      48
T      WORD      10
TZ     WORD      10000
MI     BYTE      C'-'
MONIT  BYTE      X'00'
       USE       DIMIK
ZAG    BYTE      '  5   cโ. ฃเ.-395 ฅฌจญ  ฌจโเจ๏                     แใฌฌ  ฎโเ. ํซ-โฎข เ ขญ :   '
zag1   equ       *-zag
SIGN   BYTE      '0           '
sign1  equ       *-sign   
        