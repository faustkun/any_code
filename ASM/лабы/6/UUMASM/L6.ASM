LAB3    START    0
        EXTDEF   SUM
        EXTREF   NOSH1,OSH,E
        CLEAR    X  
        LDA      FI 
        RMO      A,T
        LDS      #105
L1      LDCH     ZAG,X             
        WD       =X'00'
        TIX      #ZAG1
        JLT      L1
        J        L
L       CLEAR    B
        CLEAR    T
        CLEAR    X      
        LDS      #24
L2      COMPR    S,X
        JEQ      K           
L3      LDA      MAS,X      - 
        TIXR      S         
        TIXR      S
        TIXR      S 
        COMPR    A,B        
        JLT      SUM
        J        L2 
SUM     MUL      RO          
        ADDR     A,T 
        J        L2
K       CLEAR    X        
        CLEAR    S
        LDS      #105
        STT      SUM
        COMPR    B,T      	  
        +JEQ      OSH
        +J        NOSH1
       RSUB
MAS     WORD      0
        WORD      0
        WORD      0
        WORD      0
        WORD      0
        WORD      0
        WORD      0
        WORD      0

RO     WORD      -1
FI     BYTE       105 
ZAG    BYTE      '  6   cโ. ฃเ.-395 ฅฌจญ  ฌจโเจ๏                     แใฌฌ  ฎโเ. ํซ-โฎข เ ขญ :   '
zag1   equ       *-zag

OSH     CSECT
        EXTREF   E
OSH1    LDCH     SIGN,X
        WD       MON
        TIX      #SIGN1
        JLT      OSH1
        +J        E
        RSUB
MON     BYTE     X'00' 
SIGN    BYTE      '0           '
sign1   equ       *-sign   

        
NOSH1   CSECT
        EXTREF   SUM,E
        CLEAR    X                 
        CLEAR    S            
        CLEAR    A         . .
        CLEAR    B
        LDB      TZ
        LDT      SUM
        STT      RESS
NOSH    LDA      RESS
        RMO      T,A
        LDA      T
        DIVR     A,B
        LDA      #1           
        COMPR    A,B
        JEQ      DE
        DIVR     B,T
        CLEAR    A
        COMPR    A,T
        JEQ      NUL
        J        NULOFF
NUL                 
        COMPR    A,S
        JEQ      NOSH
        ADD      P           .   RES1 
        WD       =X'00'
        J        PRG
NULOFF               
        COMPR    A,S
        JEQ      ZN
        J        NULOF
ZN      LDCH     MI,X         
        WD       MONI    
        LDS      #1
.                    
        
NULOF   LDX      P
        ADDR     X,T
        RMO      A,T 
        WD       MONI     
PRG     SUB      P
        MULR     B,A
        SUB      RESS
        MUL      RO
        STA      RESS
        J        NOSH
DE      
        CLEAR    A
        COMPR    S,A
        JEQ      DEO
        J        DET
DEO     CLEAR    X   
        LDCH     MI,X         .  
        WD       MONI       
DET     LDA      P
        ADDR     A,T
        RMO      A,T
        WD       MONI     
        J        E
        RSUB
RESS    RESW      1
MI     BYTE      C'-'
TZ     WORD      10000
P      WORD      48    
T      WORD      10
RO     WORD      -1
MONI    BYTE      X'00'                 
E       CSECT
        RSUB
 
