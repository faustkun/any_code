SL6      START   0

MPRN3     MACRO   &MOUT,&MS3,&ML3
:�뢮� ᮮ�饭�� �� ������⢨� �㫥��� ������⮢
         LDCH    MS3,X
         WD      MOUT
         TIX     #ML3
         JLT     *-9
         RMO     L,B         : L:=B
         MEND

MPRN2     MACRO   &MOUT,&MS2,&ML2,&MRES
:�뢮� ��ப� �⢥� 
         LDCH    &MS2,X
         WD      &MOUT
         TIX     #&ML2
         JLT     *-9
         LDA     &RES
         DIV     #3          :A:=A/3
         ADD     #48         :A:=A+48
         WD      &MOUT
         RMO     L,B
         MEND

FIRSTD   RMO     B,L         : B:=L
         LDX     #0          : X:=0
:�뢮� ���������
PRN1     LDCH    STR1,X      
         WD      OUT
         TIX     #LS1
         JLT     PRN1
         LDX     #0
         STX     RESULT 
:�⥭�� ���ᨢ�
READMS   LDA     MASSIV,X    : A:=MASSIV[X]
         TIX     #27
         TIX     #27
         TIX     #27
         COMP    #0          :�ࠢ������ ������� � 0
         JGT    EMAS        :A>0
         JLT    EMAS        :A<0
         STX     RESULT      : RESULT:=<X>
         JSUB   EMAS    

EX       LDA     RESULT      : A:=RESULT
         COMP    #0          :�᫨ A>0, ��३� �� ���� OTVET
         LDX     #0
         JEQ     PRN3        
         JGT     PRN2
PRN3     MPRN3   OUT,STR3,LS3
         RMO     L,B
         RSUB
PRN2     MPRN2   OUT,STR2,LS2,RESULT         
         RMO     L,B
         RSUB
EMAS     RMO     A,X          :A:=<X>
         DIV     #3           :A:=<A>/3
         COMP    #9           :�ࠢ������ � 9
         JLT     READMS       :A<9
         JEQ     EX           :A=9

MASSIV   WORD    1      :����� ���ᨢ� 9 ᫮� ��� 27 ����
         WORD    6
         WORD    3
         WORD    4
         WORD    7
         WORD    6
         WORD    0
         WORD    0
         WORD    9
RESULT   RESW    1
OUT      BYTE    X'00'        :��࠭
STR1     BYTE    '������ୠ� ࠡ�� 6 ���祭�� �.�.                                                '
LS1      EQU     *-STR1
STR2     BYTE    '����� ��᫥����� �㫥���� ������� ���ᨢ�: '
LS2      EQU     *-STR2
STR3     BYTE    '�㫥�� �������� ����������.'
LS3      EQU     *-STR3

         END     FIRSTD


