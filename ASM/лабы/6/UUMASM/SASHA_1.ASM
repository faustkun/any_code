SASHA_1   START  1000       :���祭�� ��᫥����� ����⥫쭮�� �������
FIRST     STL    RETADR     :���࠭���� ���� ������
          LDA    HEAD0      :��ॢ�� ��ப�
          WD     MONIT
          LDA    HEAD1      :������ ���⪨
          WD     MONIT
          LDX    ZERO       :���㫥��� (X)
LEAD1     LDCH   HEAD2,X    :������ � ����訩 ���� (A) X-�� ᨬ���� ��ப�
          WD     MONIT
          TIX    LONG2      :������� X 㢥����� �� 1
          JLT    LEAD1      :�᫨ X < LONG2, ��३� �� ���� LEAD1
          LDX    ZERO       :���㫥��� (X)
          STX    ANSWER     :���㫥��� ANSWER
SEQL      LDA    MASSIV,X   :����� �� ���ᨢ� X-� �������
          TIX    LONGM      :��३� �� ᫥���騩 �������
          TIX    LONGM
          TIX    LONGM
          COMP   ZERO       :�᫨ ������� <= 0, ������� ��� � ANSWER
          JGT    NUMBR
          JEQ    NUMBR
          STA    ANSWER
NUMBR     STX    LAST       :��।������ ������ ������� ���ᨢ�
          LDA    LAST
          DIV    THREE
          COMP   TWELV      :�᫨ ���ᨢ �����稫��, �த������ �ணࠬ��
          JLT    SEQL
          LDA    ANSWER     :������� � (A) ��६����� ANSWER
          COMP   ZERO       :�᫨ ANSWER < 0, ��३� �� ���� NEGAT
          JLT    NEGAT
          LDX    ZERO       :���㫥��� (X)
LEAD3     LDCH   HEAD4,X    :������ � ����訩 ���� (A) X-�� ᨬ���� ��ப�
          WD     MONIT
          TIX    LONG4      :������� X 㢥����� �� 1
          JLT    LEAD3      :�᫨ X < LONG4, ��३� �� ���� LEAD1
          RSUB              :��室 �� �ணࠬ��
NEGAT     LDX    ZERO       :���㫥��� (X)
LEAD2     LDCH   HEAD3,X    :������ � ����訩 ���� (A) X-�� ᨬ���� ��ப�
          WD     MONIT
          TIX    LONG3      :������� X 㢥����� �� 1
          JLT    LEAD2      :�᫨ X < LONG3, ��३� �� ���� LEAD2
          LDA    ANSWER     :������� � (A) ��६����� ANSWER
          MUL    MINUS      :�������� �� -1
          STA    ANSWER
          COMP   HEAD0      :�ࠢ����� � 10
          JLT    DIG1       :��᫮ ����� 10
          DIV    HEAD0
          COMP   HEAD0
          JLT    DIG2       :��᫮ ����� 100
          DIV    HEAD0
          COMP   HEAD0
          JLT    DIG3       :��᫮ ����� 1000
          DIV    HEAD0
          COMP   HEAD0
          JLT    DIG4       :��᫮ ����� 10000
          DIV    HEAD0
          ADD    DATE       :��᫮ ����� 100000. �ਢ��� ��� � ASCII-�ଠ��
          WD     MONIT
          SUB    DATE       :��ॢ�� ���⭮ � �᫮���� �ଠ��
          MUL    HEAD0      :����⠭������� ����⪮� ����� � LAST
          MUL    HEAD0
          MUL    HEAD0
          MUL    HEAD0
          STA    LAST
          LDA    ANSWER
          SUB    LAST       :A <- (A) - LAST
          STA    ANSWER     :��᢮��� ANSWER ���⮪
          DIV    HEAD0      :A <- (A) / 1000
          DIV    HEAD0
          DIV    HEAD0
DIG4      ADD    DATE
          WD     MONIT
          SUB    DATE
          MUL    HEAD0      :����⠭������� ����� � LAST
          MUL    HEAD0
          MUL    HEAD0
          STA    LAST
          LDA    ANSWER
          SUB    LAST
          STA    ANSWER
          DIV    HEAD0      :A <- (A) / 100
          DIV    HEAD0
DIG3      ADD    DATE
          WD     MONIT
          SUB    DATE
          MUL    HEAD0      :����⠭������� �⥭ � LAST
          MUL    HEAD0
          STA    LAST
          LDA    ANSWER
          SUB    LAST
          STA    ANSWER
          DIV    HEAD0      :A <- (A) / 10
DIG2      ADD    DATE
          WD     MONIT
          SUB    DATE
          MUL    HEAD0      :����⠭������� ����⪮� � LAST
          STA    LAST
          LDA    ANSWER
          SUB    LAST
          STA    ANSWER
DIG1      ADD    DATE
          WD     MONIT
          RSUB


HEAD2     BYTE   '������ୠ� ࠡ�� 1 ��㤥�� ��㯯� ��-3-95 ����誨�� ����ᠭ��.           '
HEAD3     BYTE   '��᫥���� ����⥫�� �������: -'
HEAD4     BYTE   '����⥫�� �������� � ���ᨢ� ����������.'
MONIT     BYTE   X'00'

DATE      WORD   48         :��� �ਢ������ � ASCII-�ଠ��
HEAD0     WORD   10         :��ॢ�� ��ப�
HEAD1     WORD   13         :������ ���⪨
LONG2     WORD   78         :����� HEAD2
LONG3     WORD   34         :����� HEAD3
LONG4     WORD   45         :����� HEAD4
LONGM     WORD   36         :����� ���ᨢ�
MASSIV    WORD   1          :���ᨢ
          WORD   2
          WORD   3
          WORD   4
          WORD   -5
          WORD   6
          WORD   3276
          WORD   -8
          WORD   9
          WORD   10
          WORD   -12345
          WORD   12
MINUS     WORD   -1
THREE     WORD   3
TWELV     WORD   12
ZERO      WORD   0

ANSWER    RESW   1          :�⢥�
LAST      RESW   1
RETADR    RESW   1
