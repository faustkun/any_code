                             ;               ???????????? ?????? ?4
;              ????????? ????? ???-1-10
;		(????? ????????????? ????????? ???????)
lab4    start   0
		extref	vivod,obrmas,choice
		extdef	Mass,dMass,kon1,dkon1,kon4,dkon4
		
		;??????? ?????
		ldt		#head
		ldb		#dhead
		stb		temp
		jsub	vivod
		jsub	obrmas
		;?????? ? ????????
		lda		#10
		wd		#0
		lda		#13
		wd		#0
		jsub	choice
		
		+j		#x'FFFF'

;----------------------------------------------------------------

head	byte	'Lab_0',10,13,'Afanacev Anton ',10,13,'ITO-1-10',10,13,0
dhead	equ		*-head
kon1	byte	'Net polojitelnih eltmentov',10,13,0
dkon1	equ		*-kon1
kon2	byte	'Oshibka programmi',10,13,0
dkon2	equ		*-kon2
kon3	byte	'Summa polojitelnih >9',10,13,0
dkon3	equ		*-kon3
kon4	byte	'Summa polojitelnih =',0
dkon4	equ		*-kon4

oldb	word	0
olda	word	0
oldx	word	0
oldt	word	0
olds	word	0
str    byte   0,0,0,0,0


shift	word	48
nine	word	9
tree	word	3
temp 	word 	0
leng	word	0
res		word 	0		
nil		word	0
one 	word  	1		
Mass 	WORD   	1,4,4       ;9
dMass	equ		*-Mass
;Mass   word   	32000,1000	;?????? ??????
;Mass   word	1,-2,3	 	;4
;Mass   word	0			;??? ????????????? ?????????
;Mass   word	-8,-2		;??? ????????????? ?????????
;Mass   word	32768		;?	

;----------------------------------------------------------------
; ????? ?????
vivod	csect
        clear	a
        clear	x
		addr	t,b
		rmo	x,t
		
str1	ldch	0,x
		wd	#0
		tixr	b
		jlt	str1
		+rsub	

;----------------------------------------------------------------
;????????? ???????
obrmas	csect
		stl		oldl
		clear		b
		clear          t
		clear        x
		ldt		#2
		
		extref	Mass,dMass,intout
main2	stb		temp
         
		ldb		Mass,X;?????
		jsub	        intout
		lda		#32
		wd		#0
		ldb		temp
		lda		Mass,X
		comp	#0
		jgt		plus
		j		prover

plus	addr	a,b
		j		prover

prover	addr	t,x
		tix		#dMass
		jlt		main2
		lda		#0
		ldl		oldl
		+rsub
temp 	word	0
oldl	word	0	
;----------------------------------------------------------------
;????? ????????????? ?????
intout  csect

		sta		olda
		stb		oldb       
        stx    oldx
        sts    olds
        stt    oldt
        clear  	x
		clear	a
		;clear	b
		clear	t
		clear	s
		
        ldt    #10 
        rmo    a,b
        comp   #0
        jlt    neg
        j      listart
neg     clear  b
        subr   a,b
        lda    #45
        wd     x'01'
        j      listart
        
lintout tix    #0
listart rmo    a,b
        divr   t,b
        mulr   t,b
        subr   b,a
        lds    #48
        addr   s,a
        stch   str,x       
        divr   t,b
        lds    #0
        compr  b,s
        jgt    lintout
        lds    #-1
        ldt    #0
liprint ldch   str,x
        wd     x'01'
        addr   s,x
        compr  x,t
        jlt    liexit
        j      liprint
liexit  ldx    oldx
        lds    olds
        ldt    oldt
        ;clear 	b
        ldb	oldb
		lda	olda
        
		+rsub

oldb	word	0
olda	word	0
oldx	word	0
oldt	word	0
olds	word	0
str    byte   0,0,0,0,0
;----------------------------------------------------------------
choice	csect
		extref	kon1,dkon1,kon4,dkon4,intout
		stb		temp
		lda 	temp
		comp	#0
		ldx		#0
		lds		#dkon1
		jeq		case1; ���� ����� ����
		lds		#dkon4
		j		case4; ���� ������ ������
		
		case1	lda		#0
		ldch	kon1,X; ��� �������������
		wd		#0
		tixr	S
		jlt		case1
		+j 		#x'FFFF'
		
		case4	lda		#0
		ldch	kon4,X; �� ��
		wd		#0
		tixr	S
		jlt		case4

case4_1	lda		#0
		lda 	temp
		add 	#48
		stl		oldl
		jsub	intout
		ldl		oldl
		+j 		#x'FFFF'
		
temp	word	0	
oldl	word	0
;----------------------------------------------------------------
;----------------------------------------------------------------
;----------------------------------------------------------------
;----------------------------------------------------------------
;----------------------------------------------------------------
