Lab1	start	0

	ldx	nil	
	stx	rst
sph	ldch	hdr,X ;��ᯥ�⪠ ���������
	comp	nil
	jeq	clx
	wd	nil
	tix	nil
	j	sph

clx	ldx	nil
srch	lda	array,X	;���� ��᫥����� ����⥫쭮�� �������
	comp	nil
	jgt	pos
	jeq	pos
	sta	rst
pos	tix	nil
	tix	nil
	tix	alen
	jlt	srch

	ldx	nil	;�஢�ઠ १����
	lda	rst
	comp	nil
	jeq	pnoneg	;�᫨ ���祭�� � ��६����� १���� ��⠫��� �㫥��, ����� ����⥫��� ������⮢ ���
	mul	minone
	comp	maxp	;�᫨ ���祭�� �ॢ��室�� 9 �� �����, � ��襬, �� �� ����� �ᯥ���� १����
	jgt	upr
	sta	rst
	
pok	ldch	okmsg,X	;�᫨ �� OK, � �뢮��� ᮮ�饭�� � ⮬, �� १���� ������
	comp	nil
	jeq	pres
	wd	nil
	tix	nil
	j	pok
pres	ldch	minus	;� ᠬ १����
	wd	nil
	lda	rst
	add	sdvig
	wd	nil
	j	fin	

pnoneg	ldch	noneg,X	;����� ᮮ�饭�� �� ������⢨� ����⥫��� ������⮢
	comp	nil
	jeq	fin
	wd	nil
	tix	nil
	j	pnoneg

upr	ldch	cpr,X	;����� ᮮ�饭�� � ⮬, �� ���������� �ᯥ���� १����
	comp	nil
	jeq	fin
	wd	nil
	tix	nil
	j	upr

fin	rsub

hdr	byte       'This is a lab work �1  var 5',10,13
	byte	    'it was coded by Alex Tomenuk',10,13
        byte	    '      VSM-7-01 student',10,13
        byte       'Must find the value of a last negative',10,13
        byte       '       in array of 15 integer',10,13,10,13,0
noneg	byte       'All Array members are positive',10,13,0
cpr	byte       'The value ot the last negative',10,13
        byte       '        is lower then -09 ',10,13,0
okmsg	byte       'The value of last negative',10,13
        byte       '              is ',0

;array	word	1,2,3,4,5,6,7,8,9,10,11,12,13	;���� �ਬ���
array	word	1,2,3,-4,5,6,7,8,9,10,11,12,13
;array	word	1,2,3,4,5,6,7,8,9,10,11,12,-13
alen	word	39	;����� ���ᨢ�
sdvig	word	48	;����� � ⠡��� ᨬ�����
minone	word	-1
maxp	word	9
nil	word	0
minus	byte	'-'
rst	resw	1	;��६����� ��� �࠭���� १����
	
	end
