Lab2	start	0

	clear	X
	clear	B
	clear	T

sph	ldch	hdr,X	;��ᯥ�⪠ ���������
	compr	A,T
	jeq	clx
	wd	#0
	tix	#0
	j	sph

clx	clear	X
srch	lda	array,X	;���� ��᫥����� ����⥫쭮�� �������
	compr	A,T
	jgt	pos
	jeq	pos
	rmo	B,A
pos	tix	#0
	tix	#0
	tix	alen
	jlt	srch

	clear	X	;�஢�ઠ १����
	rmo	A,B
	compr	A,T
	jeq	pnoneg	;�᫨ ���祭�� � ��६����� १���� ��⠫��� �㫥��, ����� ����⥫��� ������⮢ ���
	mul	#-1
	comp	#9	;�᫨ ���祭�� �ॢ��室�� 9 �� �����, � ��襬, �� �� ����� �ᯥ���� १����
	jgt	upr
	rmo	B,A
	
pok	ldch	okmsg,X	;�᫨ �� OK, � �뢮��� ᮮ�饭�� � ⮬, �� १���� ������
	compr	A,T
	jeq	pres
	wd	#0
	tix	#0
	j	pok
pres	ldch	#'-'	;� ᠬ १����
	wd	#0
	rmo	A,B
	add	#48
	wd	#0
	j	fin	

pnoneg	ldch	noneg,X	;����� ᮮ�饭�� �� ������⢨� ����⥫��� ������⮢
	comp	#0
	jeq	fin
	wd	#0
	tix	#0
	j	pnoneg

upr	ldch	cpr,X	;����� ᮮ�饭�� � ⮬, �� ���������� �ᯥ���� १����
	comp	#0
	jeq	fin
	wd	#0
	tix	#0
	j	upr

fin	rsub

hdr	byte       'This is a lab work �2  var 5',10,13
	byte	    'it was coded by Alex Tomenuk',10,13
        byte	    '      VSM-7-01 student',10,13
        byte       'Must find the value of a last negative',10,13
        byte       '       in array of 15 integer',10,13,10,13,0
noneg	byte       'All Array members are positive',10,13,0
cpr	byte       'The value ot the last negative',10,13
        byte       '        is lower then -09 ',10,13,0
okmsg	byte       'The value of last negative',10,13
        byte       '              is ',0

;array	word	1,2,3,4,5,6,7,8,9,10,11,12,13	;���� �ਬ���
array	word	1,2,3,-4,5,6,7,8,9,10,11,12,13
;array	word	1,2,3,4,5,6,7,8,9,10,11,12,-13
alen	word	39	;����� ���ᨢ�

rst	resw	1	;��६����� ��� �࠭���� १����
	
	end
