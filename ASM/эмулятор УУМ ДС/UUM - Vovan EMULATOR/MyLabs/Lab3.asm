Lab3	start	0

	extref	strprn, glneg
	extdef	str, array, alen
	
	stl	retadr
	clear	B
	clear	T
	lda	#hdr	;��ᯥ�⪠ ���������
	sta	str
	jsub	strprn	
	jsub	glneg	;���� ��᫥����� ����⥫쭮�� �������
	
	clear	X	;�஢�ઠ १����
	rmo	A,B
	compr	A,T
	jeq	pnoneg	;�᫨ ���祭�� � ��६����� १���� ��⠫��� �㫥��, ����� ����⥫��� ������⮢ ���
	mul	#-1
	comp	#9	;�᫨ ���祭�� �ॢ��室�� 9 �� �����, � ��襬, �� �� ����� �ᯥ���� १����
	jgt	upr
	rmo	B,A
	
	lda	#okmsg
	sta	str
	jsub	strprn		;�᫨ �� OK, � �뢮��� ᮮ�饭�� � ⮬, �� १���� ������	
	ldch	#'-'	;� ᠬ १����
	wd	#0
	rmo	A,B
	add	#48
	wd	#0
	j	fin	

pnoneg	lda	#noneg
	sta	str
	jsub	strprn	;����� ᮮ�饭�� �� ������⢨� ����⥫��� ������⮢	
	j	fin	

upr	lda	#cpr
	sta	str
	jsub	strprn	;����� ᮮ�饭�� � ⮬, �� ���������� �ᯥ���� १����
	j	fin	

fin	ldl	retadr
	rsub

hdr	byte       'This is a lab work �3  var 5',10,13
	byte	    'it was coded by Alex Tomenuk',10,13
        byte	    '      VSM-7-01 student',10,13
        byte       'Must find the value of a last negative',10,13
        byte       '       in array of 15 integer',10,13,10,13,0
noneg	byte       'All Array members are positive',10,13,0
cpr	byte       'The value ot the last negative',10,13
        byte       '        is lower then -09 ',10,13,0
okmsg	byte       'The value of last negative',10,13
        byte       '              is ',0

;array	word	1,2,3,4,5,6,7,8,9,10,11,12,13	;���� �ਬ���
array	word	1,2,3,-4,5,6,7,8,9,10,11,12,13
;array	word	1,2,3,4,5,6,7,8,9,10,11,12,-13
alen	word	39	;����� ���ᨢ�
str	resw	1	;��६����� ��� �࠭���� ���� ��ப�
retadr	resw	1	;��६����� ��� �࠭���� ���� ������


;�����, ᮤ�ঠ�� ��楤��� �ᯥ�⪨ ��ப�
strprn	csect
	extref	str
	stl	pradr
	ldx	str
	clear	A
bprns	ldch	0,X
	compr	A,T
	jeq	eprns
	wd	#0
	tix	#0
	j	bprns
eprns	clear	X
	ldl	pradr	
	rsub
pradr	resw	1


;����� ���᪠ ��᫥����� ����⥫쭮�� ������� 
;� ���ᨢ� &array.
;������� ����頥��� � ॣ���� B
glneg	csect
	extref	array,alen
	stl	gradr
srch	lda	array,X	;
	compr	A,T
	jgt	pos
	jeq	pos
	rmo	B,A
pos	tix	#0
	tix	#0
	tix	alen
	jlt	srch
	ldl	gradr
	rsub
gradr	resw	1


	end
