//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
run = new UUM(Handle,Memo3,Memo2,Memo1);

Memo3->Enabled=false;
SpeedButton1->Enabled=true;
SpeedButton2->Enabled=true;
SpeedButton3->Enabled=false;
SpeedButton4->Enabled=false;
SpeedButton5->Enabled=false;
SpeedButton6->Enabled=true;
}
//---------------------------------------------------------------------------



void __fastcall TForm1::FormDestroy(TObject *Sender)
{
delete run;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::My_OpenExecute(TObject *Sender)
{
if (OpenDialog1->Execute())
        {
        run->Load(OpenDialog1->FileName);
        Memo3->Enabled=true;
        SpeedButton1->Enabled=true;
        SpeedButton2->Enabled=true;
        SpeedButton3->Enabled=true;
        SpeedButton4->Enabled=true;
        SpeedButton5->Enabled=true;
        SpeedButton6->Enabled=true;
        }
}
//---------------------------------------------------------------------------




void __fastcall TForm1::My_RunExecute(TObject *Sender)
{
run->Run(CheckBox1->Checked,CheckBox2->Checked, CheckBox3->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::My_NewExecute(TObject *Sender)
{
if (SaveDialog1->Execute())
        {
        String index;
        if      (SaveDialog1->FilterIndex==1){index="asm";}
        else if (SaveDialog1->FilterIndex==2){index="mac";}
        else {index="";}
        
        run->New(SaveDialog1->FileName,index);

        Memo3->Enabled=true;
        SpeedButton1->Enabled=true;
        SpeedButton2->Enabled=true;
        SpeedButton3->Enabled=true;
        SpeedButton4->Enabled=true;
        SpeedButton5->Enabled=true;
        SpeedButton6->Enabled=true;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::My_SaveExecute(TObject *Sender)
{
run->Save();
}

//---------------------------------------------------------------------------

void __fastcall TForm1::My_Save_AsExecute(TObject *Sender)
{
if (SaveDialog1->Execute())
        {
        String index;
        if      (SaveDialog1->FilterIndex==1){index="asm";}
        else if (SaveDialog1->FilterIndex==2){index="mac";}
        run->Save_As(SaveDialog1->FileName,index);
        }
}
//---------------------------------------------------------------------------



