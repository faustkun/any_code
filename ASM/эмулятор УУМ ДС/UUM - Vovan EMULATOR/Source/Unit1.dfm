object Form1: TForm1
  Left = 173
  Top = 244
  Width = 1003
  Height = 771
  Caption = 'MIREA UUM/DC Runer for  Windows XP. Creating by Vovan.'
  Color = clBlack
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    000000000000330077000000000000000000000000003B077070000000000000
    000000000000BB807007000000000000000000000300B0007000700000000000
    00000000330070070700070000000000000000003B0700700070007000000000
    00000000BB800700000700070000000000000300B00070000000700070000000
    0000330070070000000007000700000000003B07007000000000007007000000
    0000BB800700000000000007070000000300B000700000000070000077000000
    330070070000000007000000803300003B070070000000000000000800330000
    BB8007000000000000000080BBBB0300B000700000000070000008000BB03300
    70070000000707000000803300003B070070000000707000000800330000BB80
    07000000070700000080BBBB0000B000700000000070000008000BB000007007
    0000000007000000803300000000707000007770000000080033000000008700
    0007070700000080BBBB00000000080000077777000008000BB0000000000080
    0007070700008033000000000000000800007770000800330000000000000000
    800000000080BBBB00000000000000000800000008000BB00000000000000000
    0080000080330000000000000000000000080008003300000000000000000000
    00008080BBBB00000000000000000000000008000BB00000000000000000FFFF
    33FFFFFF21FFFFFF00FFFFFB007FFFF3003FFFF2001FFFF0000FFFB00007FF30
    0003FF200003FF000003FB000003F3000000F2000000F0000010B00000393000
    000F2000000F0000010F0000039F000000FF000000FF000010FF800039FFC000
    0FFFE0000FFFF0010FFFF8039FFFFC00FFFFFE00FFFFFF10FFFFFFB9FFFF}
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 67
    Height = 717
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 1
    BorderStyle = bsSingle
    Color = clBlack
    TabOrder = 0
    object SpeedButton1: TSpeedButton
      Left = 3
      Top = 4
      Width = 57
      Height = 18
      Action = My_Open
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpeedButton2: TSpeedButton
      Left = 3
      Top = 25
      Width = 57
      Height = 18
      Action = My_New
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpeedButton3: TSpeedButton
      Left = 3
      Top = 46
      Width = 57
      Height = 19
      Action = My_Save
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpeedButton4: TSpeedButton
      Left = 3
      Top = 67
      Width = 57
      Height = 18
      Action = My_Save_As
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpeedButton5: TSpeedButton
      Left = 3
      Top = 139
      Width = 57
      Height = 18
      Action = My_Run
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpeedButton6: TSpeedButton
      Left = 3
      Top = 160
      Width = 57
      Height = 18
      Action = My_Close
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CheckBox1: TCheckBox
      Left = 3
      Top = 103
      Width = 97
      Height = 17
      Caption = 'Pause'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object CheckBox2: TCheckBox
      Left = 3
      Top = 119
      Width = 97
      Height = 17
      Caption = 'Debug'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object CheckBox3: TCheckBox
      Left = 3
      Top = 87
      Width = 97
      Height = 17
      Caption = 'Macro'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object Panel2: TPanel
    Left = 67
    Top = 0
    Width = 928
    Height = 717
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 1
    Color = clSkyBlue
    TabOrder = 1
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 926
      Height = 715
      Align = alClient
      BevelOuter = bvNone
      Caption = 'Panel3'
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 0
        Top = 611
        Width = 926
        Height = 5
        Cursor = crVSplit
        Align = alBottom
        Beveled = True
        Color = clTeal
        ParentColor = False
      end
      object Panel4: TPanel
        Left = 0
        Top = 616
        Width = 926
        Height = 99
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'Panel4'
        TabOrder = 0
        object Memo1: TMemo
          Tag = 10
          Left = 0
          Top = 0
          Width = 926
          Height = 99
          Align = alClient
          BevelOuter = bvNone
          Color = clBlack
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssBoth
          TabOrder = 0
          WantTabs = True
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 926
        Height = 611
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel5'
        TabOrder = 1
        object Splitter2: TSplitter
          Left = 565
          Top = 0
          Width = 5
          Height = 611
          Cursor = crHSplit
          Align = alRight
          Beveled = True
          Color = clTeal
          ParentColor = False
        end
        object Panel6: TPanel
          Left = 570
          Top = 0
          Width = 356
          Height = 611
          Align = alRight
          BevelOuter = bvNone
          Caption = 'Panel6'
          TabOrder = 0
          object Memo2: TMemo
            Tag = 10
            Left = 0
            Top = 0
            Width = 356
            Height = 611
            Align = alClient
            BevelOuter = bvNone
            Color = clBlack
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssBoth
            TabOrder = 0
            WantTabs = True
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 565
          Height = 611
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Panel7'
          TabOrder = 1
          object Memo3: TMemo
            Left = 0
            Top = 0
            Width = 565
            Height = 592
            Align = alClient
            BevelInner = bvNone
            BevelOuter = bvNone
            BorderStyle = bsNone
            Color = clNavy
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWhite
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            OEMConvert = True
            ParentFont = False
            ScrollBars = ssBoth
            TabOrder = 0
            WantTabs = True
            OnChange = Memo3Change
            OnKeyDown = Memo3KeyDown
            OnKeyPress = Memo3KeyPress
            OnMouseDown = Memo3MouseDown
            OnMouseMove = Memo3MouseMove
          end
          object StatusBar1: TStatusBar
            Left = 0
            Top = 592
            Width = 565
            Height = 19
            Color = clTeal
            Panels = <
              item
                Width = 200
              end
              item
                Width = 200
              end>
            SimplePanel = False
          end
        end
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 8
    Top = 512
    object File1: TMenuItem
      Caption = 'File'
      object Open1: TMenuItem
        Action = My_Open
      end
      object New1: TMenuItem
        Action = My_New
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Save1: TMenuItem
        Action = My_Save
      end
      object SaveAs1: TMenuItem
        Action = My_Save_As
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Close1: TMenuItem
        Action = My_Close
      end
    end
    object Edit1: TMenuItem
      Caption = 'Edit'
      object Undo1: TMenuItem
        Action = My_Undo
      end
    end
    object View1: TMenuItem
      Caption = 'View'
      object Close2: TMenuItem
        Action = My_Fonts
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object OEMCHARSET1: TMenuItem
        Action = My_OEM_CHARSET
        AutoCheck = True
      end
      object RUSSIANCHARSET1: TMenuItem
        Action = My_RUSSIAN_CHARSET
        AutoCheck = True
      end
    end
    object Run1: TMenuItem
      Caption = 'Run'
      object Run2: TMenuItem
        Action = My_Run
      end
    end
    object About1: TMenuItem
      Caption = 'About'
    end
  end
  object ActionList1: TActionList
    Left = 8
    Top = 408
    object My_Open: TAction
      Caption = 'Open'
      ShortCut = 16463
      OnExecute = My_OpenExecute
    end
    object My_Save: TAction
      Caption = 'Save'
      ShortCut = 16467
      OnExecute = My_SaveExecute
    end
    object My_Save_As: TAction
      Caption = 'Save As...'
      ShortCut = 16468
      OnExecute = My_Save_AsExecute
    end
    object My_Close: TAction
      Caption = 'Close'
      ShortCut = 16453
      OnExecute = My_CloseExecute
    end
    object My_Run: TAction
      Caption = 'Run'
      ShortCut = 16466
      OnExecute = My_RunExecute
    end
    object My_New: TAction
      Caption = 'New...'
      ShortCut = 16462
      OnExecute = My_NewExecute
    end
    object My_Undo: TAction
      Caption = 'Undo'
      ShortCut = 16469
      OnExecute = My_UndoExecute
    end
    object My_Fonts: TAction
      Caption = 'Fonts'
      ShortCut = 16454
      OnExecute = My_FontsExecute
    end
    object My_RUSSIAN_CHARSET: TAction
      AutoCheck = True
      Caption = 'RUSSIAN CHARSET'
      Checked = True
      GroupIndex = 1
      ShortCut = 16471
      OnExecute = My_RUSSIAN_CHARSETExecute
    end
    object My_OEM_CHARSET: TAction
      AutoCheck = True
      Caption = 'OEM CHARSET'
      GroupIndex = 1
      ShortCut = 16452
      OnExecute = My_OEM_CHARSETExecute
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'ASM Files|*.ASM|MACROS Files|*.MAC|all files|*.*'
    InitialDir = '.'
    Options = [ofShowHelp, ofPathMustExist, ofFileMustExist, ofCreatePrompt, ofEnableSizing]
    Left = 8
    Top = 440
  end
  object SaveDialog1: TSaveDialog
    Filter = 'ASM Files|*.ASM|MACROS Files|*.MAC|all files|*.*'
    Options = [ofShowHelp, ofPathMustExist, ofFileMustExist, ofCreatePrompt, ofEnableSizing]
    Left = 8
    Top = 544
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MinFontSize = 0
    MaxFontSize = 0
    Options = [fdForceFontExist, fdShowHelp]
    Left = 8
    Top = 472
  end
end
