//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <Dialogs.hpp>
#include <Mask.hpp>

#include "h\my.h"
#include "h\uum.h"



//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TPanel *Panel2;
        TMainMenu *MainMenu1;
        TMenuItem *File1;
        TMenuItem *Edit1;
        TMenuItem *View1;
        TMenuItem *About1;
        TActionList *ActionList1;
        TPanel *Panel3;
        TPanel *Panel4;
        TSplitter *Splitter1;
        TPanel *Panel5;
        TMemo *Memo1;
        TPanel *Panel6;
        TSplitter *Splitter2;
        TPanel *Panel7;
        TMemo *Memo2;
        TMemo *Memo3;
        TAction *My_Open;
        TAction *My_Save;
        TAction *My_Save_As;
        TAction *My_Close;
        TAction *My_Run;
        TAction *My_New;
        TMenuItem *Open1;
        TMenuItem *New1;
        TMenuItem *N1;
        TMenuItem *Save1;
        TMenuItem *SaveAs1;
        TMenuItem *N2;
        TMenuItem *Close1;
        TMenuItem *Run1;
        TMenuItem *Run2;
        TSpeedButton *SpeedButton1;
        TSpeedButton *SpeedButton2;
        TSpeedButton *SpeedButton3;
        TSpeedButton *SpeedButton4;
        TSpeedButton *SpeedButton5;
        TSpeedButton *SpeedButton6;
        TCheckBox *CheckBox1;
        TCheckBox *CheckBox2;
        TOpenDialog *OpenDialog1;
        TSaveDialog *SaveDialog1;
        TAction *My_Undo;
        TMenuItem *Undo1;
        TStatusBar *StatusBar1;
        TFontDialog *FontDialog1;
        TAction *My_Fonts;
        TMenuItem *Close2;
        TAction *My_RUSSIAN_CHARSET;
        TAction *My_OEM_CHARSET;
        TMenuItem *N3;
        TMenuItem *OEMCHARSET1;
        TMenuItem *RUSSIANCHARSET1;
        TCheckBox *CheckBox3;
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall My_CloseExecute(TObject *Sender);
        void __fastcall My_OpenExecute(TObject *Sender);
        void __fastcall My_UndoExecute(TObject *Sender);
        void __fastcall Memo3Change(TObject *Sender);
        void __fastcall Memo3KeyPress(TObject *Sender, char &Key);
        void __fastcall Memo3KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall Memo3MouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall Memo3MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall My_FontsExecute(TObject *Sender);
        void __fastcall My_RUSSIAN_CHARSETExecute(TObject *Sender);
        void __fastcall My_OEM_CHARSETExecute(TObject *Sender);
        void __fastcall My_RunExecute(TObject *Sender);
        void __fastcall My_NewExecute(TObject *Sender);
        void __fastcall My_SaveExecute(TObject *Sender);
        void __fastcall My_Save_AsExecute(TObject *Sender);
private:	// User declarations
public:		// User declarations


        UUM *run;



        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------



void __fastcall TForm1::Memo3Change(TObject *Sender)
{
TPoint curs;
curs=Memo3->CaretPos;
StatusBar1->Panels->operator [](1)->Text="Line: "+String(curs.y+1)+"\tCol: "+String(curs.x+1);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Memo3KeyPress(TObject *Sender, char &Key)
{
TPoint curs;
curs=Memo3->CaretPos;
StatusBar1->Panels->operator [](1)->Text="Line: "+String(curs.y+1)+"\tCol: "+String(curs.x+1);        
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Memo3KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
TPoint curs;
curs=Memo3->CaretPos;
StatusBar1->Panels->operator [](1)->Text="Line: "+String(curs.y+1)+"\tCol: "+String(curs.x+1);        
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Memo3MouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
TPoint curs;
curs=Memo3->CaretPos;
StatusBar1->Panels->operator [](1)->Text="Line: "+String(curs.y+1)+"\tCol: "+String(curs.x+1);        
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Memo3MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
TPoint curs;
curs=Memo3->CaretPos;
StatusBar1->Panels->operator [](1)->Text="Line: "+String(curs.y+1)+"\tCol: "+String(curs.x+1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::My_FontsExecute(TObject *Sender)
{
FontDialog1->Font=Memo3->Font;
if(FontDialog1->Execute())
        {
        Memo3->Font=FontDialog1->Font;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::My_RUSSIAN_CHARSETExecute(TObject *Sender)
{
Memo3->Font->Charset=RUSSIAN_CHARSET;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::My_OEM_CHARSETExecute(TObject *Sender)
{
Memo3->Font->Charset=OEM_CHARSET;        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::My_UndoExecute(TObject *Sender)
{
Memo3->Undo();        
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __fastcall TForm1::My_CloseExecute(TObject *Sender)
{
Close();        
}
//---------------------------------------------------------------------------








#endif
