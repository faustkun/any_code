#ifndef MY_H
#define MY_H



#include <dir.h>
#include <math.h>
#include <iostream.h>
#include <fstream.h>
#include <iomanip.h>
#include <stdlib.h>
















//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//interface of header my.h
template <class T> void swap(T *element1Ptr, T *element2Ptr);
String get_dir();
bool str_int(AnsiString temp);
bool str_float(AnsiString temp);
int Hex(AnsiString temp);
void Bitmap_to_Hex(AnsiString open, AnsiString save);
int f_size(AnsiString open);
bool StringToDouble(String string ,double *type);
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------




void __fastcall Dos_To_Win(TStringList* in_sl)
{
TStringList *out = new  TStringList;
out->Clear();
char Oem_s[256];
char Ansi_s[256];
String in_s;
for (int a=0;a<in_sl->Count;a++)
        {
        in_s=in_sl->operator [](a);
        strcpy (Oem_s,in_s.c_str());
        OemToChar(Oem_s,Ansi_s);
        out->Add(Ansi_s);
        }
in_sl->Clear();
in_sl->AddStrings(out);
delete out;
}
//---------------------------------------------------------------------------



void __fastcall Win_To_Dos(TStringList* in_sl)
{
TStringList *out = new  TStringList;
out->Clear();
char Oem_s[256];
char Ansi_s[256];
String in_s;
for (int a=0;a<in_sl->Count;a++)
        {
        in_s=in_sl->operator [](a);
        strcpy (Oem_s,in_s.c_str());
        CharToOem(Oem_s,Ansi_s);
        out->Add(Ansi_s);
        }

in_sl->Clear();
in_sl->AddStrings(out);
delete out;
}
//---------------------------------------------------------------------------
























bool __fastcall if_dir_empty(String Dir)
{
Sleep(1);
ffblk fd;
Dir=Dir+"*.*";
if (findfirst(Dir.c_str(),&fd,0)){return true;}
else {return false;}
}
//---------------------------------------------------------------------------


int __fastcall File_Size(String file_name)
{
HANDLE hFile;
DWORD out;
hFile =CreateFile(file_name.c_str(),READ_CONTROL,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
Sleep(1);
out=GetFileSize(hFile, NULL);
CloseHandle(hFile);
if (out==INVALID_FILE_SIZE){return -1;}
return out;
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
SHELLEXECUTEINFO info;

void __fastcall Run_And_Wait(String file_path,String dir,  bool Hide, HWND Handl)
{
info.lpDirectory=dir.c_str();
info.lpFile=file_path.c_str();
if (Hide){info.nShow=SW_HIDE;}
else {info.nShow=SW_SHOW;}
info.hwnd=Handl;
info.fMask=SEE_MASK_NOCLOSEPROCESS;
info.cbSize=sizeof(info);

ShellExecuteEx(&info);
WaitForSingleObject(info.hProcess,INFINITE);
}
//---------------------------------------------------------------------------











bool File_Exist(String file_name)
{
HANDLE hFile;
DWORD out;
hFile =CreateFile(file_name.c_str(),READ_CONTROL,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
Sleep(1);
out=GetFileSize(hFile, NULL);

if (out==INVALID_FILE_SIZE){return false;}
else if (out==0){return false;}
return true;
}



//---------------------------------------------------------------------------
template <class T>
void swap(T *element1Ptr, T *element2Ptr)
{
        T temp = *element1Ptr;
        *element1Ptr = *element2Ptr;
        *element2Ptr = temp;
}
//---------------------------------------------------------------------------

String get_dir()
{
        String status;
        char path[MAXPATH];
        strcpy(path, "X:\\");
        path[0] = 'A' + getdisk();
        getcurdir(0, path+3);
        status=path;
        return status;
}

//---------------------------------------------------------------------------

bool str_int(AnsiString temp)
{
if (temp!="")
        {
        bool status;
        char temp_c[1];
        for (int a=1;a<=temp.Length();a++)
                {
                strcpy(temp_c,AnsiString(temp.SubString(a,1)).c_str());
                if      (
                        ((a==1)&&(temp_c[0]==45))||
                        ((temp_c[0]>=48)&&(temp_c[0]<=57))
                        ){status=true;}

                else {status=false;a=temp.Length();}
                }
        return status;
}
else {return false;}
}

//---------------------------------------------------------------------------

bool str_float(AnsiString temp)
{
if (temp!="")
        {
        int temp_int=0;
        bool status;
        char temp_c[1];
        for (int a=1;a<=temp.Length();a++)
                {
                strcpy(temp_c,AnsiString(temp.SubString(a,1)).c_str());
                if ((a==1)&&(temp_c[0]==45)){temp_int=1;}
                if      (
                        ((a==1)&&(temp_c[0]==45))||
                        ((temp_c[0]>=48)&&(temp_c[0]<=57))||
                        ((a>1+temp_int)&&(a<temp.Length())&&(temp_c[0]==44))
                        ){status=true;}
                else {status=false;a=temp.Length();}
                }
        return status;
}
else {return false;}
}

//---------------------------------------------------------------------------

int Hex(AnsiString temp)
{
int tin=0;
AnsiString simvol="";
for (int a=1;a<=temp.Length();a++)
        {
        simvol=temp.SubString(a,1);
        if ((simvol=="A")||(simvol=="a")){simvol="10";}
        if ((simvol=="B")||(simvol=="b")){simvol="11";}
        if ((simvol=="C")||(simvol=="c")){simvol="12";}
        if ((simvol=="D")||(simvol=="d")){simvol="13";}
        if ((simvol=="E")||(simvol=="e")){simvol="14";}
        if ((simvol=="F")||(simvol=="f")){simvol="15";}
        tin=tin+pow( 16,(temp.Length()-a) )*StrToInt(simvol);
        }

return tin;
}

//---------------------------------------------------------------------------
void Bitmap_to_Hex(AnsiString open, AnsiString save)
{
Graphics::TBitmap *Bitmap;
Bitmap = new Graphics::TBitmap();
Bitmap->LoadFromFile(open);
ofstream runfile(save.c_str() , ios::out);
for (int a=0;a<Bitmap->Height;a++)
        {
        for(int b=0;b<Bitmap->Width;b++)
                {
                AnsiString temp=IntToHex(Bitmap->Canvas->Pixels[b][a], 6);
                runfile << temp.c_str();
                }
        runfile <<endl;
        }
runfile.close();
}


//---------------------------------------------------------------------------
int f_size(AnsiString open)
{
ofstream runfile(open.c_str() , ios::in);
runfile.seekp(0, ios::end);
return runfile.tellp();
}



//---------------------------------------------------------------------------
bool StringToDouble(String string ,double *type)
{
bool status=false;
char temp[1];
int kol=0;
int poz=0;
bool otr=false;


int* leng = new int;
*leng=0;

*type=0;

*leng=string.Length();
if (string.SubString(1,1)=="-"){otr=true;string=string.SubString(2,*leng-1);*leng--;}

        for (int a=1;a<=*leng;a++)
                {
                strcpy(temp,String(string.SubString(a,1)).c_str());
                switch (temp[0])
                        {
                        case '1': status=true;break;
                        case '2': status=true;break;
                        case '3': status=true;break;
                        case '4': status=true;break;
                        case '5': status=true;break;
                        case '6': status=true;break;
                        case '7': status=true;break;
                        case '8': status=true;break;
                        case '9': status=true;break;
                        case '0': status=true;break;
                        case '.': kol++;break;
                        case ',': kol++;break;
                        default : status=false;break;
                        }
                if (kol>1){status=false;break;}
                if ((poz==0)&&(kol==1)){poz=a;status=true;}
                }
if ( (string.SubString(*leng,1)==".") || (string.SubString(*leng,1)==",")){status=false;}
if (kol==1){string=String(string.SubString(1,poz-1)+string.SubString(poz+1,*leng));*leng--;}


if (status)
        {
        for (int a=1;a<=*leng;a++)
                {
                strcpy(temp,String(string.SubString(a,1)).c_str());
                switch (temp[0])
                        {
                        case '0': *type=*type+pow(10,*leng-a)*0;break;
                        case '1': *type=*type+pow(10,*leng-a)*1;break;
                        case '2': *type=*type+pow(10,*leng-a)*2;break;
                        case '3': *type=*type+pow(10,*leng-a)*3;break;
                        case '4': *type=*type+pow(10,*leng-a)*4;break;
                        case '5': *type=*type+pow(10,*leng-a)*5;break;
                        case '6': *type=*type+pow(10,*leng-a)*6;break;
                        case '7': *type=*type+pow(10,*leng-a)*7;break;
                        case '8': *type=*type+pow(10,*leng-a)*8;break;
                        case '9': *type=*type+pow(10,*leng-a)*9;break;
                        }
                }
        if (otr){*type=*type*(-1);}
        if (kol==1){*type=*type*pow(10,-(*leng-poz+1));}
        status=true;
        }
delete leng;
return status;
}
//---------------------------------------------------------------------------


#endif
