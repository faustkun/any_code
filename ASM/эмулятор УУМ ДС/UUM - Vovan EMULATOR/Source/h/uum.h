#ifndef UUM_H
#define UUM_H



class UUM
{
public:
        UUM(HANDLE Hand, TMemo* Edit1, TMemo* Out1, TMemo* Info1);
        ~UUM();
        void __fastcall Run(bool pause, bool debug, bool macro);
        void __fastcall Load(String real_file_name1);
        void __fastcall Save();
        void __fastcall Save_As(String new_file_name, String Extention);
        void __fastcall New(String new_file_name, String Extention);


private:
        void  __fastcall Set_extention(String *in, String ext);
        HANDLE Handle;

        TMemo* Edit;
        TMemo* Out;
        TMemo* Info;

        String real_file_name;
        String current_dir;
        String s1,s2;

};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


UUM::UUM(HANDLE Hand, TMemo* Edit1, TMemo* Out1, TMemo* Info1)
{
current_dir=get_dir();
if (current_dir!=""){current_dir=current_dir+"\\";}

Handle=Hand;
Edit=Edit1;
Out=Out1;
Info=Info1;

s1="";
s2="";
real_file_name="";
}
//---------------------------------------------------------------------------


UUM::~UUM()
{
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------



void __fastcall UUM::Load(String real_file_name1)
{
real_file_name=real_file_name1;

if(real_file_name==""){return;}
if(current_dir==""){return;}

//������� Memo-��
Edit->Clear();
Info->Clear();
Out->Clear();

//������� �����
Run_And_Wait(current_dir+"bin\\del.bat",current_dir+"bin\\",true,Handle);

//�������� ����� � loads
TStringList *loads = new  TStringList;
loads->LoadFromFile(real_file_name);

//�������������� DOS - ����� � ��������� WIN
Dos_To_Win(loads);

//�������� loads � Memo
Edit->Lines->AddStrings(loads);

//����� ���������� ����
Info->Lines->Clear();
Info->Lines->Add("��� �����:\t\t\t"+real_file_name);
Info->Lines->Add("������� ����� ���������:\t"+current_dir);

delete loads;
}
//---------------------------------------------------------------------------


void __fastcall UUM::Run(bool pause, bool debug, bool macro)
{
if(real_file_name==""){return;}
if(current_dir==""){return;}
Out->Clear();

//������� �����
Run_And_Wait(current_dir+"bin\\del.bat",current_dir+"bin\\",true,Handle);

//����������� � ���������� � ��������� DOS
TStringList *loads = new  TStringList;
loads->AddStrings(Edit->Lines);
Win_To_Dos(loads);
s1=current_dir+"temp\\temp1.asm";
loads->SaveToFile(s1);






//�������� � ������ ��� ����
loads->Clear();
loads->Add("@echo off");

if (macro)
        {
        loads->Add("ren temp1.asm temp1.mac");
        loads->Add("..\\bin\\UUMAC /Comment; temp1.mac temp1.asm  >out0.txt");
        }

loads->Add("..\\bin\\UASM temp1.asm temp.obj >out1.txt");
loads->Add("..\\bin\\UUMLINK temp.obj >out2.txt");
s1=current_dir+"temp\\temp.bat";
loads->SaveToFile(s1);
Run_And_Wait(s1,current_dir+"temp\\",true,Handle);


//�������� Out-��
if (!macro)
        {
        s1=current_dir+"temp\\out1.txt";
        loads->LoadFromFile(s1);
        Out->Lines->Add("-----------------------------------");
        Out->Lines->Add("UASM Zone");
        Out->Lines->Add("-----------------------------------");
        Out->Lines->Add("");
        Out->Lines->AddStrings(loads);

        s1=current_dir+"temp\\out2.txt";
        loads->LoadFromFile(s1);
        Out->Lines->Add("");
        Out->Lines->Add("");
        Out->Lines->Add("-----------------------------------");
        Out->Lines->Add("UUMLINK Zone");
        Out->Lines->Add("-----------------------------------");
        Out->Lines->Add("");
        Out->Lines->AddStrings(loads);
        }

        else
        {
        s1=current_dir+"temp\\out0.txt";
        loads->LoadFromFile(s1);
        Dos_To_Win(loads);
        Out->Lines->Add("-----------------------------------");
        Out->Lines->Add("UUMAC Zone");
        Out->Lines->Add("-----------------------------------");
        Out->Lines->Add("");
        Out->Lines->AddStrings(loads);

        s1=current_dir+"temp\\out1.txt";
        loads->LoadFromFile(s1);
        Out->Lines->Add("");
        Out->Lines->Add("");
        Out->Lines->Add("-----------------------------------");
        Out->Lines->Add("UASM Zone");
        Out->Lines->Add("-----------------------------------");
        Out->Lines->Add("");
        Out->Lines->AddStrings(loads);

        s1=current_dir+"temp\\out2.txt";
        loads->LoadFromFile(s1);
        Out->Lines->Add("");
        Out->Lines->Add("-----------------------------------");
        Out->Lines->Add("UUMLINK Zone");
        Out->Lines->Add("-----------------------------------");
        Out->Lines->Add("");
        Out->Lines->AddStrings(loads);
        }


//�������� run.bat � ������ uum
s1=current_dir+"temp\\run.bat";
loads->Clear();
loads->Add("@echo off");
loads->Add("cls");
if (debug){loads->Add("..\\bin\\UUMDC temp.uum /d");}
else {loads->Add("..\\bin\\UUMDC temp.uum");}
if (pause){loads->Add("pause");}
loads->SaveToFile(s1);
s2=current_dir+"temp\\";
ShellExecute(Handle,"open",s1.c_str(),"",s2.c_str(),SW_RESTORE);

delete loads;
}
//---------------------------------------------------------------------------

void __fastcall UUM::Save()
{
if(real_file_name==""){return;}
if(current_dir==""){return;}

//����������� � ���������� � ��������� DOS
TStringList *loads = new  TStringList;
loads->AddStrings(Edit->Lines);
Win_To_Dos(loads);
loads->SaveToFile(real_file_name);

delete loads;
}
//---------------------------------------------------------------------------


void __fastcall UUM::Save_As(String new_file_name, String Extention)
{
if(real_file_name==""){return;}
if(current_dir==""){return;}
if(new_file_name==""){return;}

//��������� � ��������� ����������
Set_extention(&new_file_name,Extention);

//����������� � ���������� � ��������� DOS
TStringList *loads = new  TStringList;
loads->AddStrings(Edit->Lines);
Win_To_Dos(loads);

loads->SaveToFile(new_file_name);
Load(new_file_name);

delete loads;
}
//---------------------------------------------------------------------------


void __fastcall UUM::New(String new_file_name, String Extention)
{
if(current_dir==""){return;}
if(new_file_name==""){return;}

//��������� � ��������� ����������
Set_extention(&new_file_name, Extention);

//���������� ������� �����
Save();

//�������� ������� �����
TStringList *loads = new  TStringList;
loads->Clear();
loads->SaveToFile(new_file_name);

//�������� ������� �����
Load(new_file_name);

delete loads;
}
//---------------------------------------------------------------------------




void __fastcall UUM::Set_extention(String* in, String ext)
{
String temp="";
String temp1=*in;
if (temp1==""){return;}
if (temp1.Length()<4){return;}

if (ext==""){ext="asm";}

temp=temp1.SubString(temp1.Length()-2,3);
temp=temp.LowerCase();

if (temp=="asm"){return;}
else if (temp=="mac"){return;}

*in=temp1+"."+ext;


}
//---------------------------------------------------------------------------

#endif
