
lab6    start   0
;----------------------------------------------------------------



;----------------------------------------------------------------

vivod	macro
        clear	a
        clear	x
		addr	t,b
		rmo	x,t
		
str1	ldch	0,x
		wd	#0
		tixr	b
		jlt	str1
		mend	

;----------------------------------------------------------------
obrmas	macro	&mass,&raz
		j       j1
		
temp 	word	0
oldl	word	0
		
j1		stl		oldl
		clear		b
		clear       t
		clear       x
		ldt		#2
		
;		extref	mass,raz,intout
main2	stb		temp
         
		ldb		mass,X;?????
		intout
		lda		#32
		wd		#0
		ldb		temp
		lda		mass,X
		comp	#0
		jgt		plus
		j		prover

plus	addr	a,b
		j		prover

prover	addr	t,x
		tix		raz
		jlt		main2
		lda		#0
		ldl		oldl
		mend

;----------------------------------------------------------------
intout  macro
		j		j2
		
oldb	word	0
olda	word	0
oldx	word	0
oldt	word	0
olds	word	0
str    byte   0,0,0,0,0

j2		sta		olda
		stb		oldb       
        stx    oldx
        sts    olds
        stt    oldt
        clear  	x
		clear	a
		;clear	b
		clear	t
		clear	s
		
        ldt    #10 
        rmo    a,b
        comp   #0
        jlt    neg
        j      listart
neg     clear  b
        subr   a,b
        lda    #45
        wd     x'01'
        j      listart
        
lintout tix    #0
listart rmo    a,b
        divr   t,b
        mulr   t,b
        subr   b,a
        lds    #48
        addr   s,a
        stch   str,x       
        divr   t,b
        lds    #0
        compr  b,s
        jgt    lintout
        lds    #-1
        ldt    #0
liprint ldch   str,x
        wd     x'01'
        addr   s,x
        compr  x,t
        jlt    liexit
        j      liprint
liexit  ldx    oldx
        lds    olds
        ldt    oldt
        ;clear 	b
        ldb	oldb
		lda	olda
        
		mend


;----------------------------------------------------------------
choice	macro	&kon1,&dkon1,&kon4,&dkon4
;		extref	kon1,dkon1,kon4,dkon4,intout
		j		j3
		
temp	word	0	
oldl	word	0

j3		stb		temp
		lda 	temp
		comp	#0
		ldx		#0
		lds		#dkon1
		jeq		case1; ��v� ����� ��v�
		lds		#dkon4
		j		case4; ��v� ����-� ����
		
		case1	lda		#0
		ldch	kon1,X; ��� ��v�����v��v�
		wd		#0
		tixr	S
		jlt		case1
		j		j4
		
		case4	lda		#0
		ldch	kon4,X; � ��
		wd		#0
		tixr	S
		jlt		case4

case4_1	lda		#0
		lda 	temp
		add 	#48
		stl		oldl
		intout
		ldl		oldl
j4
		mend
		
;----------------------------------------------     ���� ���ᨢ�
getmas	macro	&raz, &znak, &num1, &vgl
		j		j4
;znak	word	1
;num1	word	0
;vgl		word	0

j4		stl		vgl
		clear	x
;		extref	raz,toarr
;		extdef	num1,znak

g5		clear	s
		clear	t

		lda		#1
		sta		znak
		clear	a
		sta		num1

g2		clear	a
		rd		#248
		
		comp	#'-'         ; �᫨ -
		jeq		g1

		comp	#' '        ; �᫨ �஡��
		jeq		g3
		
		comp	#13         ; si enter
		jeq		g4
		
		comp	#'0'
		jlt		g2
		comp	#'9'
		jgt		g2
		
		lds		#2
		compr	t,s
		jeq		g2
		
		comp	#'0'
		jgt		g6
		clear	s
		compr	t,s
		jeq		g7
		
g6		ldt		#1
		
		sub		#48        ; �᫨ �᫮
		rmo		s,a
		lda		num1
		rmo		b,a
		mul		#10
		addr	s,a
		compr	a,b
		jlt		g2
		sta		num1
		rmo		a,s
		add		#48
		wd		#0
		j		g2
		
g7		lda		znak
		comp	#0
		jlt		g2
		ldt		#2
		lda		#'0'
		wd		#0
		j		g2
		
g1		lda		znak
		comp	#0
		jlt		g2          ; ����. ��譥�� �����
		clear	s
		compr	t,s
		jgt		g2          ; ����. ����� �᫨ �� �� � ��砫�
		lda		#-1
		sta		znak
		lda		#'-'
		wd		#0
		j		g2
		
g3		clear	s
		compr	t,s
		jeq		g2        ; ����. �஡��, �᫨ �᫮ �� ������� ��� ������ ⮫쪮 -
		wd		#0
		toarr	&mass, &znak, &num1
		j		g5
		
g4		clear	s
		compr	t,s
		jeq		g2
		toarr	&mass, &znak, &num1
		stx		raz
		
		ldl		vgl
		

		mend
;---------------------------------------------  �������� � ���ᨢ
toarr	macro	&mass, &znak, &num1
;		extref	mass,znak,num1
		clear	a
		lds		znak
		ldt		num1
		mulr	s,t
		stt		mass,x
		tixr	a
		tixr	a
		tixr	a

		mend
;-------------------------------------------

logo	macro &login,&dlogin,&pass,&dpass,&log2,&log4  
		j	q0
     
;s       word    77
oldt	word	0
oldb	word	0
oldx	word	0
olda	word	0
olds	word	0
oldl	word	0
dlina	word	2
h		word	5	

q0	
	stt	oldt
	stb	oldb
	stx	oldx
	sta	olda
	sts	olds
	stl	oldl
	ldt		#login
	ldb		#dlogin
	vivod
	ldt	oldt
	ldb	oldb
	ldx	oldx
	lda olda
	lds	olds
	ldl	oldl
	
q1	clear	a
	rd	#248
	comp	#13
	jeq	qz
	sta	log2,x
	wd	#0
	tix	#0
	
	j	q1       
    
qz	stt	oldt
	stb	oldb
	stx	oldx
	sta	olda
	sts	olds
	stl	oldl
	ldt		#pass
	ldb		#dpass
	vivod
	ldt	oldt
	ldb	oldb
	ldx	oldx
	lda olda
	lds	olds
	ldl	oldl
	
q3	clear	a
	rd	#248
	comp	#13
	jeq	q4
	sta	log4,x
	tix	#0
	lda	#42
	wd	#0
	j	q3
		
q4	stx	log4
	clear	x
	clear	a
	clear	b
    
        clear	b
		clear 	x
		clear	s
		ldt     $log4
        lds     $log2
        clear   a
        rmo     a,s
      
l2      stx     oldx
        clear   X
l1      
        wd      #0
        tix     dlina
         addr    t,a
        jlt     l1
        
        sta     olda
        lda	#10
        wd	#0
        lda	#13
        wd	#0
        lda     olda
        
        ldb     dlina
        addr    t,b
        stb     dlina
                
        ldx     oldx
        tix     h
        jlt     l2
		
		clear	a
		clear	b
		clear	x
		clear	t

		mend

;-------------------------------------------

;		extref	vivod,obrmas,choice,getmas
;		extdef	mass,head,dhead,temp,kon1,dkon1,kon4,dkon4,raz
		
		;??????? ?????
		logo	login,dlogin,pass,dpass,log2,log4 
		ldt		#head
		ldb		#dhead
		vivod

run1	clear	a
		clear	b
		clear	s
		clear	x
		clear	l
		clear	t
		
		ldt		#vvm
		ldb		#dvvm
		vivod

		getmas	&raz, &znak, &num1, &vgl
		
		lda		#10
		wd		#0
		lda		#13
		wd		#0
		
		ldt		#outm
		ldb		#doutm
		vivod
		
		obrmas	mass,raz
		;?????? ? ????????
		lda		#10
		wd		#0
		lda		#13
		wd		#0
		choice	kon1,dkon1,kon4,dkon4
		lda		#10
		wd		#0
		lda		#13
		wd		#0
		
		ldt		#endl
		ldb		#dendl
		vivod
		
run2	rd		#248
		comp	#'Y'
		jeq		run1
		comp	#'y'
		jeq		run1
		comp	#'N'
		jeq		fin
		comp	#'n'
		jeq		fin
		j		run2
		
fin		+j		#x'FFFF'

;----------------------------------------------------------------

head	byte	'Lab_6',10,13,'Afanacev Anton ',10,13,'ITO-1-10',10,13,0
dhead	equ		*-head
kon1	byte	'Net polojitelnih eltmentov',10,13,0
dkon1	equ		*-kon1
kon2	byte	'Oshibka programmi',10,13,0
dkon2	equ		*-kon2
kon3	byte	'Summa polojitelnih >9',10,13,0
dkon3	equ		*-kon3
kon4	byte	'Summa polojitelnih =',0
dkon4	equ		*-kon4
vvm		byte	'Vvedite	massiv:',10,13,0
dvvm	equ		*-vvm
endl	byte	'Povtorim Y/N',10,13,0
dendl	equ		*-endl
outm	byte	'itogoviy massiv:',10,13,0
doutm	equ		*-outm
oldb	word	0
olda	word	0
oldx	word	0
oldt	word	0
olds	word	0
str    byte   0,0,0,0,0

raz		word	0
shift	word	48
nine	word	9
tree	word	3
temp 	word 	0
leng	word	0
res		word 	0		
nil		word	0
one 	word  	1		
Mass 	resw   	100
znak	word	0
num1	word	0

login	byte	'login:',0
dlogin	equ		*-login
pass	byte	' ',10,13,'pasword:',10,13,0
dpass	equ		*-pass
vgl		word	0
log2    resw   	100
log4    resw   	100  
