//---------------------------------------------------------------------------


#include <tchar.h>
#include <fstream>
#include "iostream"
#include <string>
#include <locale>
#include <vector>
#include "Rus.cpp"
#pragma argsused


using namespace std;
//using namespace System;
//using namespace System::IO;

const int kol_st = 200;
const int kol_lines = 15;
const int MAXFLOAT = 99999;

float max(float a, float b)
{
  return(a>b)? a:b;
}

class station;

class vetka
{
  string *name;
  station* s;
  int n;

  public:
  string *get_name();
  void set_name(string *nam);
  void add_stations(station *st, int n);
};

class station
{
  string *name;
  vetka *parent;
  public:
  void set_name(string *nam, vetka *parent);
  string *get_name();
  string *get_line_name();
};
// -------------- -------------------------------------
  void station::set_name(string *nam, vetka *parent)
  {
	name = new string(*nam);
	this->parent = parent;
  }

  string *station::get_name()
  {
	return name;
  }

  string *station::get_line_name()
  {
	return parent->get_name();
  }

// --------------------- -----------------------
string *vetka::get_name()
{
	return name;
}

void vetka::set_name(string *nam)
{
  name = new string(*nam, 5, nam->length());
}

void vetka::add_stations(station *st, int n)
{
  this->n = n;
  s = st;
}
// -----------------------
class Metro
{
  vetka *all_lines; // ��� �����
  
  float dist[kol_st][kol_st]; // ���������� ����� ���������

  int count_st; // ���������� �������
  int count_li; // ���������� �����

  int for_out;
// --------- functions ---------
  int *string_to_number(pair<string, string>  *names);
  friend void print_paths();
  void Add_Transfer(string *line1, string *station1, string *line2, string *station2, int time);

  public:
  float minmax; // ������� �� ����������� ������
  vector<int> min_paths; //����������� ���� ������������ ����
  station *all_station; // ������ ���� �������
  int *points;
  float clast[kol_st][kol_st];
  int count; // ���������� ������ ������� // ��� ��� ����� ��� ����������
  int used[kol_st];
   float min_cost[2*kol_st]; // ����������� ��������� ������
  int kur_kol; // ���������� ��������
  vector<int> paths;


	int get_kol_st()
  {
	return count_st;
  }
  string get_all_station()
  {
	for_out++;
	string tmp = *all_station[for_out-1].get_name() + ' ' + *all_station[for_out-1].get_line_name();
	return(tmp);
  }

  Metro()
  {
	for_out = 0;
	count_st = count_li = 0;
	all_station = new station[kol_st];
	all_lines   = new vetka[kol_lines];

	for(int i = 0; i < kol_st; i++)
	  for(int j = 0; j < kol_st; j++)
		dist[i][j] = MAXFLOAT;
	for(int i = 0; i < kol_st; i++) // �������������� ��������� ������
	  dist[i][i] = 0;
  }
  float min(float a, float b)
  {
	return a<b? a:b;
  }
// ---------------------------
  private:
  
  
  
 
  


  void count_cost(int p);
  void DFS(int u);  // ������ ����� � ������� � ������� ������������ ����

  public:
  void find_paths(pair<string, string> *for_vistit, int count, int kur);
  void input();

};


  void Metro::count_cost(int p)
  {
	if((count + kur_kol-1) != paths.size())
	  return;

	float cost[kol_st];
	for(int i = 0; i < paths.size(); i++)
	  cost[i] = 0;
	// = clast[0][points[p]];
	// ������� - �� ������� �� ����� ����� ����������
	int cir_kol = 0;
	for(int i = 0; i < paths.size(); i++)
	{
	  if(paths[i] == points[0])
		cir_kol++;
	  if(cir_kol > kur_kol)
		return;
	}

	int circle_kol = -1;//kur_kol;
	for(int i = 1; i < paths.size(); i++)
	{
	  if(paths[i-1] == points[0])
		circle_kol++;

	  cost[circle_kol] += clast[paths[i]][paths[i-1]];

	  if(cost[circle_kol] > minmax)
		return;
	}
	cost[circle_kol] += clast[points[0]][paths[paths.size()-1]]; // ��������� ��������� �������� ����� � ����� ��������� �����

	float local_max = -1;

	for(int i = 0; i < kur_kol; i++)
	  local_max = max(local_max, cost[i]);

	if(local_max < minmax)
	{
	  minmax = local_max;
	  memcpy(min_cost, cost, (paths.size() - kur_kol)*sizeof(int));
	  min_paths = paths;
	}
  }



  void Metro::DFS(int u)  // ������ ����� � ������� � ������� ������������ ����
  {
	if(u == 0)
	  count_cost(u);  //����� ����������� ��������� ����

	for(int v = 0; v < count; v++)
	{
	  if((u == 0) && (v == 0))
		continue;

	  if(used[v] == 0)
	  {
//		if(points[u] == 0)
//		  cout << "=(";
		paths.push_back(points[u]);
		used[v] = 1;
		used[0] = 0;
		DFS(v);
		used[v] = 0;
		paths.pop_back();
	  }
	}
  }


  void Metro::find_paths(pair<string, string> *for_vistit, int count, int kur)
  {
	kur_kol = kur;
	this->count = count;
	points = string_to_number(for_vistit); // ����� ����� ����� count
	memcpy(clast, dist, sizeof(dist));

	for(int k = 0; k < count_st; k++)    // ���������� �������� ������ - � ���� �������������)
	  for(int i = 0; i < count_st; i++)
		for(int j = 0; j < count_st; j++)	
		  clast[i][j] = min(clast[i][j], clast[i][k] + clast[k][j]);
	

	for(int i = 0; i < count; i++)
	{
	  used[i] = 0;
	  min_cost[i] = MAXFLOAT;
	}
	minmax = MAXFLOAT;
//	paths.push_back(points[0]); // ������ ����� - � ����� �����������
  //	path[0] = -1;
	DFS(0); // ����� ������������ ����� ������������ ���� � ���������������� �����
  }

  int *Metro::string_to_number(pair<string, string>  *names)
  {
	int* ans = new int[kol_st];
	for(int k = 0; k < count; k++) // (names->empty() == false)
	{
	  bool flag = true;
	  for(int i = 0; i < count_st; i++)
	  {
		bool u2 = names[k].second == *all_station[i].get_name();
		bool u1 = names[k].first == *all_station[i].get_line_name();
		if(u1 && u2 )
		{
		  flag = false;
		  ans[k] = i;
		 // names->pop();
		  break;
		}
	  }
	  if(flag)
	  {
		cout << "bad input =(";
		//names->pop();
	  }
	}
	return ans;
  }

  void Metro::Add_Transfer(string *line1, string *station1, string *line2, string *station2, int time)
  {    // + ��������� ��� � �������
	//(*find_line(line1)).find_station(station1)    - ��� ����� ��� �� ������)
	int tmp1, tmp2;
	tmp1 = tmp2 = kol_st+1;
	for(int i = 0; i < count_st; i++)
	{
	  string* temp_s = all_station[i].get_name();
	  string* temp_l = all_station[i].get_line_name();

	  if((*temp_s == *station1) &&  (*temp_l == *line1))
	  {
		tmp1 = i;
	  }
	  if((*temp_s == *station2) &&  (*temp_l == *line2))
	  {
		tmp2 = i;
	  }
	}
	if((tmp1 != kol_st+1) && (tmp2 != kol_st+1)) // �������� �� ���������� ���� - ����� ��������
	{
	  dist[tmp1][tmp2] = time;
	  dist[tmp2][tmp1] = time;
	}
	else
	  cout << "Critical Eror!";
  }

  void Metro::input()
  {
	ifstream f;
	//f.open(GetCurrentDirectory) + 'Metro.txt');
  //	cout << (ExtractFilePath(Application->ExeName).c_str() + char("Metro.txt"));
	string Dir =  /*Directory::GetCurrentDirectory() + */"Metro.txt";
	f.open(Dir.c_str());
	string a;
	if(f) // ���� ���� ������ ������
	{
	  int n;
	  f >> n;
	  for(count_li = 0; count_li < n; count_li++)  // ���� �� ���� ������
	  {
		string tmp;
		f >> tmp;
		all_lines[count_li].set_name(&tmp); // c������ ��� �����
		int old_count_st = count_st;
		do         // ����� ��� �������� ��� �������� ��� �����...
		{
		  f >> tmp;
		  if(tmp == "Driving=")
			break;
		  all_station[count_st].set_name(&tmp, &all_lines[count_li]); // ������������� ��� � �����
		  count_st++;
	 //	  all_lines[count_li].add_station(...);
		}
		while(tmp != "Driving=");
		all_lines->add_stations(&all_station[old_count_st], count_st - old_count_st);
		// ������ - ������� ��������� ����� ���������.
		for(int i = old_count_st; i < count_st-1; i++)
		{
		  f >> dist[i][i+1];
		  dist[i+1][i] = dist[i][i+1];

		  if(dist[i][i+1] == 0)
		  {
			dist[i][i+1] = MAXFLOAT;
			dist[i+1][i] = MAXFLOAT;
		  }
		}
	  }
	  // ������ ��������� ���������.
	  while(f)
	  {
		string line1, station1, line2, station2;
		float time;
		f >> line1 >> station1 >> line2 >> station2 >> time;

		if( (line1 == "") && (line1 == station1) && (line2 == station2))
		  break;

		Add_Transfer(&line1, &station1, &line2, &station2, time);
	  }
	}
	else // ToDO - ���������� ���������� ��������
	{}
  }


//---------------------------------------------------------------------------
