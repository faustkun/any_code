// tr5.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <ctime>
#include <ppl.h>
#include <ppltasks.h>


#define N 5

using namespace std;
using namespace concurrency;

int res=0;
int res1=0;
int res2=0;
int res3=0;
int res4=0;
int res5=0;
int res6=0;
int res7=0;

int x[N][N];

void f1(int a, int &res)
{
  for(int i=0;i< N;i++)
  for(int j=0;j< N;j++)
  for(int k=0;k< N;k++)
  {
	  if (( ( a&( 1<< (N*j + i))   )&&(  a&(1<< (N*k+j))  )  )&&( ( a&( 1<< (N*k + i)    ))==0))
			  return;
  }

  res++;
}



void paralel(int start, int end, int &res)
{

for(int a=start; a < end; a++)
	  f1(a, res);
}


int _tmain(int argc, _TCHAR* argv[])
{
   
	cout<<"computing..."<<'\n';
		//154 302



	

		//  2^25 = 33�554�432
		//  2^36 = 68�719�476�736
  
 /*concurrency::parallel_invoke(
   [&] { paralel(0,8388607,res);},
   [&] { paralel(8388608,16777215,res1);},
   [&] { paralel(16777216,25165821,res2);},
   [&] { paralel(25165822,33554432,res3);});*/
	
	int q=4194304;
concurrency::parallel_invoke(
   [&] { paralel(0,q-1,res);},
   [&] { paralel(q,2*q-1,res1);},
   [&] { paralel(2*q,3*q-1,res2);},
   [&] { paralel(3*q,4*q-1,res3);},
   [&] { paralel(4*q,5*q-1,res4);},
   [&] { paralel(5*q,6*q-1,res5);},
   [&] { paralel(6*q,7*q-1,res6);},
   [&] { paralel(7*q,8*q,res7);});

	
	res+=res1+res2+res3+res4+res5+res6+res7;
	cout<<res-1;

	cout <<endl<< "Time: " << double(clock()) / CLOCKS_PER_SEC << " sec." << endl;

	cin.get();
	return 0;
}
