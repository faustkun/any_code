#ifndef MD5_H
#define MD5_H
#include "stdafx.h"
#include "iostream"
#include <cmath>
#include "string"
#include "fstream"

using namespace std;

typedef unsigned int uint;

class Mymd5
{
private:
	/*������� ����������*/
	uint funcF(uint x,  uint y, uint z);
	uint funcG(uint x,  uint y, uint z);
	uint funcH(uint x,  uint y, uint z);
	uint funcI(uint x,  uint y, uint z);
	/*�������������� �������*/
	void create_mass_const(uint T[64]);
	uint shift(uint x, int shift);
	void init(uint &A, uint &B, uint &C, uint &D);
	string hex_out(uint A);
	unsigned char* first_n_second_steps(string IN, int &size);
public:
	string md5(string input);
	int count_textfile(char * in);
	string *md5_table(char * in);
	int *mas_identical(char *in);
	int sravn(string str1,string str2);
};

/*������� ����������*/

uint Mymd5::funcF(uint x,  uint y, uint z)
{
	return (x&y)|((~x)&z);
}

uint Mymd5::funcG(uint x,  uint y, uint z)
{
	return (x&z)|((~z)&y);
}

uint Mymd5::funcH(uint x,  uint y, uint z)
{
	return x^y^z;
}

uint Mymd5::funcI(uint x,  uint y, uint z)
{
	return y^((~z)|x);
}

/*�������������� �������*/

void Mymd5::create_mass_const(uint T[64])
{
	for(int i = 1; i<=64; i++)
		{
			T[i-1]=4294967296*abs(sin((double)i));
		}	

}

uint Mymd5::shift(uint x, int shift)
{
return x << shift | x >> (32-shift);
}

void Mymd5::init(uint &A, uint &B, uint &C, uint &D)
{
	A=0x67452301;
	B=0xefcdab89;
	C=0x98badcfe;
	D=0x10325476;
}

string Mymd5::hex_out(uint A)
{
	string Q;
	uint temp;
	char c;
	for(int i=0; i<8; i++)
	{
		temp=A%16;
		A=A/16;
			switch (temp)
		{
			
			case 0:{c='0';break;}
			case 1:{c='1';break;}
			case 2:{c='2';break;}
			case 3:{c='3';break;}
			case 4:{c='4';break;}
			case 5:{c='5';break;}
			case 6:{c='6';break;}
			case 7:{c='7';break;}
			case 8:{c='8';break;}
			case 9:{c='9';break;}
			case 10:{c='a';break;}
			case 11:{c='b';break;}
			case 12:{c='c';break;}
			case 13:{c='d';break;}
			case 14:{c='e';break;}
			case 15:{c='f';break;}
		}
		Q=Q+c;
	}

	for(int i=0;i<8;i+=2)
	{
		c=Q[i];
		Q[i]=Q[i+1];
		Q[i+1]=c;
	}
	return Q;
}

unsigned char* Mymd5::first_n_second_steps(string IN, int &size)
{
	int length=IN.length();
	int rests=length%64;
	size=0;
if(rests < 56)
 size=length-rests+56+8;
else 
 size=length+64-rests+56+8;
 
unsigned char* result=new unsigned char[size];
 
for(int i=0;i<length;i++) //������ length ��������� IN
 result[i]=IN[i]; //��������� ��������� �������� ���������
 
result[length]=0x80; //����� � ����� ��������� ��������� ���.
 
for(int i=length+1;i<size;i++) //� ��� ���������
 result[i]=0; //��������� ������
__int64 bitLength=(uint)(length)*8; //����� ��������� � �����.
 
for(int i=0;i<8;i++) //��������� 8 ����
 result[size-8+i]=(bitLength >> i*8); //��������� 64-������ �������������� ����� ������ �� ������������
return result;
}
/*������� �������*/
string Mymd5::md5(string input)
{
	int size;
	//���� 1 � 2(���������� ����� � ���������� ������)
	unsigned char* temp=first_n_second_steps(input,size);
	//��� 3(������������� ����������,���������� ��������� �� ����� � �������� ������� ��������)
	uint A,B,C,D;
	init(A,B,C,D);
	
	uint T[64];
	create_mass_const(T);

	uint *X=new uint[size/4];
	X=(uint*)(temp);
	//��� 4(����������)
	uint AA,BB,CC,DD;
	
	for(int i=0;i<size/4;i+=16)
 {
	 AA = A;BB = B; CC = C; DD = D;
 
	 //����� 1
	 A = B + shift((A + funcF(B,C,D) + X[i+ 0] + T[ 0]),  7);
	 D = A + shift((D + funcF(A,B,C) + X[i+ 1] + T[ 1]), 12);
	 C = D + shift((C + funcF(D,A,B) + X[i+ 2] + T[ 2]), 17);
	 B = C + shift((B + funcF(C,D,A) + X[i+ 3] + T[ 3]), 22);
 
	 A = B + shift((A + funcF(B,C,D) + X[i+ 4] + T[ 4]),  7);
	 D = A + shift((D + funcF(A,B,C) + X[i+ 5] + T[ 5]), 12);
	 C = D + shift((C + funcF(D,A,B) + X[i+ 6] + T[ 6]), 17);
	 B = C + shift((B + funcF(C,D,A) + X[i+ 7] + T[ 7]), 22);
 
	 A = B + shift((A + funcF(B,C,D) + X[i+ 8] + T[ 8]),  7);
	 D = A + shift((D + funcF(A,B,C) + X[i+ 9] + T[ 9]), 12);
	 C = D + shift((C + funcF(D,A,B) + X[i+10] + T[10]), 17);
	 B = C + shift((B + funcF(C,D,A) + X[i+11] + T[11]), 22);
 
	 A = B + shift((A + funcF(B,C,D) + X[i+12] + T[12]),  7);
	 D = A + shift((D + funcF(A,B,C) + X[i+13] + T[13]), 12);
	 C = D + shift((C + funcF(D,A,B) + X[i+14] + T[14]), 17);
	 B = C + shift((B + funcF(C,D,A) + X[i+15] + T[15]), 22);
 
	 //����� 2
	 A = B + shift((A + funcG(B,C,D) + X[i+ 1] + T[16]),  5);
	 D = A + shift((D + funcG(A,B,C) + X[i+ 6] + T[17]),  9);
	 C = D + shift((C + funcG(D,A,B) + X[i+11] + T[18]), 14);
	 B = C + shift((B + funcG(C,D,A) + X[i+ 0] + T[19]), 20);
 
	 A = B + shift((A + funcG(B,C,D) + X[i+ 5] + T[20]),  5);
	 D = A + shift((D + funcG(A,B,C) + X[i+10] + T[21]),  9);
	 C = D + shift((C + funcG(D,A,B) + X[i+15] + T[22]), 14);
	 B = C + shift((B + funcG(C,D,A) + X[i+ 4] + T[23]), 20);
 
	 A = B + shift((A + funcG(B,C,D) + X[i+ 9] + T[24]),  5);
	 D = A + shift((D + funcG(A,B,C) + X[i+14] + T[25]),  9);
	 C = D + shift((C + funcG(D,A,B) + X[i+ 3] + T[26]), 14);
	 B = C + shift((B + funcG(C,D,A) + X[i+ 8] + T[27]), 20);
 
	 A = B + shift((A + funcG(B,C,D) + X[i+13] + T[28]),  5);
	 D = A + shift((D + funcG(A,B,C) + X[i+ 2] + T[29]),  9);
	 C = D + shift((C + funcG(D,A,B) + X[i+ 7] + T[30]), 14);
	 B = C + shift((B + funcG(C,D,A) + X[i+12] + T[31]), 20);
 
	 //����� 3
	 A = B + shift((A + funcH(B,C,D) + X[i+ 5] + T[32]),  4);
	 D = A + shift((D + funcH(A,B,C) + X[i+ 8] + T[33]), 11);
	 C = D + shift((C + funcH(D,A,B) + X[i+11] + T[34]), 16);
	 B = C + shift((B + funcH(C,D,A) + X[i+14] + T[35]), 23);
 
	 A = B + shift((A + funcH(B,C,D) + X[i+ 1] + T[36]),  4);
	 D = A + shift((D + funcH(A,B,C) + X[i+ 4] + T[37]), 11);
	 C = D + shift((C + funcH(D,A,B) + X[i+ 7] + T[38]), 16);
	 B = C + shift((B + funcH(C,D,A) + X[i+10] + T[39]), 23);
 
	 A = B + shift((A + funcH(B,C,D) + X[i+13] + T[40]),  4);
	 D = A + shift((D + funcH(A,B,C) + X[i+ 0] + T[41]), 11);
	 C = D + shift((C + funcH(D,A,B) + X[i+ 3] + T[42]), 16);
	 B = C + shift((B + funcH(C,D,A) + X[i+ 6] + T[43]), 23);
 
	 A = B + shift((A + funcH(B,C,D) + X[i+ 9] + T[44]),  4);
	 D = A + shift((D + funcH(A,B,C) + X[i+12] + T[45]), 11);
	 C = D + shift((C + funcH(D,A,B) + X[i+15] + T[46]), 16);
	 B = C + shift((B + funcH(C,D,A) + X[i+ 2] + T[47]), 23);

	 //����� 4
	 A = B + shift((A + funcI(B,C,D) + X[i+ 0] + T[48]),  6);
	 D = A + shift((D + funcI(A,B,C) + X[i+ 7] + T[49]), 10);
	 C = D + shift((C + funcI(D,A,B) + X[i+14] + T[50]), 15);
	 B = C + shift((B + funcI(C,D,A) + X[i+ 5] + T[51]), 21);
 
	 A = B + shift((A + funcI(B,C,D) + X[i+12] + T[52]),  6);
	 D = A + shift((D + funcI(A,B,C) + X[i+ 3] + T[53]), 10);
	 C = D + shift((C + funcI(D,A,B) + X[i+10] + T[54]), 15);
	 B = C + shift((B + funcI(C,D,A) + X[i+ 1] + T[55]), 21);
 
	 A = B + shift((A + funcI(B,C,D) + X[i+ 8] + T[56]),  6);
	 D = A + shift((D + funcI(A,B,C) + X[i+15] + T[57]), 10);
	 C = D + shift((C + funcI(D,A,B) + X[i+ 6] + T[58]), 15);
	 B = C + shift((B + funcI(C,D,A) + X[i+13] + T[59]), 21);
 
	 A = B + shift((A + funcI(B,C,D) + X[i+ 4] + T[60]),  6);
	 D = A + shift((D + funcI(A,B,C) + X[i+11] + T[61]), 10);
	 C = D + shift((C + funcI(D,A,B) + X[i+ 2] + T[62]), 15);
	 B = C + shift((B + funcI(C,D,A) + X[i+ 9] + T[63]), 21);
 
	 A = AA + A;
	 B = BB + B;
	 C = CC + C;
	 D = DD + D;
}
	delete []X;
	

	//��� 5(������ ����� ������ ����������)
	string s;
	s=hex_out(A)+hex_out(B)+hex_out(C)+hex_out(D);
	/*cout<<endl<<s<<endl;*/
	return s;
}

int Mymd5::count_textfile(char * in)
{
	ifstream fin;
	fin.open(in,ios::in);
		int str=0;
	while( !fin.eof() ) if( fin.get() == '\n' ) str++;
	fin.close();
	return str;
};

string* Mymd5::md5_table(char * in)
{	
	char q;
	int size=count_textfile(in);
	string *Q=new string[size];
	/*string r;*/
	for(int i=0;i<size;i++)
		Q[i]="";
	ifstream fin;
	fin.open(in,ios::in);
	for(int i=0;i<size;i++)
	{	fin.get(q);
		while(q/*=fin.get()*/!='\n')
		{Q[i]=Q[i]+q;fin.get(q);}
		/*r=Q[i];*/
		Q[i]=md5(Q[i]);
	}
	fin.close();
	return Q;
};

int* Mymd5::mas_identical(char *in)
{	
	int size=count_textfile(in);
	string *Q=md5_table(in);
	int *Res=new int[size];
	for(int k=0;k<size;k++)
		Res[k]=-1;
	for(int f=0;f<size;f++)
		if(Res[f]==-1)
			for(int q=f+1;q<size;q++)
				if(Q[q]==Q[f])
					Res[q]=f;
	for(int f=0;f<size;f++)
		Q[f].~string();
	return Res;
};

int Mymd5::sravn(string str1,string str2)
{
	if(md5(str1)==md5(str2))
		return 1;
	return 0;
};



#endif // MD5_H