#include "stdafx.h"
#include "md5.cpp"
#include "iostream"
#include "fstream"
#include "string"

using namespace std;

int count_textfile(char * in)
{
	ifstream fin;
	fin.open(in,ios::in);
		int str=0;
	while( !fin.eof() ) if( fin.get() == '\n' ) str++;
	fin.close();
	return str;
};
string *md5_table(char * in)
{	
	char q;
	int size=count_textfile(in);
	string *Q=new string[size];
	for(int i=0;i<size;i++)
		Q[i]="";
	ifstream fin;
	fin.open(in,ios::in);
	for(int i=0;i<size;i++)
	{
		while(q=fin.get()!='\n')
			Q[i]=Q[i]+q;
		Q[i]=md5(Q[i]);
	}
	fin.close();
	return Q;
};
int *mas_identical(char *in)
{	
	int size=count_textfile(in);
	string *Q=md5_table(in);
	int *Res=new int[size];
	for(int k=0;k<size;k++)
		Res[k]=-1;
	for(int f=0;f<size;f++)
		if(Res[f]==-1)
			for(int q=f+1;q<size;q++)
				if(Q[q]==Q[f])
					Res[q]=f;
	return Res;
};