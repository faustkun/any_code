/**
 * Created by Антон on 07.09.2014.
 */
public class sort {
    public static void main(String[] args) {
        int len = 1000000;
        int[] arr1 = mas(len);
        int[] arr2 = new int[len];
        int[] arr3 = new int[len];
        for(int i  = 0; i < len; i++){
            arr2[i] = arr1[i];
            arr3[i] = arr1[i];
        }
        long timeout1= System.currentTimeMillis();
        bubleSort(arr1);
        timeout1 = System.currentTimeMillis() - timeout1;
        System.out.println("buble sort: " + ((float)timeout1/1000) + " seconds");

        long timeout2= System.currentTimeMillis();
        selectionSort(arr2);
        timeout2 = System.currentTimeMillis() - timeout2;
        System.out.println("selection sort: " + ((float)timeout2/1000) + " seconds");
        long timeout3= System.currentTimeMillis();
        quickSort(arr3,0,len-1);
        timeout3 = System.currentTimeMillis() - timeout3;
        System.out.println("quick sort: " + ((float)timeout3/1000) + " seconds");
    }

    private static int[] mas(int len/*длина массива*/) {
        int[] arr = new int[len];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random() * 100);
            //System.out.print(arr[i] + "  ");
        }
        //System.out.println();
        return arr;
    }

    private static void bubleSort(int[] mas){
        for(int i = 0; i < mas.length-1; i++){
            for(int j = 0; j < mas.length-1; j++){
                if (mas[j] > mas[j+1]){
                    int temp = mas[j+1];
                    mas[j+1] = mas[j];
                    mas[j] = temp;
                }
            }
        }
        //for(int i = 0; i < mas.length; i++)
        //   System.out.print(mas[i] + "  ");
    }

    private static void quickSort(int[] mas,int l, int r){
        int i = l;
        int j = r;
        int x = mas[(int)(l+(r-l)/2)];
        while (i <= j) {
            while (mas[i] < x) {
                i++;
            }
            while (mas[j] > x) {
                j--;
            }
            if (i <= j) {
                int temp = mas[i];
                mas[i] = mas[j];
                mas[j] = temp;
                i++;
                j--;
            }
        }
        if (l<j){
            quickSort(mas, l, j);
        }
        if(i<r){
            quickSort(mas, i, r);
        }
    }

    private static void selectionSort(int[] mas){
        for(int i = 0; i < mas.length-1; i++){
            for(int j = i+1; j < mas.length; j++){
                if (mas[i] > mas[j]){
                    int temp = mas[j];
                    mas[j] = mas[i];
                    mas[i] = temp;
                }
            }

        /*System.out.print(i + "-я итерация:  ");
        for(int q = 0; q < mas.length; q++)
                System.out.print(mas[q] + "  ");
        System.out.println();*/

        }
        //for(int i = 0; i < mas.length; i++)
        // System.out.print(mas[i] + "  ");
    }

}
