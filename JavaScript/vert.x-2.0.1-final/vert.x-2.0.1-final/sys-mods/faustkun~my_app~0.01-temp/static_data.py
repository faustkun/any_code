from core.event_bus import EventBus

# Delete albums
def album_delete_handler(reply):

#   albums = [
#       {
#           'artist': 'The Wurzels',
#           'genre': 'Scrumpy and Western',
#           'title': 'I Am A Cider Drinker',
#           'price': 0.99
#       },
#       {
#           'artist': 'Vanilla Ice',
#           'genre': 'Hip Hop',
#           'title': 'Ice Ice Baby',
#           'price': 0.01
#       },
#       {
#           'artist': 'Ena Baga',
#           'genre': 'Easy Listening',
#           'title': 'The Happy Hammond',
#           'price': 0.50
#       },
#       {
#           'artist': 'The Tweets',
#           'genre': 'Bird related songs',
#           'title': 'The Birdy Song',
#           'price': 1.20
#       }
#   ]
    items = [
        {
            '_id': '1',
            'name': 'R1',
            'link': [],
			'info': 'information'
        },
        {
            '_id': '2',
            'name': 'R2',
            'link': [],
			'info': 'information'
        },
        {
            '_id': '3',
            'name': 'R3',
            'link': [],
			'info': 'information'
        },
        {
            '_id': '4',
            'name': 'M1',
            'link': ['1', '2'],
			'info': 'information'
        },
        {
            '_id': '5',
            'name': 'M2',
            'link': ['1', '2', '3'],
			'info': 'information'
        },
        {
            '_id': '6',
            'name': 'Q1',
            'link': ['4', '5', '1'],
			'info': 'information'
        }
	]

	

    # Insert albums - in real life 'price' would probably be stored in a different collection, but, hey, this is a demo.
    for i in range(0, len(items)):
        EventBus.send('vertx.mongopersistor', {
            'action': 'save',
            'collection': 'items',
            'document': items[i]
        })

EventBus.send('vertx.mongopersistor', {'action': 'delete', 'collection': 'items', 'matcher': {}}, album_delete_handler)


# Delete users
def user_delete_handler(reply):
    # And a user
    EventBus.send('vertx.mongopersistor', {
        'action': 'save',
        'collection': 'users',
        'document': {
            'firstname': 'Anton',
            'lastname': 'Afanasev',
            'email': 'Tosha1992G@mail.ru',
            'username': 'faustkun',
            'password': 'faustkun'
        }
    })
EventBus.send('vertx.mongopersistor', {'action': 'delete', 'collection': 'users', 'matcher': {}}, user_delete_handler)




