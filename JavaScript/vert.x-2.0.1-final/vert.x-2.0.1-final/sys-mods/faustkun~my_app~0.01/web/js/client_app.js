/*
 * Copyright 2011-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function DemoViewModel() {

  var that = this;
  var eb = new vertx.EventBus(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/eventbus');
 // console.log(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/eventbus');
  that.items = ko.observableArray([]);
  that.out = ko.observableArray([]);

  eb.onopen = function() {

    // Get the static data from mongodb

    eb.send('vertx.mongopersistor', {action: 'find', collection: 'items', matcher: {}, sort: {'_id': 1} },
      function(reply) {
        if (reply.status === 'ok') {
          var item_inp_Array = [];
          for (var i = 0; i < reply.results.length; i++) {
            item_inp_Array[i] = new Item(reply.results[i]);
			console.log(item_inp_Array[i]);
          }
          that.item_inp = ko.observableArray(item_inp_Array);
          //that.item_inp.sort(function(a, b){return (a._id>=b._id)? 1 : -1;});
		  ko.applyBindings(that);
        } else {
          console.error('Failed to retrieve items: ' + reply.message);
        }
      });
  };

  eb.onclose = function() {
    eb = null;
  };

  that.addToProject = function(item) {
    console.log("Adding to project: " + JSON.stringify(item));
    for (var i = 0; i < that.items().length; i++) {
      var compare = that.items()[i];
	  //var compare2 = that.out()[i];
      if (compare.item._id === item._id) {
        compare.quantity(compare.quantity() + 1);
		//compare2.quantity(compare2.quantity() + 1);
        return;
      }
    }
    that.items.push(new ProjectItem(item));
	//that.out.push(new ProjectItem(item));
  };

  that.removeFromProject = function(ProjectItem) {
    that.items.remove(ProjectItem);
  };
  
  
  that.monomering = function ()
{	//���� �������� ����� �� �����
	that.out.remove(function(item) { return 1 });
	console.log(that.items().length);
	for(var inp = 0; inp<that.items().length; inp ++)
	{console.log(that.items()[inp].item,that.items()[inp].quantity() );
	that.out.push(new ProjectItem(that.items()[inp].item, that.items()[inp].quantity()))}

	var flag = 1;
	console.log('start monomering');
	while(flag)
	{	flag = 0;//�������� ���� � �������� �������� out
		for(var i = 0; i<that.out().length; i++)
		{	//���� � out ���� �����, ��� � ���� ���� ���-�� � link 
			if (that.out()[i].item.link.length)
				{console.log('link not free');
				 flag = 1;
				 temp = that.out()[i];//�������� ������ � temp
				 console.log(temp.item.link);
				 that.out.remove(that.out()[i]);//������� �������� ������
				 //������� item_inp �� ������� ������ ������
				 for(var m = 0; m<temp.item.link.length; m++)
				 {  console.log('start search in item_inp: level 1');
					for (var q = 0; q < that.item_inp().length; q++)
						{   console.log('search in item_inp: level 2');
							var compare = that.item_inp()[q];
							if (compare._id ==  temp.item.link[m])//���� ������� ������, �� ��������� ��� � out
								{	console.log('start insert', temp.item.link[m]);
									var z = 0;
									for (z = 0; z < that.out().length; z++) 
									{
										var compare1 = that.out()[z];
										console.log(compare1.item._id, compare._id);
										
										if (compare1.item._id === compare._id) 
										{	console.log('update quantity');
											compare1.quantity(compare1.quantity() + 1*temp.quantity());
											z = that.out().length + 10;
										}
																				
									}
									console.log(z, that.out().length + 11);
									if(z!=that.out().length + 11){that.out.push(new ProjectItem(compare, temp.quantity()));console.log('update quantity2');}
								}																
						}					
				 }				 
				}
		}
	}
console.log(that.out());
}
  

  that.orderReady = ko.computed(function() {
    var or =  that.items().length > 0 && that.loggedIn;
    return or;
  });
  
  //that.orderSubmitted = ko.observable(false);

  that.resetLink = function (){
	var temp = '';
	
	for(var i = 0; i<that.items().length; i ++)
		{
			for(var z = 0; z <that.items()[i].quantity(); z++)
			{
				temp = temp + that.items()[i].item._id + " ";
			}
		}
	
	that.link_new(temp);
	 //ko.applyBindings(this);
	console.log(that.link_new());
  };
  
  that.saveItem = function() {

   // if (!orderReady()) {
   //   return;
   // }

    
    var orderMsg = {
      action: "save",
      collection: "items",
      document: {
        _id: that.article_new(),
        name: that.name_new(),
		link: that.link_new().split(' '),
		info: that.info_new()
      }
    }

    eb.send('vertx.mongopersistor', orderMsg, function(reply) {
      if (reply.status === 'ok') {
       // that.orderSubmitted(true);
        // Timeout the order confirmation box after 2 seconds
        // window.setTimeout(function() { that.orderSubmitted(false); }, 2000);
      } else {
        console.error('Failed to save');
      }
    });
	console.error(orderMsg.document);
	that.item_inp.push(new Item(orderMsg.document));
  };

  that.username = ko.observable('');
  that.password = ko.observable('');
  that.loggedIn = ko.observable(false);
  that.article_new = ko.observable('');
  that.name_new = ko.observable('');
  that.info_new = ko.observable('');
  that.link_new = ko.observable('');
  
  that.login = function() {
    if (that.username().trim() != '' && that.password().trim() != '') {
      eb.login(that.username(), that.password(), function (reply) {
        if (reply.status === 'ok') {
          that.loggedIn(true);
        } else {
          alert('invalid login');
        }
      });
    }
  }

  function Item(json) {
    var that = this;
    that._id = json._id;
    that.name = json.name;
    that.link = json.link;
	that.info = json.info;
  }

  function ProjectItem(item, quantity) {
    var that = this;
    that.item = item;
    if(quantity){that.quantity = ko.observable(quantity);}
	else {that.quantity = ko.observable(1);}
  }
})();