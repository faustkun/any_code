/**
 * Created by faustkun on 31.03.15.
 */
var listOfTemes = [];
Template.createTests.helpers({
    disciplines: function(){return Disciplines.find()}
});

Template.createTests.rendered = function(){
    listOfTemes = [];
    console.log(Template.instance().$('.discip')[0].value);
    var parent = Template.instance().$('.tem')[0];
    var temes = Disciplines.find({_id: Template.instance().$('.discip')[0].value}).fetch()[0].temes;
    //parent.size = temes.length;
    //console.log(temes);
    for(var i = 0; i < temes.length; i++){
        var option = document.createElement('option');
        option.value = temes[i];
        option.text = temes[i];
        parent.add(option);
    }


    checkNumber1 = function (e) {
        var s = parseInt(e.target.value + String.fromCharCode(e.charCode), 10);
        if(e.target.max < s || e.target.min > s){
            return false;
        }
        return (/[0-9]/.test(String.fromCharCode(e.charCode)));};
    checkNumber2 = function(e){
        var s = parseInt(e.target.value, 10);
        //console.log(s);
        if(e.target.max < s || e.target.min > s){
            e.target.value = 1;
            return false;
        }
    };
    var t = Template.instance().$('input[type="number"]');
    for (var i =0; i < t.length; i++){
        t[i].onkeypress = checkNumber1;
        t[i].onchange = checkNumber2;
    }
};

Template.createTests.events({
    'click .check_teme': function(){
        var parent = Template.instance().$(".temes")[0];
        var c = document.createElement("h5");
        var teme = Template.instance().$('.tem')[0];
        if(teme.options.length > 0){
            c.textContent = teme.value;
            listOfTemes[listOfTemes.length] = teme.value;

            for(var i = 0; i < teme.options.length; i++){
                if(teme.options[i].value == c.textContent){teme.remove(i);}
            }

            var s = "<i value = '" + c.innerText + "' class='fa fa-trash-o fa-1x delete' id = '" + listOfTemes.length + "'style='color:crimson'></i>";
            c.innerHTML = c.innerHTML + s;
            parent.appendChild(c);
            //console.log(listOfTemes);
        }else{
            console.log("Нечего вставлять");
        }

    },

    'change .discip': function(){
        //console.log("changed");
        //console.log(Template.instance().$('.discip')[0].value);
        listOfTemes = [];
        Template.instance().$(".temes").empty();
        Template.instance().$('.tem').find('option').remove().end();

        var parent = Template.instance().$('.tem')[0];
        var temes = Disciplines.find({_id: Template.instance().$('.discip')[0].value}).fetch()[0].temes;
        //console.log(temes);
        for(var i = 0; i < temes.length; i++){
            var option = document.createElement('option');
            option.value = temes[i];
            option.text = temes[i];
            parent.add(option);
        }
    },

    'click .delete': function(elem){
        //console.log(listOfTemes[elem.target.id - 1]);

        var select = Template.instance().$('.tem')[0];
        var option = document.createElement('option');
        option.value = listOfTemes[elem.target.id - 1];
        option.text = listOfTemes[elem.target.id - 1];
        select.add(option);

        listOfTemes.splice(elem.target.id - 1, 1);

        Template.instance().$(".temes").empty();
        var parent = Template.instance().$('.temes')[0];
        for(var i = 0; i < listOfTemes.length; i ++){
            var c = document.createElement("h5");
            c.textContent = listOfTemes[i];
            var s = "<i class='fa fa-trash-o fa-1x delete' id = '" + (i + 1) + "'style='color:crimson'></i>";
            c.innerHTML = c.innerHTML + s;
            parent.appendChild(c);
        }
        //console.log(listOfTemes);

    },
    'click .add_new_test': function(){
        var data = {
            name: Template.instance().$('.name_new_test')[0].value,
            discipline: Template.instance().$('.discip')[0].value,
            temes: listOfTemes,
            count: parseInt(Template.instance().$(".items_counter")[0].value, 10)
        };
        if(data.name == "" || data.name == undefined){
            throwError("Невозможно создать тест с пустым именем");
        }else if(data.temes.length == 0){
            throwError("Невозможно создать тест без тем по дисциплине");
        }else {
            Meteor.call("addTest", data, function(error){
                if(error){throwError(error.reason);}
                else{throwNotification("Тест успешно добавлен в БД");
                }});
        }
        //console.log(data);

    }
});