/**
 * Created by faustkun on 02.04.15.
 */
Template.toGroup.helpers({
    groups: function(){
        return Groups.find();
    },
    tests: function(){
        return Tests.find();
    }
});

Template.toGroup.events({
    'click .give_test_to_proup': function(){
        var test = Template.instance().$('.test_to_group')[0].value;
        var group = Template.instance().$('.goups_users')[0].value;

        var data = {
            group: group,
            test: test
        };
        Session.set('currentGroup', group);
        Meteor.call("giveToGroup", data, function(error){
            if(error){throwError(error.reason);}
            else{throwNotification("Тест успешно выдан группе " +
            Groups.findOne({_id: Session.get('currentGroup')}).name);
            }
        });
    }
});