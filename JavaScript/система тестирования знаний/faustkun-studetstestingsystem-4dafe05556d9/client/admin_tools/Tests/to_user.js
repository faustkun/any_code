/**
 * Created by faustkun on 02.04.15.
 */
Template.toUser.helpers({
    tests: function(){
        return Tests.find();
    },
    users: function(){
        return Meteor.users.find();
    }
});

Template.toUser.events({
    "click .give_test_to_users": function(){
        var test = Template.instance().$('.test_to_user')[0].value;
        var arr = Template.instance().$('input:checkbox:checked');
        //console.log(arr.length);
        var data = [];
        for(var i = 0; i < arr.length; i++){
            data[data.length] = {
                userId: arr[i].value,
                test: test
            }
        }
        if(data.length == 0){
            throwError("Не выбрано ниодного пользователя");
        }else {
            Meteor.call('giveToUsers', data, function(error){
                if(error){throwError(error.reason);}
                else{throwNotification("Тест успешно выдан выбранным пользователям");}});
        }

    }
});