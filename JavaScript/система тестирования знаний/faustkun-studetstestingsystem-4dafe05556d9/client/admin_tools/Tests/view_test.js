/**
 * Created by faustkun on 31.03.15.
 */
Template.viewTest.helpers({
    discipline: function(){
        return Disciplines.find({_id: Template.instance().data.discipline}).fetch()[0].name;
    }
});

Template.viewTest.events({
    'click .delete': function(){
        if(confirm("Внимание! Удаление теста повлечет за собой удаление всех результатов прохождения данного теста")){
            Meteor.call('deleteTest', this._id, function(error){
                if(error){throwError(error.reason);}
                else{throwNotification("Тест успешно удален из БД");}});
        }

    }
});