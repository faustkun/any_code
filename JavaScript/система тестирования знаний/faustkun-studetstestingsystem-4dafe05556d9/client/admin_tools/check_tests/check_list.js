/**
 * Created by faustkun on 19.04.15.
 */
Template.checkList.helpers({
    results: function(){return Results.find({}, {sort: {idTest: 1}})},
    testDate: function(id){
        var date = Results.findOne({_id: id}).date;
        if (date == undefined){return "-"}
        date = new Date(date);
        return formatDate(date);},
    testName: function(id){
        return Tests.find({_id: id}).fetch()[0].name},
    mark: function(id){
        var data = Results.findOne({_id: id});
        if (data.flag == false){return "ещё не пройден"};
        if (data.complete == false && data.flag == true){ return 'ещё не проверен'};
        if(data.complete == true){return data.mark};
    },
    disciplineName: function(id){
        var temp = Tests.findOne({_id: id});
        return Disciplines.findOne({_id: temp.discipline}).name},
    user: function(id){
        return Meteor.users.findOne({_id: id}).profile.name;
    }
});