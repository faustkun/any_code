/**
 * Created by faustkun on 19.04.15.
 */
Template.checkResult.helpers({
    userData:function(){
        var t = Results.findOne();
        return {
            name: Meteor.users.findOne({_id: t.idUser}).profile.name,
            test: Tests.findOne({_id: t.idTest}).name
        }
    },
    tasks: function(){return Tasks.find()},
    answer: function(id){
        var t = Results.findOne().marks;
        for(var i = 0; i < t.length; i++){
            if(t[i].idTask == id){
                return t[i].answer
            }
        }
    }
});

Template.checkResult.events({
    'click .check': function(){
        var res = Results.findOne();
        var id_res = res._id;
        var marks = res.marks;

        for(var i = 0; i < marks.length; i++){
            if(marks[i].mark < 0){
                marks[i].mark = parseInt(document.getElementById(marks[i].idTask).value, 10);
            }
        }
        Meteor.call('checkedResults', id_res, marks, function(error){
            if(error){throwError(error.reason);}
            else{Router.go('checkList')}});

    }
});
Template.checkResult.rendered = function(){
    checkNumber1 = function (e) {
        var s = parseInt(e.target.value + String.fromCharCode(e.charCode), 10);
        if(e.target.max < s || e.target.min > s){
            return false;
        }
        return (/[0-9]/.test(String.fromCharCode(e.charCode)));};
    checkNumber2 = function(e){
        var s = parseInt(e.target.value, 10);
        //console.log(s);
        if(e.target.max < s || e.target.min > s){
            e.target.value = 1;
            return false;
        }
    };

    var t = Template.instance().$('input[type="number"]');
    for (var i =0; i < t.length; i++){
        t[i].onkeypress = checkNumber1;
        t[i].onchange = checkNumber2;
    }
};