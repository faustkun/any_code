/**
 * Created by faustkun on 28.03.15.
 */
Template.configPanel.helpers({
    disciplines:function(){return Disciplines.find();},
    groups: function(){return Groups.find();}
});

Template.configPanel.events({
    'click .check_new_group': function(){
        if(Template.instance().$("#new_group")[0].value != ""){
            Meteor.call("addGroup", Template.instance().$("#new_group")[0].value, function(error){
                if(error){throwError(error.reason);}
                else{throwNotification("Новая группа успешно добавлена в БД");}});
        }else {
            throwError("Слишком короткое имя для группы");
        }

    },
    'click .check_new_discipline': function(){
        if(Template.instance().$("#new_discipline")[0].value != ""){
            Meteor.call('addDiscipline', Template.instance().$("#new_discipline")[0].value, function(error){
                if(error){throwError(error.reason);}
                else{throwNotification("Новая дисциплина успешно добавлена в БД");}});
        } else {
            throwError("Слишком короткое имя для дисциплины");
        }

    }
});