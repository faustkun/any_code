/**
 * Created by faustkun on 30.03.15.
 */
Template.disciplineNTemeBlock.helpers({
    disciplines: function(){return Disciplines.find()}
});
Template.disciplineNTemeBlock.rendered = function(){
    //console.log(Template.instance().$('.discip')[0].value);
    var parent = Template.instance().$('.tem')[0];
    var temes = Disciplines.find({_id: Template.instance().$('.discip')[0].value}).fetch()[0].temes;
    //parent.size = temes.length;
    //console.log(temes);
    for(var i = 0; i < temes.length; i++){
        var option = document.createElement('option');
        option.value = temes[i];
        option.text = temes[i];
        parent.add(option);
    }
};
Template.disciplineNTemeBlock.events({
    'change .discip': function(){
        //console.log("changed");
        //console.log(Template.instance().$('.discip')[0].value);
        Template.instance().$('.tem').find('option').remove().end();
        var parent = Template.instance().$('.tem')[0];
        var temes = Disciplines.find({_id: Template.instance().$('.discip')[0].value}).fetch()[0].temes;
        //console.log(temes);
        for(var i = 0; i < temes.length; i++){
            var option = document.createElement('option');
            option.value = temes[i];
            option.text = temes[i];
            parent.add(option);
        }
    }
});