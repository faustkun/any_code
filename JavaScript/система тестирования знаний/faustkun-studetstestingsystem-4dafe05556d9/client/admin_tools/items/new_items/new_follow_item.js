/**
 * Created by faustkun on 20.03.15.
 */
var len = 4;
Template.newFollowItem.rendered = function(){
    checkNumber1 = function (e) {
        var s = parseInt(e.target.value + String.fromCharCode(e.charCode), 10);
        if(e.target.max < s || e.target.min > s){
            return false;
        }
        return (/[0-9]/.test(String.fromCharCode(e.charCode)));};
    checkNumber2 = function(e){
        var s = parseInt(e.target.value, 10);
        //console.log(s);
        if(e.target.max < s || e.target.min > s){
            e.target.value = 1;
            return false;
        }
    };
    checkTextarea1 = function(e){
        //console.log(e);
        var maxLen = 800;
        if (e.target.value.length > maxLen){
            e.target.value = e.target.value.substr(0, maxLen);
        }
    };
    checkTextarea2 = function(e){
        //console.log(e);
        var maxLen = 800;
        if (e.target.value.length == maxLen){
            return false;
        }
    };

    var t = Template.instance().$('input[type="number"]');
    for (var i =0; i < t.length; i++){
        t[i].onkeypress = checkNumber1;
        t[i].onchange = checkNumber2;
    }

    var t = Template.instance().$('textarea');
    for (var i =0; i < t.length; i++){
        t[i].onchange = checkTextarea1;
        t[i].onkeypress = checkTextarea2;
    }
};
Template.newFollowItem.events({
    'click input[type="submit"]': function () {
        var file = Template.instance().$('#file').get(0).files[0];
        var fileObj = Images.insert(file);
        Template.instance().image_id = fileObj._id;
        console.log(fileObj._id);
    },
    'click .follow_inc': function(){
        var parent = document.getElementById("follow_list");
        var elem = document.createElement('div');
        len ++;
        elem.id = 'follow' + len;
        elem.innerHTML = "<input type='text' class='form-control' maxlength='200' id='follow_t_" + len + "'>";
        parent.appendChild(elem);
    },
    'click .follow_dec': function(){
        if (len > 1){
            var elem = document.getElementById('follow' + len);
            len --;
            elem.remove();
        }
    },
    'click .follow_item_adding': function(){
        var discipline = Disciplines.find({_id: Template.instance().$('.discip')[0].value}).fetch()[0];
        var teme = Template.instance().$('.tem')[0].value;

        var head = document.getElementById('follow_head').value;
        var url = '';
        if (Template.instance().image_id != undefined){
            url = '/cfs/files/img/' + Template.instance().image_id;
        }
        var data = [];
        for(var i = 1; i < len + 1; i ++){
            data[i - 1] = document.getElementById('follow_t_' + i).value;
        }
        var cost = parseInt(document.getElementById('follow_cost').value, 10);

        var item = {
            type: 4,
            discipline: discipline,
            teme: teme,
            head: head,
            url: url,
            imgId: Template.instance().image_id,
            data: data,
            cost: cost
        };
        Meteor.call("itemInsert",item, function(error){
            if(error){throwError(error.reason);}
            else{throwNotification("Тестовое задание успешно добавлено в БД");}});
        //console.log(item);
    }
});