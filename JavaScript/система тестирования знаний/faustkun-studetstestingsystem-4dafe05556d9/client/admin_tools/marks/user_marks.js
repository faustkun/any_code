/**
 * Created by faustkun on 03.05.15.
 */
Template.userMarks.helpers({
    results: function(){return Results.find({}, {sort: {idTest: 1}})},
    testDate: function(id){
        var date = Results.findOne({_id: id}).date;
        if (date == undefined){return "-"}
        date = new Date(date);
        return formatDate(date);},
    testName: function(id){
        return Tests.find({_id: id}).fetch()[0].name},
    mark: function(id){
        var data = Results.findOne({_id: id});
        if (data.flag == false){return "ещё не пройден"};
        if (data.complete == false && data.flag == true){ return 'ещё не проверен'};
        if(data.complete == true){return data.mark};

    },
    disciplineName: function(id){
        var temp = Tests.findOne({_id: id});
        return Disciplines.findOne({_id: temp.discipline}).name}
});


function formatDate(date) {

    var dd = date.getDate();
    if (dd < 10) dd = '0' + dd;

    var mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

    var yy = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;

    return dd + '.' + mm + '.' + yy;
}