/**
 * Created by faustkun on 31.03.15.
 */
Template.taskItem2.events({
    'click .delete': function(){if(confirm('Вы уверены что хотите удалить это тестовое задание?')){
        Meteor.call('deleteTask', this._id, function(error){
            if(error){throwError(error.reason);}
            else{throwNotification("Тестовое задание успешно удалено");}});
    }
    }
});

Template.taskItem2.rendered = function(){
    //console.log(Template.instance().data);
    var radio = Template.instance().$('.radio_zone')[0];
    var t = document.createElement('h5');
    t.innerHTML = "<input type='radio' name='radio' value='true'/>" + Template.instance().data.data[0];

    var tt = document.createElement('h5');
    tt.innerHTML = "<input type='radio' name='radio' value='false'/>" + Template.instance().data.data[1];

    var a = Template.instance().$('.answer')[0];
    if(Template.instance().data.answers){
        a.textContent = a.textContent + " " +Template.instance().data.data[0];
    }else{
        a.textContent = a.textContent + " " +Template.instance().data.data[1];
    }

    radio.appendChild(t);
    radio.appendChild(tt);

};