/**
 * Created by faustkun on 31.03.15.
 */
Template.taskItem3.helpers({
    second_length: function(){return Template.instance().data.second.length;}
});
Template.taskItem3.events({
    'click .delete': function(){if(confirm('Вы уверены что хотите удалить это тестовое задание?')){
        Meteor.call('deleteTask', this._id, function(error){
            if(error){throwError(error.reason);}
            else{throwNotification("Тестовое задание успешно удалено");}});
    }
    }
});