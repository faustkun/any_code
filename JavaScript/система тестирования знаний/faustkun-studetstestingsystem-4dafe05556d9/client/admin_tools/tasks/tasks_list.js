/**
 * Created by faustkun on 07.12.14.
 */


var task_number = 0;
Template.taskList.helpers({
    tasks: function(){return Tasks.find({}, {sort: {submitted: -1}})},
    number: function () {
        task_number ++;
        return task_number}

});

Template.taskList.rendered = function(){
    task_number = 0;
};