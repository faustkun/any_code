/**
 * Created by faustkun on 26.03.15.
 */
var len = 1;
var groupsTemp = [];

Template.newUsers.helpers({
    groups: function(){
        var q = Groups.find();
        groupsTemp = q.fetch();
        return q;}
});

Template.newUsers.events({
    'click .newUsers_inc': function(){
        var parent = document.getElementById("body");
        var elem = document.createElement('tr');
        len ++;
        elem.id = 'newUsers_tr_' + len;
        var th1 = "<th><input type='text' class='form-control' maxlength='20' id='newUsers_st_n_" + len + "' style='width: 80px'></th>";
        var th2 = "<th><input type='text' class='form-control' maxlength='20' id='newUsers_usN_" + len + "' style='width: 100px'></th>";
        var th3 = "<th><input type='text' class='form-control' maxlength='140' id='newUsers_fio_" + len + "'></th>";
        var th4 = "<th><p><select id='newUsers_group_" + len + "' style='width: 120px'> </select></p></th>";
        elem.innerHTML = th1 + th2 + th3 + th4;
        parent.appendChild(elem);
        var temp = document.getElementById('newUsers_group_' + len);
        Blaze.render(Template.groupsList, temp);

        //console.log(temp.options[0].value);
    },

    'click .newUsers_dec': function(){
        if (len > 1){
            var elem = document.getElementById('newUsers_tr_' + len);
            len --;
            elem.remove();
        }
    },
    'click .newUsers_add': function(){
        var arr = [];
        for(var i = 1; i < len + 1; i++){
            var sn = document.getElementById('newUsers_st_n_' + i).value;
            var usN = document.getElementById('newUsers_usN_' + i).value;
            var fio = document.getElementById('newUsers_fio_' + i).value;
            var group = Groups.find({_id: $('#newUsers_group_' + i)[0].value}).fetch()[0];

            var user = {
                studentNumber: sn,
                username: usN,
                //password: sn,
                name: fio,
                group: group
            };
            arr[i - 1] = user;
        }
        Meteor.call('addUsers', arr, function(error){
            if(error){throwError(error.reason);}
            else{throwNotification("Пользователи успешно добавлны в БД");}});
        //console.log(arr);
    }
});