/**
 * Created by faustkun on 04.04.15.
 */
Template.alterItem.rendered = function(){
    //console.log(Template.instance().data);
    var radio = Template.instance().$('.radio_zone')[0];
    var t = document.createElement('h5');
    t.innerHTML = "<input type='radio' name='radio' value='true' class='radio_alternative'/>" + Template.instance().data.data[0];

    var tt = document.createElement('h5');
    tt.innerHTML = "<input type='radio' name='radio' value='false' class='radio_alternative'/>" + Template.instance().data.data[1];

    radio.appendChild(t);
    radio.appendChild(tt);
    var result = Template.instance().$('input[name=radio]:checked').val();
    //console.log(result, Template.instance().data._id);
    Session.set(Template.instance().data._id, result);

};

Template.alterItem.events({
    'change .radio_alternative': function(){
        var result = Template.instance().$('input[name=radio]:checked').val();
        //console.log(result, Template.instance().data._id);
        Session.set(Template.instance().data._id, result);
    }
});
