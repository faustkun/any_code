/**
 * Created by faustkun on 04.04.15.
 */
Template.followItem.helpers({
    list_length: function(){return Template.instance().data.data.length;},
    list: function(){
        function shuffle(o){
            for(var j, x, k = o.length; k; j = Math.floor(Math.random() * k), x = o[--k], o[k] = o[j], o[j] = x);
            return o;
        };
        var temp = Template.instance().data.data;
        temp = shuffle(temp);
        Template.instance().data.temp_list = temp;
        return temp;
    }
});

Template.followItem.events({
    'change input': function(){
        function int_to_int(c){
            for(var i = c - 3; i < c + 1; i ++ ){
                if(i == c){return i}
            }
        }
        var temp = Template.instance().$('input');
        var temp2 = [];
        //console.log(this.temp_list);
        for(var i = 0; i < temp.length; i ++){
            temp2[int_to_int(parseInt(temp[i].value, 10))] = Template.instance().data.temp_list[i];
        }
        var result = [];
        for(var i = 1; i < temp2.length; i ++){
            result[i - 1] = temp2[i];
        }
        //console.log(result, Template.instance().data._id);
        Session.set(Template.instance().data._id, result);
    }
});

Template.followItem.rendered = function(){
    function int_to_int(c){
        for(var i = c - 3; i < c + 1; i ++ ){
            if(i == c){return i}
        }
    }
    var temp = Template.instance().$('input');
    var temp2 = [];
    //console.log(this.temp_list);
    for(var i = 0; i < temp.length; i ++){
        temp2[int_to_int(parseInt(temp[i].value, 10))] = Template.instance().data.temp_list[i];
    }
    var result = [];
    for(var i = 1; i < temp2.length; i ++){
        result[i - 1] = temp2[i];
    }
    //console.log(result, Template.instance().data._id);
    Session.set(Template.instance().data._id, result);


    checkNumber1 = function (e) {
        var s = parseInt(e.target.value + String.fromCharCode(e.charCode), 10);
        if(e.target.max < s || e.target.min > s){
            return false;
        }
        return (/[0-9]/.test(String.fromCharCode(e.charCode)));};
    checkNumber2 = function(e){
        var s = parseInt(e.target.value, 10);
        //console.log(s);
        if(e.target.max < s || e.target.min > s){
            e.target.value = 1;
            return false;
        }
    };

    var t = Template.instance().$('input[type="number"]');
    for (var i =0; i < t.length; i++){
        t[i].onkeypress = checkNumber1;
        t[i].onchange = checkNumber2;
    }

};