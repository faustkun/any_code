/**
 * Created by faustkun on 04.04.15.
 */
var task_number = 0;
Template.testing.helpers({
    tasks: function(){return Tasks.find()},
    number: function () {
        task_number ++;
        return task_number}

});

Template.testing.events({
    'click .end_test': function(){
        var temp = Tasks.find().fetch();
        var ids = [];
        for(var i = 0; i < temp.length; i++){
            ids[i] = temp[i]._id;
        }
        //console.log(ids);
        var result = [];
        for(var i = 0; i < ids.length; i ++){
            result[i] = {
                id: ids[i],
                data: Session.get(ids[i])
            }
        }

        console.log(result);
        //console.log(Results.findOne()._id);
        Meteor.call('testingResults', Results.findOne()._id, result, function(error){
            if(error){throwError(error.reason);}
            else{Router.go('userPage')}});
    }
});

Template.testing.rendered = function(){
    task_number = 0;
};