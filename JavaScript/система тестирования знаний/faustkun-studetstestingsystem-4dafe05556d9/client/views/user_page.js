/**
 * Created by faustkun on 03.04.15.
 */
Template.userPage.helpers({
    results: function(){return Results.find({flag: false}, {sort: {idTest: 1}});},
    completeResults: function(){return Results.find({flag: true}, {sort: {date: 1}});},
    testDate: function(id){
        date = new Date(Results.findOne({_id: id}).date);
        return formatDate(date);
    },
    testName: function(id){
        return Tests.findOne({_id: id}).name},
    disciplineName: function(id){
        var temp = Tests.findOne({_id: id});
        return Disciplines.findOne({_id: temp.discipline}).name},
    mark: function(id){
        var data = Results.findOne({_id: id});
        if (data.flag == false){return "ещё не пройден"};
        if (data.complete == false && data.flag == true){ return 'ещё не проверен'};
        if(data.complete == true){return data.mark};

    }
});

function formatDate(date) {

    var dd = date.getDate();
    if (dd < 10) dd = '0' + dd;

    var mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

    var yy = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;

    return dd + '.' + mm + '.' + yy;
}