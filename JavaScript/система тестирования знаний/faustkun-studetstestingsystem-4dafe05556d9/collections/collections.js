/**
 * Created by faustkun on 09.12.14.
 */
Tasks = new Mongo.Collection('tasks');
Groups = new Mongo.Collection('groups');
Disciplines = new Mongo.Collection('disciplines');
Tests = new Mongo.Collection('tests');
Results = new Mongo.Collection('results');


//FS.debug = true;

var ImageStore = new FS.Store.FileSystem('img', {
    path: '~/uploads/full'
});

Images = new FS.Collection('img', {
    stores: [ImageStore],
    filter: {
        maxSize: 10485760, // in bytes
        allow: {
            contentTypes: ['image/*']
        },
        onInvalid: function (message) {
            if (Meteor.isClient) {
                alert(message);
            } else {
                console.log(message);
            }
        }
    }
});
Images.allow({
    insert: function () {
        return true;
    },
    update: function () {
        return true;
    },
    remove: function () {
        return true;
    },
    download: function () {
        return true;
    }
});


//-------------------------------------------------------------------------
//каскадное удаление

Groups.before.remove(function(userId, doc){
    Meteor.users.remove({'profile.group._id': doc._id});
});

Meteor.users.before.remove(function(userId, doc){
    Results.remove({idUser: doc._id})
});

Tests.before.remove(function(userId, doc){
    Results.remove({idTest: doc._id})
});

Disciplines.before.remove(function(userId, doc){
    Tests.remove({discipline: doc._id});
    Tasks.remove({'discipline._id': doc._id});
});

Disciplines.before.update(function(userId, doc, fieldNames, modifier, options){
    if(modifier.$set != undefined){
        var mass = [];
        for(var i = 0; i < doc.temes.length; i ++){
            var flag = true;
            for (var j = 0; j < modifier.$set.temes.length; j ++){
                if(doc.temes[i] == modifier.$set.temes[j]){flag = false;}
            }
            if(flag == true){
                mass[mass.length] = doc.temes[i];
            }
        }
        //console.log(doc);
        Tests.remove({discipline: doc._id, temes: {$in: mass}});
        Tasks.remove({'discipline._id': doc._id, teme: {$in: mass}});
    };
});

Tasks.before.remove(function(userId, doc){
    Images.remove({_id: doc.imgId});
});
