/**
 * Created by faustkun on 19.02.15.
 */
Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'error2'
});

Router.map(function() {

    //пользовательские роутинги
    this.route('userPage', {path: '/',
        waitOn: function(){
            var user = Meteor.user();
            if(user){
                return [Meteor.subscribe('userPage_tests'),
                        Meteor.subscribe('userPage_results', user._id),
                        Meteor.subscribe('userPage_disciplines')]
            }
        }
    });

    this.route('testing', {path: '/testing/:_id',
        waitOn: function () {
            return [Meteor.subscribe('testing_results', this.params._id),
                    Meteor.subscribe('testing_tasks', this.params._id)]
        }
    });

    //админские роутинги
    this.route('newTestItem', {path: '/newts',
        waitOn: function(){return Meteor.subscribe("newTestItem_disciplines")}
    });

    this.route('taskList', {path: '/tasklist',
        waitOn: function () {return Meteor.subscribe('taskList_tasks')}
    });

    this.route('usersPasswords', {path: '/users_passwords',
        waitOn: function(){
            return [Meteor.subscribe("usersPasswords_groups"),
                    Meteor.subscribe("usersPasswords_users")]
        }
    });

    this.route('configPanel', {path: '/configs',
        waitOn: function(){
            return [Meteor.subscribe("configPanel_groups"),
                    Meteor.subscribe("configPanel_disciplines")]
        }
    });

    this.route('adminTests', {path: '/admin_tests',
        waitOn: function(){
            return [Meteor.subscribe('adminTests_tests'),
                    Meteor.subscribe("adminTests_disciplines")]
        }
    });

    this.route('giveTests', {path: '/give_tests',
        waitOn: function(){
            return [Meteor.subscribe("giveTests_groups"),
                    Meteor.subscribe('giveTests_users'),
                    Meteor.subscribe("giveTests_tests")]
        }
    });

    this.route('checkList', {path: '/check',
        waitOn: function () {
            return [Meteor.subscribe('checkList_tests'),
                    Meteor.subscribe('checkList_results'),
                    Meteor.subscribe('checkList_disciplines'),
                    Meteor.subscribe('checkList_users')]}
    });

    this.route('checkResult', {path: '/check/:idRes',
        waitOn: function () {
            return [Meteor.subscribe('checkResult_tests'),
                    Meteor.subscribe('checkResult_tasks', this.params.idRes),
                    Meteor.subscribe('checkResult_result', this.params.idRes),
                    Meteor.subscribe('checkResult_user', this.params.idRes)]
        }
    });

    this.route('marks', {path: '/marks',
        waitOn: function(){return Meteor.subscribe('marks_users');}
    });

    this.route('userMarks', {path: '/marks/:id',
        data: function(){
            return {
                user: Meteor.users.findOne({_id: this.params.id})
            }
        },
        waitOn: function(){
            return [Meteor.subscribe('userMarks_user', this.params.id),
                    Meteor.subscribe('userMarks_tests'),
                    Meteor.subscribe('userMarks_results', this.params.id),
                    Meteor.subscribe('userMarks_disciplines')]
        }
    });
});



var requireAdmin = function(){
    if(!Meteor.user()){
        this.render('oups');}
    else {
        if (!Roles.userIsInRole(Meteor.user()._id, ['admin'])) {
            this.render('youNotAdmin');
        } else {
            this.next();
        }
    }
};


Router.onBeforeAction(requireAdmin, {only: ['newTestItem',
                                            'usersPasswords',
                                            'configPanel',
                                            'taskList',
                                            'adminTests',
                                            'checkList',
                                            'checkListTest',
                                            'checkResult',
                                            'giveTests',
                                            'marks',
                                            'userMarks']});
Router.onBeforeAction(function() { clearErrors();
                                    clearNotifications();
                                    this.next(); });
