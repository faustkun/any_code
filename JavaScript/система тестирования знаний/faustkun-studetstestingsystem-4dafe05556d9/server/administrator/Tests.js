/**
 * Created by faustkun on 31.03.15.
 */
Meteor.methods({
    'addTest': function (data) {
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            if(Tests.findOne({name: data.name, discipline: data.discipline}) == undefined){
               Tests.insert(data);
            }else {
               throw new Meteor.Error("Такой тест уже существует", "Такой тест уже существует");
            }
        }
    },
    'deleteTest': function(idTest){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            Tests.remove({_id: idTest});}
    }
});