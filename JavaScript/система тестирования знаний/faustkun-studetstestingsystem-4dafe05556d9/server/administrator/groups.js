/**
 * Created by faustkun on 28.03.15.
 */
Meteor.methods({
    "addGroup": function(groupName){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            if(Groups.findOne({name: groupName})== undefined){
                Groups.insert({name: groupName});
            }else{
                throw new Meteor.Error("Такая группа уже существует", 'Такая группа уже существует');
            }


        }
    },
    "deleteGroup": function(groupId){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            Groups.remove({_id: groupId});
        }
    }
});