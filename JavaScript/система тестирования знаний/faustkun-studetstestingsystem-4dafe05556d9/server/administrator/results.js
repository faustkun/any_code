/**
 * Created by faustkun on 02.04.15.
 */
Meteor.methods({
        //процесс выдачи теста пользователю
        giveToUsers: function (data) {
            var user = Meteor.user();
            if(Roles.userIsInRole(user._id, ['admin'])){
                for(var i = 0; i < data.length; i++){
                    var item = {
                        idUser: data[i].userId,
                        idTest: data[i].test,
                        flag: false
                    };

                    Results.insert(item);
                }
            }
        },
        //тоже самое что и для пользоватея только для всей группы
        giveToGroup: function (data) {
            var user = Meteor.user();
            if (Roles.userIsInRole(user._id, ['admin'])) {
                var temp = Meteor.users.find({'profile.group._id': data.group}).fetch();
                for(var i = 0; i < temp.length; i ++){
                    var item = {
                        idUser: temp[i]._id,
                        idTest: data.test,
                        flag: false
                    };

                    Results.insert(item);
                }
            }
        },
        //получение результатов тестирования и автоматическая проверка всего что можно, а если возможно то и выставление
        //оценки
        testingResults: function(idRes, data){
            if(Meteor.user()){
                var ids = [];
                for(var i = 0; i < data.length; i ++){
                    ids[i] = data[i].id;
                }
                //console.log(ids);

                var currentTasks = Tasks.find({_id: {$in: ids}/*, type: {$lt: 5}*/}).fetch();

                //далее идет автоматическая проверка заданий
                var results = [];
                for(var i = 0; i < data.length; i ++){
                    var temp;
                    for (var j = 0; j < currentTasks.length; j ++){
                        if(currentTasks[j]._id == data[i].id){
                            temp= currentTasks[j];
                        }
                    }
                    //console.log(check_results(data[i].data, temp));
                    results[i] = {
                        idTask: data[i].id,
                        answer: data[i].data,
                        cost: temp.cost,
                        mark: check_results(data[i].data, temp)
                    };
                }
                var complete = true;
                for(var i = 0; i < results.length; i++){
                    if(results[i].mark < 0){
                        Results.update({_id: idRes}, {$set:{marks: results, flag: true, complete: false, date: new Date()}});
                        complete = false;
                    }
                }
                if(complete == true){
                    var costs = 0;
                    var marks = 0;
                    for(var i = 0; i < results.length; i++){
                        costs += results[i].cost;
                        marks += results[i].mark;
                    }
                    var itogMark = ((marks/costs)*100).toFixed(2);
                    Results.update({_id: idRes}, {$set:{mark: itogMark, marks: results, flag: true, complete: true, date: new Date()}});
                }
                //console.log(results);
            }
        },
        //изменения после проверки преподавателем, тут же считаем итоговую оценку
        checkedResults: function(idRes, data){
            var user = Meteor.user();
            if(Roles.userIsInRole(user._id, ['admin'])){
                var marks = 0;
                var costs = 0;
                for(var i = 0; i < data.length; i++){
                    //console.log(i, ':', data[i].mark, data[i].cost)
                    marks += data[i].mark;
                    costs += data[i].cost;
                }
                var itogMark = ((marks/costs)*100).toFixed(2);
                //console.log((marks/costs)*100);
                Results.update({_id: idRes}, {$set:{mark: itogMark, marks: data, flag: true, complete: true}});
            }
        }
    }
);


function check_results(data, task){
    //проверка заданий с множдественным выбором
    if(task.type == 1){
        var cost = task.cost;
        var delta = cost/task.data.length;
        for(var i = 0; i < data.length; i ++){
            if(task.answers.indexOf(data[i]) == -1){cost = cost - delta;}
        }
        for(var i = 0; i < task.answers.length; i ++){
            if(data.indexOf(task.answers[i]) == -1){cost = cost - delta;}
        }
        return cost;
    }
    //проверка заданий с альтернативным выбором
    if(task.type == 2){
        var cost = task.cost;
        if(data == task.answers + ""){
            return cost;
        }else {
            return 0;
        }
    }
    //проверка заданий на установление соответсвия
    if(task.type == 3){
        var counter = data.length;
        var delta = task.cost/task.answers.length;
        for(var i = 0; i < data.length; i ++){
            if(data[i] != task.answers[i]){counter -= 1}
        }
        //console.log(counter*delta);
        return counter*delta;
    }
    //проверка заданий на установление последовательности
    if(task.type == 4){
        while(data.length != task.data.length){data[data.length] = "";}
        var temp1 = [];
        for (var i = 0; i < data.length - 1; i ++){
            temp1[i] = data[i] + "-" + data[(i + 1)];
        }
        var temp2 = [];
        for (var i = 0; i < task.data.length - 1; i ++){
            temp2[i] = task.data[i] + "-" + task.data[(i + 1)];
        }
        var cost = task.cost;
        var delta = cost/temp1.length;
        for(var i = 0; i < temp1.length; i ++){
            if(temp2.indexOf(temp1[i]) == -1){cost = cost - delta;}
        }
        //console.log("Левое множество", temp1);
        //console.log("Правое множество", temp2);
        //console.log("Оценка ", cost);
        return cost;
    }
    //задания-дополнения и со свободным изложением автоматической проверке не подлежат так что ставим -1
    if(task.type > 4){
        return -1}
}