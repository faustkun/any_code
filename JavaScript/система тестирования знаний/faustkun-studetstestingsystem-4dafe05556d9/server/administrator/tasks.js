/**
 * Created by faustkun on 31.03.15.
 */
Meteor.methods({
    'deleteTask': function(idTask){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            Tasks.remove({_id: idTask});}
    }
});