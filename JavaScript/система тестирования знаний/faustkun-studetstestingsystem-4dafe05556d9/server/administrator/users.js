/**
 * Created by faustkun on 15.03.15.
 */

Meteor.methods({
    'changePasswd': function (id, password){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
        Accounts.setPassword(id, password);}
    },

    'addUsers': function(users){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            _.each(users, function(user){
                Accounts.createUser({
                    username: user.username,
                    password: user.studentNumber,
                    profile:{
                        name: user.name,
                        group: user.group,
                        studentNumber: user.studentNumber
                    }
                })
            })
        }
    },

    'updateUserData': function(idUser,userData){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            Meteor.users.update({_id: idUser}, {$set: userData});
        }
    },

    'deleteUser': function(idUser){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            Meteor.users.remove({_id: idUser});
        }
    }
});