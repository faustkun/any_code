/**
 * Created by faustkun on 09.12.14.
 */
if(Meteor.users.find({username: 'admin'}).fetch() == 0){
    var id = Accounts.createUser({
        username: 'admin',
        password: 'admin'
    });
    Roles.addUsersToRoles(id, ['admin']);
}
if(Groups.find().fetch().length == 0){
    var group = {
        name: "ИТО-1-10"
    };
    Groups.insert(group);
}

if(Disciplines.find().fetch().length == 0){
    var discipline = {
        name: "Pascal",
        temes: ['Тема 1', 'Тема 2', 'Тема 3']
    };

    Disciplines.insert(discipline);
}
