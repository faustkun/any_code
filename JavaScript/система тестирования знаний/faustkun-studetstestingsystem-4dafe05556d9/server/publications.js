/**
 * Created by faustkun on 09.12.14.
 */

//В этом файле было очень много непонятного кода, поэтому я разделил его так чтобы каждому роутингу соответсвовала одна
//уникальная публикация из базы данных, это позволит в будущем расширять функционал, без вреда для других страниц
//приложения

//для пользовательских роутингов

// '/'
Meteor.publish('userPage_tests', function(){
    return Tests.find();
});
Meteor.publish('userPage_results', function(idUser){
    return Results.find({idUser: idUser});
});
Meteor.publish('userPage_disciplines', function(){
    return Disciplines.find();
});

// '/testing/:_id'
Meteor.publish('testing_results', function(id){
    return Results.find({_id: id, flag: false});
});
Meteor.publish('testing_tasks', function(id){
    //функция для перемешивания
    function shuffle(o){
        for(var j, x, k = o.length; k; j = Math.floor(Math.random() * k), x = o[--k], o[k] = o[j], o[j] = x);
        return o;
    };

    var res = Results.findOne({_id: id, flag: false});
    var test = Tests.findOne({_id: res.idTest});
    var options = {type: 0, discipline: 0, teme: 0, url: 0, head: 0,
        data: 0, answers: 0, cost: 0, addedBy: 0, submitted: 0
    };

    var find = Tasks.find({'discipline._id': test.discipline, teme:{$in: test.temes}}, {fields: options}).fetch();
    var ids = [];
    for(var i = 0; i < find.length; i ++){
        ids[i] = find[i]._id;
    }

    ids = shuffle(ids);
    if(ids.length > test.count){
        ids = ids.slice(0, test.count);
    }
    console.log(ids.length);
    //!!!!!!надо поправить создание тестов так, чтобы они создавались с полным именем дисциплины
    return Tasks.find({_id: {$in: ids}})
});


//для админских роутингов

// '/newts'
Meteor.publish('newTestItem_disciplines', function() {
    return Disciplines.find();
});

// '/tasklist'
Meteor.publish('taskList_tasks', function() {
    return Tasks.find();
});

// '/users_passwords'
Meteor.publish('usersPasswords_groups', function() {
    return Groups.find();
});
Meteor.publish('usersPasswords_users', function() {
    var options = {
        _id: 1,
        'profile.studentNumber': 1,
        username: 1,
        'profile.name': 1,
        'profile.group': 1};
    return Meteor.users.find({}, {fields: options});
});

// '/configs'
Meteor.publish('configPanel_groups', function() {
    return Groups.find();
});
Meteor.publish('configPanel_disciplines', function() {
    return Disciplines.find();
});

// '/admin_tests'
Meteor.publish('adminTests_tests', function(){
    return Tests.find();
});
Meteor.publish('adminTests_disciplines', function() {
    return Disciplines.find();
});

// '/give_tests'
Meteor.publish('giveTests_groups', function() {
    return Groups.find();
});
Meteor.publish('giveTests_users', function() {
    var options = {
        _id: 1,
        'profile.studentNumber': 1,
        username: 1,
        'profile.name': 1,
        'profile.group': 1};
    return Meteor.users.find({}, {fields: options});
});
Meteor.publish('giveTests_tests', function(){
    return Tests.find();
});

// '/check'
Meteor.publish('checkList_tests', function(){
    return Tests.find();
});
Meteor.publish('checkList_results', function(){
    return Results.find({complete: false});
});
Meteor.publish('checkList_disciplines', function(){
    return Disciplines.find();
});
Meteor.publish('checkList_users', function(){
    return Meteor.users.find();
});

// '/check/:idRes'
Meteor.publish('checkResult_tests', function(){
    return Tests.find();
});
Meteor.publish('checkResult_tasks', function(id){
    var res = Results.findOne({_id: id});
    var temp = res.marks;
    var ids = [];
    for(var i = 0; i < temp.length; i++){
        if(temp[i].mark < 0){ids[ids.length] = temp[i].idTask};
    }
    return Tasks.find({_id:{$in: ids}})
});
Meteor.publish('checkResult_result', function(id){
    return Results.find({_id: id});
});
Meteor.publish('checkResult_user', function(id){
    var t = Results.findOne({_id: id}).idUser;
    return Meteor.users.find({_id: t});
});

// '/marks'
Meteor.publish('marks_users', function() {
    var options = {
        _id: 1,
        'profile.studentNumber': 1,
        username: 1,
        'profile.name': 1,
        'profile.group': 1};
    return Meteor.users.find({}, {fields: options});
});

// '/marks/:id'
Meteor.publish('userMarks_user', function(id){
    return Meteor.users.find({_id: id});
});
Meteor.publish('userMarks_tests', function(){
    return Tests.find();
});
Meteor.publish('userMarks_results', function(idUser){
    return Results.find({idUser: idUser});
});
Meteor.publish('userMarks_disciplines', function(){
   return Disciplines.find();
});
