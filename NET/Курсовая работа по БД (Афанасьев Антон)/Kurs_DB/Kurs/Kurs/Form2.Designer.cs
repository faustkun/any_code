﻿namespace Kurs
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlDataAdapter1 = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand2 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand2 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand2 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand2 = new System.Data.SqlClient.SqlCommand();
            this.sqlDataAdapter2 = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand3 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand3 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand3 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand3 = new System.Data.SqlClient.SqlCommand();
            this.sqlDataAdapter3 = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand4 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand4 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand4 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand4 = new System.Data.SqlClient.SqlCommand();
            this.sqlDataAdapter4 = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand5 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand5 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand5 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand5 = new System.Data.SqlClient.SqlCommand();
            this.sqlDataAdapter5 = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand6 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand6 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand6 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand6 = new System.Data.SqlClient.SqlCommand();
            this.sqlDataAdapter6 = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand7 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand7 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand7 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand7 = new System.Data.SqlClient.SqlCommand();
            this.sqlDataAdapter7 = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand8 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand8 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand8 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand8 = new System.Data.SqlClient.SqlCommand();
            this.sqlDataAdapter8 = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlSelectCommand9 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand9 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand9 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand9 = new System.Data.SqlClient.SqlCommand();
            this.sqlDataAdapter9 = new System.Data.SqlClient.SqlDataAdapter();
            this.dataSet11 = new Kurs.DataSet1();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(803, 265);
            this.dataGridView1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(74, 311);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(250, 39);
            this.button1.TabIndex = 2;
            this.button1.Text = "Применить измененя";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(492, 311);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(223, 38);
            this.button2.TabIndex = 3;
            this.button2.Text = "Отменить измення";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // sqlConnection1
            // 
            this.sqlConnection1.ConnectionString = "Data Source=faustkun;Initial Catalog=kino;Integrated Security=True";
            this.sqlConnection1.FireInfoMessageEventOnUserErrors = false;
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = "SELECT film.*\r\nFROM     film";
            this.sqlSelectCommand1.Connection = this.sqlConnection1;
            // 
            // sqlInsertCommand1
            // 
            this.sqlInsertCommand1.CommandText = "INSERT INTO [film] ([title], [year], [length]) VALUES (@title, @year, @length);\r\n" +
    "SELECT id_f, title, year, length FROM film WHERE (id_f = SCOPE_IDENTITY())";
            this.sqlInsertCommand1.Connection = this.sqlConnection1;
            this.sqlInsertCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@title", System.Data.SqlDbType.NVarChar, 0, "title"),
            new System.Data.SqlClient.SqlParameter("@year", System.Data.SqlDbType.Int, 0, "year"),
            new System.Data.SqlClient.SqlParameter("@length", System.Data.SqlDbType.Int, 0, "length")});
            // 
            // sqlUpdateCommand1
            // 
            this.sqlUpdateCommand1.CommandText = resources.GetString("sqlUpdateCommand1.CommandText");
            this.sqlUpdateCommand1.Connection = this.sqlConnection1;
            this.sqlUpdateCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@title", System.Data.SqlDbType.NVarChar, 0, "title"),
            new System.Data.SqlClient.SqlParameter("@year", System.Data.SqlDbType.Int, 0, "year"),
            new System.Data.SqlClient.SqlParameter("@length", System.Data.SqlDbType.Int, 0, "length"),
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_title", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "title", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_year", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "year", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_length", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "length", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 4, "id_f")});
            // 
            // sqlDeleteCommand1
            // 
            this.sqlDeleteCommand1.CommandText = "DELETE FROM [film] WHERE (([id_f] = @Original_id_f) AND ([title] = @Original_titl" +
    "e) AND ([year] = @Original_year) AND ([length] = @Original_length))";
            this.sqlDeleteCommand1.Connection = this.sqlConnection1;
            this.sqlDeleteCommand1.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_title", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "title", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_year", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "year", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_length", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "length", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDataAdapter1
            // 
            this.sqlDataAdapter1.DeleteCommand = this.sqlDeleteCommand1;
            this.sqlDataAdapter1.InsertCommand = this.sqlInsertCommand1;
            this.sqlDataAdapter1.SelectCommand = this.sqlSelectCommand1;
            this.sqlDataAdapter1.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "film", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_f", "id_f"),
                        new System.Data.Common.DataColumnMapping("title", "title"),
                        new System.Data.Common.DataColumnMapping("year", "year"),
                        new System.Data.Common.DataColumnMapping("length", "length")})});
            this.sqlDataAdapter1.UpdateCommand = this.sqlUpdateCommand1;
            // 
            // sqlSelectCommand2
            // 
            this.sqlSelectCommand2.CommandText = "SELECT country.*\r\nFROM     country";
            this.sqlSelectCommand2.Connection = this.sqlConnection1;
            // 
            // sqlInsertCommand2
            // 
            this.sqlInsertCommand2.CommandText = "INSERT INTO [country] ([id_f], [id_c]) VALUES (@id_f, @id_c);\r\nSELECT id_f, id_c " +
    "FROM country WHERE (id_c = @id_c) AND (id_f = @id_f)";
            this.sqlInsertCommand2.Connection = this.sqlConnection1;
            this.sqlInsertCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 0, "id_f"),
            new System.Data.SqlClient.SqlParameter("@id_c", System.Data.SqlDbType.Int, 0, "id_c")});
            // 
            // sqlUpdateCommand2
            // 
            this.sqlUpdateCommand2.CommandText = "UPDATE [country] SET [id_f] = @id_f, [id_c] = @id_c WHERE (([id_f] = @Original_id" +
    "_f) AND ([id_c] = @Original_id_c));\r\nSELECT id_f, id_c FROM country WHERE (id_c " +
    "= @id_c) AND (id_f = @id_f)";
            this.sqlUpdateCommand2.Connection = this.sqlConnection1;
            this.sqlUpdateCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 0, "id_f"),
            new System.Data.SqlClient.SqlParameter("@id_c", System.Data.SqlDbType.Int, 0, "id_c"),
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_id_c", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_c", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDeleteCommand2
            // 
            this.sqlDeleteCommand2.CommandText = "DELETE FROM [country] WHERE (([id_f] = @Original_id_f) AND ([id_c] = @Original_id" +
    "_c))";
            this.sqlDeleteCommand2.Connection = this.sqlConnection1;
            this.sqlDeleteCommand2.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_id_c", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_c", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDataAdapter2
            // 
            this.sqlDataAdapter2.DeleteCommand = this.sqlDeleteCommand2;
            this.sqlDataAdapter2.InsertCommand = this.sqlInsertCommand2;
            this.sqlDataAdapter2.SelectCommand = this.sqlSelectCommand2;
            this.sqlDataAdapter2.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "country", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_f", "id_f"),
                        new System.Data.Common.DataColumnMapping("id_c", "id_c")})});
            this.sqlDataAdapter2.UpdateCommand = this.sqlUpdateCommand2;
            // 
            // sqlSelectCommand3
            // 
            this.sqlSelectCommand3.CommandText = "SELECT countryes.*\r\nFROM     countryes";
            this.sqlSelectCommand3.Connection = this.sqlConnection1;
            // 
            // sqlInsertCommand3
            // 
            this.sqlInsertCommand3.CommandText = "INSERT INTO [countryes] ([title]) VALUES (@title);\r\nSELECT id_c, title FROM count" +
    "ryes WHERE (id_c = SCOPE_IDENTITY())";
            this.sqlInsertCommand3.Connection = this.sqlConnection1;
            this.sqlInsertCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@title", System.Data.SqlDbType.NVarChar, 0, "title")});
            // 
            // sqlUpdateCommand3
            // 
            this.sqlUpdateCommand3.CommandText = "UPDATE [countryes] SET [title] = @title WHERE (([id_c] = @Original_id_c) AND ([ti" +
    "tle] = @Original_title));\r\nSELECT id_c, title FROM countryes WHERE (id_c = @id_c" +
    ")";
            this.sqlUpdateCommand3.Connection = this.sqlConnection1;
            this.sqlUpdateCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@title", System.Data.SqlDbType.NVarChar, 0, "title"),
            new System.Data.SqlClient.SqlParameter("@Original_id_c", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_c", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_title", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "title", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@id_c", System.Data.SqlDbType.Int, 4, "id_c")});
            // 
            // sqlDeleteCommand3
            // 
            this.sqlDeleteCommand3.CommandText = "DELETE FROM [countryes] WHERE (([id_c] = @Original_id_c) AND ([title] = @Original" +
    "_title))";
            this.sqlDeleteCommand3.Connection = this.sqlConnection1;
            this.sqlDeleteCommand3.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_id_c", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_c", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_title", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "title", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDataAdapter3
            // 
            this.sqlDataAdapter3.DeleteCommand = this.sqlDeleteCommand3;
            this.sqlDataAdapter3.InsertCommand = this.sqlInsertCommand3;
            this.sqlDataAdapter3.SelectCommand = this.sqlSelectCommand3;
            this.sqlDataAdapter3.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "countryes", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_c", "id_c"),
                        new System.Data.Common.DataColumnMapping("title", "title")})});
            this.sqlDataAdapter3.UpdateCommand = this.sqlUpdateCommand3;
            // 
            // sqlSelectCommand4
            // 
            this.sqlSelectCommand4.CommandText = "SELECT director.*\r\nFROM     director";
            this.sqlSelectCommand4.Connection = this.sqlConnection1;
            // 
            // sqlInsertCommand4
            // 
            this.sqlInsertCommand4.CommandText = "INSERT INTO [director] ([id_f], [id_h]) VALUES (@id_f, @id_h);\r\nSELECT id_f, id_h" +
    " FROM director WHERE (id_f = @id_f) AND (id_h = @id_h)";
            this.sqlInsertCommand4.Connection = this.sqlConnection1;
            this.sqlInsertCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 0, "id_f"),
            new System.Data.SqlClient.SqlParameter("@id_h", System.Data.SqlDbType.Int, 0, "id_h")});
            // 
            // sqlUpdateCommand4
            // 
            this.sqlUpdateCommand4.CommandText = "UPDATE [director] SET [id_f] = @id_f, [id_h] = @id_h WHERE (([id_f] = @Original_i" +
    "d_f) AND ([id_h] = @Original_id_h));\r\nSELECT id_f, id_h FROM director WHERE (id_" +
    "f = @id_f) AND (id_h = @id_h)";
            this.sqlUpdateCommand4.Connection = this.sqlConnection1;
            this.sqlUpdateCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 0, "id_f"),
            new System.Data.SqlClient.SqlParameter("@id_h", System.Data.SqlDbType.Int, 0, "id_h"),
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_id_h", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_h", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDeleteCommand4
            // 
            this.sqlDeleteCommand4.CommandText = "DELETE FROM [director] WHERE (([id_f] = @Original_id_f) AND ([id_h] = @Original_i" +
    "d_h))";
            this.sqlDeleteCommand4.Connection = this.sqlConnection1;
            this.sqlDeleteCommand4.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_id_h", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_h", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDataAdapter4
            // 
            this.sqlDataAdapter4.DeleteCommand = this.sqlDeleteCommand4;
            this.sqlDataAdapter4.InsertCommand = this.sqlInsertCommand4;
            this.sqlDataAdapter4.SelectCommand = this.sqlSelectCommand4;
            this.sqlDataAdapter4.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "director", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_f", "id_f"),
                        new System.Data.Common.DataColumnMapping("id_h", "id_h")})});
            this.sqlDataAdapter4.UpdateCommand = this.sqlUpdateCommand4;
            // 
            // sqlSelectCommand5
            // 
            this.sqlSelectCommand5.CommandText = "SELECT actors.*\r\nFROM     actors";
            this.sqlSelectCommand5.Connection = this.sqlConnection1;
            // 
            // sqlInsertCommand5
            // 
            this.sqlInsertCommand5.CommandText = "INSERT INTO [actors] ([id_f], [id_h]) VALUES (@id_f, @id_h);\r\nSELECT id_f, id_h F" +
    "ROM actors WHERE (id_f = @id_f) AND (id_h = @id_h)";
            this.sqlInsertCommand5.Connection = this.sqlConnection1;
            this.sqlInsertCommand5.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 0, "id_f"),
            new System.Data.SqlClient.SqlParameter("@id_h", System.Data.SqlDbType.Int, 0, "id_h")});
            // 
            // sqlUpdateCommand5
            // 
            this.sqlUpdateCommand5.CommandText = "UPDATE [actors] SET [id_f] = @id_f, [id_h] = @id_h WHERE (([id_f] = @Original_id_" +
    "f) AND ([id_h] = @Original_id_h));\r\nSELECT id_f, id_h FROM actors WHERE (id_f = " +
    "@id_f) AND (id_h = @id_h)";
            this.sqlUpdateCommand5.Connection = this.sqlConnection1;
            this.sqlUpdateCommand5.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 0, "id_f"),
            new System.Data.SqlClient.SqlParameter("@id_h", System.Data.SqlDbType.Int, 0, "id_h"),
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_id_h", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_h", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDeleteCommand5
            // 
            this.sqlDeleteCommand5.CommandText = "DELETE FROM [actors] WHERE (([id_f] = @Original_id_f) AND ([id_h] = @Original_id_" +
    "h))";
            this.sqlDeleteCommand5.Connection = this.sqlConnection1;
            this.sqlDeleteCommand5.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_id_h", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_h", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDataAdapter5
            // 
            this.sqlDataAdapter5.DeleteCommand = this.sqlDeleteCommand5;
            this.sqlDataAdapter5.InsertCommand = this.sqlInsertCommand5;
            this.sqlDataAdapter5.SelectCommand = this.sqlSelectCommand5;
            this.sqlDataAdapter5.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "actors", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_f", "id_f"),
                        new System.Data.Common.DataColumnMapping("id_h", "id_h")})});
            this.sqlDataAdapter5.UpdateCommand = this.sqlUpdateCommand5;
            // 
            // sqlSelectCommand6
            // 
            this.sqlSelectCommand6.CommandText = "SELECT humans.*\r\nFROM     humans";
            this.sqlSelectCommand6.Connection = this.sqlConnection1;
            // 
            // sqlInsertCommand6
            // 
            this.sqlInsertCommand6.CommandText = "INSERT INTO [humans] ([fio], [year]) VALUES (@fio, @year);\r\nSELECT id_h, fio, yea" +
    "r FROM humans WHERE (id_h = SCOPE_IDENTITY())";
            this.sqlInsertCommand6.Connection = this.sqlConnection1;
            this.sqlInsertCommand6.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@fio", System.Data.SqlDbType.NVarChar, 0, "fio"),
            new System.Data.SqlClient.SqlParameter("@year", System.Data.SqlDbType.Int, 0, "year")});
            // 
            // sqlUpdateCommand6
            // 
            this.sqlUpdateCommand6.CommandText = "UPDATE [humans] SET [fio] = @fio, [year] = @year WHERE (([id_h] = @Original_id_h)" +
    " AND ([fio] = @Original_fio) AND ([year] = @Original_year));\r\nSELECT id_h, fio, " +
    "year FROM humans WHERE (id_h = @id_h)";
            this.sqlUpdateCommand6.Connection = this.sqlConnection1;
            this.sqlUpdateCommand6.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@fio", System.Data.SqlDbType.NVarChar, 0, "fio"),
            new System.Data.SqlClient.SqlParameter("@year", System.Data.SqlDbType.Int, 0, "year"),
            new System.Data.SqlClient.SqlParameter("@Original_id_h", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_h", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_fio", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "fio", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_year", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "year", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@id_h", System.Data.SqlDbType.Int, 4, "id_h")});
            // 
            // sqlDeleteCommand6
            // 
            this.sqlDeleteCommand6.CommandText = "DELETE FROM [humans] WHERE (([id_h] = @Original_id_h) AND ([fio] = @Original_fio)" +
    " AND ([year] = @Original_year))";
            this.sqlDeleteCommand6.Connection = this.sqlConnection1;
            this.sqlDeleteCommand6.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_id_h", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_h", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_fio", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "fio", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_year", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "year", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDataAdapter6
            // 
            this.sqlDataAdapter6.DeleteCommand = this.sqlDeleteCommand6;
            this.sqlDataAdapter6.InsertCommand = this.sqlInsertCommand6;
            this.sqlDataAdapter6.SelectCommand = this.sqlSelectCommand6;
            this.sqlDataAdapter6.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "humans", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_h", "id_h"),
                        new System.Data.Common.DataColumnMapping("fio", "fio"),
                        new System.Data.Common.DataColumnMapping("year", "year")})});
            this.sqlDataAdapter6.UpdateCommand = this.sqlUpdateCommand6;
            // 
            // sqlSelectCommand7
            // 
            this.sqlSelectCommand7.CommandText = "SELECT genre.*\r\nFROM     genre";
            this.sqlSelectCommand7.Connection = this.sqlConnection1;
            // 
            // sqlInsertCommand7
            // 
            this.sqlInsertCommand7.CommandText = "INSERT INTO [genre] ([id_f], [id_g]) VALUES (@id_f, @id_g);\r\nSELECT id_f, id_g FR" +
    "OM genre WHERE (id_f = @id_f) AND (id_g = @id_g)";
            this.sqlInsertCommand7.Connection = this.sqlConnection1;
            this.sqlInsertCommand7.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 0, "id_f"),
            new System.Data.SqlClient.SqlParameter("@id_g", System.Data.SqlDbType.Int, 0, "id_g")});
            // 
            // sqlUpdateCommand7
            // 
            this.sqlUpdateCommand7.CommandText = "UPDATE [genre] SET [id_f] = @id_f, [id_g] = @id_g WHERE (([id_f] = @Original_id_f" +
    ") AND ([id_g] = @Original_id_g));\r\nSELECT id_f, id_g FROM genre WHERE (id_f = @i" +
    "d_f) AND (id_g = @id_g)";
            this.sqlUpdateCommand7.Connection = this.sqlConnection1;
            this.sqlUpdateCommand7.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 0, "id_f"),
            new System.Data.SqlClient.SqlParameter("@id_g", System.Data.SqlDbType.Int, 0, "id_g"),
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_id_g", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_g", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDeleteCommand7
            // 
            this.sqlDeleteCommand7.CommandText = "DELETE FROM [genre] WHERE (([id_f] = @Original_id_f) AND ([id_g] = @Original_id_g" +
    "))";
            this.sqlDeleteCommand7.Connection = this.sqlConnection1;
            this.sqlDeleteCommand7.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_id_g", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_g", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDataAdapter7
            // 
            this.sqlDataAdapter7.DeleteCommand = this.sqlDeleteCommand7;
            this.sqlDataAdapter7.InsertCommand = this.sqlInsertCommand7;
            this.sqlDataAdapter7.SelectCommand = this.sqlSelectCommand7;
            this.sqlDataAdapter7.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "genre", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_f", "id_f"),
                        new System.Data.Common.DataColumnMapping("id_g", "id_g")})});
            this.sqlDataAdapter7.UpdateCommand = this.sqlUpdateCommand7;
            // 
            // sqlSelectCommand8
            // 
            this.sqlSelectCommand8.CommandText = "SELECT genres.*\r\nFROM     genres";
            this.sqlSelectCommand8.Connection = this.sqlConnection1;
            // 
            // sqlInsertCommand8
            // 
            this.sqlInsertCommand8.CommandText = "INSERT INTO [genres] ([title]) VALUES (@title);\r\nSELECT id_g, title FROM genres W" +
    "HERE (id_g = SCOPE_IDENTITY())";
            this.sqlInsertCommand8.Connection = this.sqlConnection1;
            this.sqlInsertCommand8.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@title", System.Data.SqlDbType.NVarChar, 0, "title")});
            // 
            // sqlUpdateCommand8
            // 
            this.sqlUpdateCommand8.CommandText = "UPDATE [genres] SET [title] = @title WHERE (([id_g] = @Original_id_g) AND ([title" +
    "] = @Original_title));\r\nSELECT id_g, title FROM genres WHERE (id_g = @id_g)";
            this.sqlUpdateCommand8.Connection = this.sqlConnection1;
            this.sqlUpdateCommand8.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@title", System.Data.SqlDbType.NVarChar, 0, "title"),
            new System.Data.SqlClient.SqlParameter("@Original_id_g", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_g", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_title", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "title", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@id_g", System.Data.SqlDbType.Int, 4, "id_g")});
            // 
            // sqlDeleteCommand8
            // 
            this.sqlDeleteCommand8.CommandText = "DELETE FROM [genres] WHERE (([id_g] = @Original_id_g) AND ([title] = @Original_ti" +
    "tle))";
            this.sqlDeleteCommand8.Connection = this.sqlConnection1;
            this.sqlDeleteCommand8.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_id_g", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_g", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_title", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "title", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDataAdapter8
            // 
            this.sqlDataAdapter8.DeleteCommand = this.sqlDeleteCommand8;
            this.sqlDataAdapter8.InsertCommand = this.sqlInsertCommand8;
            this.sqlDataAdapter8.SelectCommand = this.sqlSelectCommand8;
            this.sqlDataAdapter8.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "genres", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_g", "id_g"),
                        new System.Data.Common.DataColumnMapping("title", "title")})});
            this.sqlDataAdapter8.UpdateCommand = this.sqlUpdateCommand8;
            // 
            // sqlSelectCommand9
            // 
            this.sqlSelectCommand9.CommandText = "SELECT marks.*\r\nFROM     marks";
            this.sqlSelectCommand9.Connection = this.sqlConnection1;
            // 
            // sqlInsertCommand9
            // 
            this.sqlInsertCommand9.CommandText = "INSERT INTO [marks] ([id_f], [mark]) VALUES (@id_f, @mark);\r\nSELECT id_o, id_f, m" +
    "ark FROM marks WHERE (id_o = SCOPE_IDENTITY())";
            this.sqlInsertCommand9.Connection = this.sqlConnection1;
            this.sqlInsertCommand9.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 0, "id_f"),
            new System.Data.SqlClient.SqlParameter("@mark", System.Data.SqlDbType.Int, 0, "mark")});
            // 
            // sqlUpdateCommand9
            // 
            this.sqlUpdateCommand9.CommandText = resources.GetString("sqlUpdateCommand9.CommandText");
            this.sqlUpdateCommand9.Connection = this.sqlConnection1;
            this.sqlUpdateCommand9.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@id_f", System.Data.SqlDbType.Int, 0, "id_f"),
            new System.Data.SqlClient.SqlParameter("@mark", System.Data.SqlDbType.Int, 0, "mark"),
            new System.Data.SqlClient.SqlParameter("@Original_id_o", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_o", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_mark", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "mark", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@id_o", System.Data.SqlDbType.Int, 4, "id_o")});
            // 
            // sqlDeleteCommand9
            // 
            this.sqlDeleteCommand9.CommandText = "DELETE FROM [marks] WHERE (([id_o] = @Original_id_o) AND ([id_f] = @Original_id_f" +
    ") AND ([mark] = @Original_mark))";
            this.sqlDeleteCommand9.Connection = this.sqlConnection1;
            this.sqlDeleteCommand9.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_id_o", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_o", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_id_f", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "id_f", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_mark", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "mark", System.Data.DataRowVersion.Original, null)});
            // 
            // sqlDataAdapter9
            // 
            this.sqlDataAdapter9.DeleteCommand = this.sqlDeleteCommand9;
            this.sqlDataAdapter9.InsertCommand = this.sqlInsertCommand9;
            this.sqlDataAdapter9.SelectCommand = this.sqlSelectCommand9;
            this.sqlDataAdapter9.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "marks", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("id_o", "id_o"),
                        new System.Data.Common.DataColumnMapping("id_f", "id_f"),
                        new System.Data.Common.DataColumnMapping("mark", "mark")})});
            this.sqlDataAdapter9.UpdateCommand = this.sqlUpdateCommand9;
            // 
            // dataSet11
            // 
            this.dataSet11.DataSetName = "DataSet1";
            this.dataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 373);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form2";
            this.Text = "Редактирование таблицы";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Data.SqlClient.SqlConnection sqlConnection1;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand1;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand1;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand1;
        private System.Data.SqlClient.SqlDataAdapter sqlDataAdapter1;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand2;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand2;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand2;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand2;
        private System.Data.SqlClient.SqlDataAdapter sqlDataAdapter2;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand3;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand3;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand3;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand3;
        private System.Data.SqlClient.SqlDataAdapter sqlDataAdapter3;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand4;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand4;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand4;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand4;
        private System.Data.SqlClient.SqlDataAdapter sqlDataAdapter4;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand5;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand5;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand5;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand5;
        private System.Data.SqlClient.SqlDataAdapter sqlDataAdapter5;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand6;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand6;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand6;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand6;
        private System.Data.SqlClient.SqlDataAdapter sqlDataAdapter6;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand7;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand7;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand7;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand7;
        private System.Data.SqlClient.SqlDataAdapter sqlDataAdapter7;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand8;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand8;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand8;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand8;
        private System.Data.SqlClient.SqlDataAdapter sqlDataAdapter8;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand9;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand9;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand9;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand9;
        private System.Data.SqlClient.SqlDataAdapter sqlDataAdapter9;
        private DataSet1 dataSet11;

    }
}