﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kurs
{
    public partial class Form2 : Form
    {
        public Form2(string TableName)
        {
            InitializeComponent();

            dataGridView1.DataSource = dataSet11;
            dataGridView1.DataMember = TableName;

            sqlDataAdapter1.Fill(dataSet11);
            sqlDataAdapter2.Fill(dataSet11);
            sqlDataAdapter3.Fill(dataSet11);
            sqlDataAdapter4.Fill(dataSet11);
            sqlDataAdapter5.Fill(dataSet11);
            sqlDataAdapter6.Fill(dataSet11);
            sqlDataAdapter7.Fill(dataSet11);
            sqlDataAdapter8.Fill(dataSet11);
            sqlDataAdapter9.Fill(dataSet11);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sqlDataAdapter1.Update(dataSet11);
            sqlDataAdapter2.Update(dataSet11);
            sqlDataAdapter3.Update(dataSet11);
            sqlDataAdapter4.Update(dataSet11);
            sqlDataAdapter5.Update(dataSet11);
            sqlDataAdapter6.Update(dataSet11);
            sqlDataAdapter7.Update(dataSet11);
            sqlDataAdapter8.Update(dataSet11);
            sqlDataAdapter9.Update(dataSet11);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            switch (dataGridView1.DataMember)
            {
                case "film": dataSet11.film.RejectChanges();
                    break;

                case "country": dataSet11.country.RejectChanges();
                    break;
                case "countryes": dataSet11.countryes.RejectChanges();
                    break;
                case "director": dataSet11.director.RejectChanges();
                    break;
                case "actors": dataSet11.actors.RejectChanges();
                    break;
                case "humans": dataSet11.humans.RejectChanges();
                    break;
                case "genre": dataSet11.genre.RejectChanges();
                    break;
                case "genres": dataSet11.genres.RejectChanges();
                    break;
                case "marks": dataSet11.marks.RejectChanges();
                    break;
            }
        }
    }
}
