﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kurs
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {sqlSelectCommand1.CommandText = richTextBox1.Text;
             DataSet dss = new DataSet();
             sqlDataAdapter1.Fill(dss);
             dataGridView1.DataSource = dss;
             DataTable dts = new DataTable();
             dts = dss.Tables[0];
            dataGridView1.DataMember = dts.ToString();}
            catch
            { MessageBox.Show("Не верный запрос!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error); }

        }

        }
    
}
