﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kurs
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
           // Страны
            sqlDataAdapter1.Fill(dataSet21);
            DataSet dco1 = new DataSet();
            DataSet dco2 = new DataSet();
            DataSet dco3 = new DataSet();
            dco1 = dataSet21.Copy();
            dco2 = dataSet21.Copy();
            dco3 = dataSet21.Copy();
            county.DataSource = dco1;
            county.DisplayMember = "countryes.title";
            county2.DataSource = dco2;
            county2.DisplayMember = "countryes.title";
            county3.DataSource = dco3;
            county3.DisplayMember = "countryes.title";
            //Люди
            sqlDataAdapter2.Fill(dataSet31);
            DataSet ds1 = new DataSet();
            ds1 = dataSet31.Copy();
            DataSet ds2 = new DataSet();
            ds2 = dataSet31.Copy();
            DataSet ds3 = new DataSet();
            ds3 = dataSet31.Copy();
            DataSet ds4 = new DataSet();
            ds4 = dataSet31.Copy();
            actor1.DataSource = ds1;
            actor1.DisplayMember = "humans.fio";
            actor2.DataSource = ds2;
            actor2.DisplayMember = "humans.fio";
            actor3.DataSource = ds3;
            actor3.DisplayMember = "humans.fio";

            director.DataSource = ds4;
            director.DisplayMember = "humans.fio";
            //Жанры
            sqlDataAdapter3.Fill(dataSet41);
            DataSet dg1 = new DataSet();
            dg1 = dataSet41.Copy();
            DataSet dg2 = new DataSet();
            dg2 = dataSet41.Copy();
            DataSet dg3 = new DataSet();
            dg3 = dataSet41.Copy();
            genre1.DataSource = dg1;
            genre2.DataSource = dg2;
            genre3.DataSource = dg3;
            genre1.DisplayMember = "genres.title";
            genre2.DisplayMember = "genres.title";
            genre3.DisplayMember = "genres.title";
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
                actor2.Enabled = true;
            else
                actor2.Enabled = false;

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                actor3.Enabled = true;
            else
                actor3.Enabled = false;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
                genre2.Enabled = true;
            else
                genre2.Enabled = false;
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
                genre3.Enabled = true;
            else
                genre3.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //обновление режиссера и актеров
            sqlDataAdapter2.Fill(dataSet31);
            DataSet ds1 = new DataSet();
            ds1 = dataSet31.Copy();
            DataSet ds2 = new DataSet();
            ds2 = dataSet31.Copy();
            DataSet ds3 = new DataSet();
            ds3 = dataSet31.Copy();
            DataSet ds4 = new DataSet();
            ds4 = dataSet31.Copy();
            actor1.DataSource = ds1;
            actor1.DisplayMember = "humans.fio";
            actor2.DataSource = ds2;
            actor2.DisplayMember = "humans.fio";
            actor3.DataSource = ds3;
            actor3.DisplayMember = "humans.fio";

            director.DataSource = ds4;
            director.DisplayMember = "humans.fio";


        }

        private void button5_Click(object sender, EventArgs e)
        {
            //обновление страны
            sqlDataAdapter1.Fill(dataSet21);
            DataSet dco1 = new DataSet();
            DataSet dco2 = new DataSet();
            DataSet dco3 = new DataSet();
            dco1 = dataSet21.Copy();
            dco2 = dataSet21.Copy();
            dco3 = dataSet21.Copy();
            county.DataSource = dco1;
            county.DisplayMember = "countryes.title";
            county2.DataSource = dco2;
            county2.DisplayMember = "countryes.title";
            county3.DataSource = dco3;
            county3.DisplayMember = "countryes.title";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //обновление жанры
            sqlDataAdapter3.Fill(dataSet41);
            DataSet dg1 = new DataSet();
            dg1 = dataSet41.Copy();
            DataSet dg2 = new DataSet();
            dg2 = dataSet41.Copy();
            DataSet dg3 = new DataSet();
            dg3 = dataSet41.Copy();
            genre1.DataSource = dg1;
            genre2.DataSource = dg2;
            genre3.DataSource = dg3;
            genre1.DisplayMember = "genres.title";
            genre2.DisplayMember = "genres.title";
            genre3.DisplayMember = "genres.title";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
               //компонуем запрос к базе, для начала запишем данные по фильму
                string command = "execute add_film '" + Title.Text + "', " + year.Text + ", " + length.Text + ", ";

                //вставка страны
                command = command + "'" + county.Text + "', ";
                if (county2.Enabled) command = command + "'" + county2.Text + "', ";
                else command = command + "'', ";

                if (county3.Enabled) command = command + "'" + county3.Text + "', ";
                else command = command + "'', ";
                //вставка режиссера

                command = command + "'" + director.Text + "', ";

                //вставка актеров
                command = command + "'" + actor1.Text + "', ";
                if (actor2.Enabled) command = command + "'" + actor2.Text + "', ";
                else command = command + "'', ";
                if (actor3.Enabled) command = command + "'" + actor3.Text + "', ";
                else command = command + "'', ";
                //вставка жанров
                command = command + "'" + genre1.Text + "', ";
                if (genre2.Enabled) command = command + "'" + genre2.Text + "', ";
                else command = command + "'', ";
                if (genre3.Enabled) command = command + "'" + genre3.Text + "'";
                else command = command + "''";

                sqlSelectCommand4.CommandText = command;
                sqlDataAdapter4.Fill(dataSet51);
                MessageBox.Show("Поздравляем!!!!", "Запись успешно вставлена", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

            }
            catch
            { MessageBox.Show("Не верный запрос!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error); }
           
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked)
                county3.Enabled = true;
            else
                county3.Enabled = false;
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
                county2.Enabled = true;
            else
                county2.Enabled = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form2 f42 = new Form2("countryes");
            f42.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 f4h = new Form2("humans");
            f4h.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form2 f4g = new Form2("genres");
            f4g.Show();
        }

    }
}
