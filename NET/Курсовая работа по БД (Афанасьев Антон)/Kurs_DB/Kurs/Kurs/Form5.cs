﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kurs
{
    public partial class Form5 : Form
    {
        DataSet dsq = new DataSet();
        DataTable dts = new DataTable();
       //те кто не имеют оценок
        string s1 = "SELECT * FROM    film f WHERE f.id_f not in (select id_f From marks)";
        //те кто имеют оценки
        string s2 = "SELECT f.id_f, f.title, f.length ,f.year, avg(m.mark) as [mark] FROM    film f inner join marks m on f.id_f = m.id_f group by f.title,f.year, f.length,f.id_f ";
        
        public Form5()
        {
           
            InitializeComponent();
            
            sqlSelectCommand1.CommandText = s1;
           
            sqlDataAdapter1.Fill(dsq);
            dataGridView1.DataSource = dsq;
            
            dts = dsq.Tables[0];
            dataGridView1.DataMember = dts.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //добавление оценки по фильму в базу
            try
            {
                int s = Convert.ToInt32(dataGridView1[0, dataGridView1.CurrentRow.Index].Value.ToString());
                sqlSelectCommand1.CommandText = "execute add_mark " + s + ", " + Convert.ToInt32(textBox1.Text);
                sqlDataAdapter1.Fill(dsq);
            }
            catch
            {   MessageBox.Show("Не верный запрос!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);}
            
            }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {//переключатель между теми фильмами у которых есть оценка и теми у которых нет
            if (checkBox1.Checked)
            {
                dsq.Clear();
                sqlSelectCommand1.CommandText = s2;

                sqlDataAdapter1.Fill(dsq);
                dataGridView1.DataSource = dsq;

                dts = dsq.Tables[0];
                dataGridView1.DataMember = dts.ToString();
            }
            else
            {
                dsq.Clear();
                sqlSelectCommand1.CommandText = s1;

                sqlDataAdapter1.Fill(dsq);
                dataGridView1.DataSource = dsq;

                dts = dsq.Tables[0];
                dataGridView1.DataMember = dts.ToString();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {//запрос к базе в результате которого получим похожие на выделенный фильм
            try
            {
                sqlSelectCommand1.CommandText = "Select * from idetifing (" + dataGridView1[0, dataGridView1.CurrentRow.Index].Value.ToString() + ")" ;
                DataSet dss = new DataSet();
                sqlDataAdapter1.Fill(dss);
                dataGridView2.DataSource = dss;
                DataTable dts = new DataTable();
                dts = dss.Tables[0];
                dataGridView2.DataMember = dts.ToString();
            }
            catch
            { MessageBox.Show("Не верный запрос!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
    }
}
