import java.util.Calendar
import java.util.Calendar._

/**
Шахматная ассоциация решила оснастить всех своих сотрудников такими телефонными номерами, которые бы набирались
на кнопочном телефоне ходом коня. Например, ходом коня набирается телефон 340-49-27. При этом телефонный номер не
может начинаться ни с цифры 0, ни с цифры 8.

Требуется написать программу, определяющую количество телефонных номеров длины N, набираемых ходом коня.

Входные данные
Входной файл INPUT.TXT содержит натуральное число N (N <= 100).

Выходные данные
В выходной файл OUTPUT.TXT выведите искомое количество телефонных номеров.
 */



object horseGo{
  def main (args: Array[String]) {
    val t = getInstance().getTimeInMillis()
    println(start(100))
    println(getInstance().getTimeInMillis() - t)
  }
  def recur(prev: Int, len: Int): Int ={
    if(len == 0){ 1 }
    else {
      var count = 0
      if(prev == 1){
        count += recur(6, len - 1)
        count += recur(8, len - 1)
      }
      if(prev == 2){
        count += recur(7, len - 1)
        count += recur(9, len - 1)
      }
      if(prev == 3){
        count += recur(4, len - 1)
        count += recur(8, len - 1)
      }
      if(prev == 4){
        count += recur(3, len - 1)
        count += recur(9, len - 1)
        count += recur(0, len - 1)
      }
      if(prev == 5){ 0}
      if(prev == 6){
        count += recur(1, len - 1)
        count += recur(7, len - 1)
        count += recur(0, len - 1)
      }
      if(prev == 7){
        count += recur(2, len - 1)
        count += recur(6, len - 1)
      }
      if(prev == 8){
        count += recur(1, len - 1)
        count += recur(3, len - 1)
      }
      if(prev == 9){
        count += recur(2, len - 1)
        count += recur(4, len - 1)
      }
      if(prev == 0){
        count += recur(4, len - 1)
        count += recur(6, len - 1)
      }
      println(count)
      count
    }
  }
  def start(len: Int): Int ={
    var count = 0
    count += recur(1, len - 1)
    count += recur(2, len - 1)
    count += recur(3, len - 1)
    count += recur(4, len - 1)
    count += recur(5, len - 1)
    count += recur(6, len - 1)
    count += recur(7, len - 1)
    count += recur(9, len - 1)

    count
  }
}