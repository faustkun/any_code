import java.util.Calendar

/**
Имеется ряд из N лампочек, которые пронумерованы от 1 до N. Изначально ни одна из лампочек не горит. Далее происходит
K последовательных линейных инверсий этого ряда ламп. Под линейной инверсией понимается инверсия каждой P-й лампочки в
ряде. Например, если P=3, то произойдет инверсия 3й, 6й, 9й и т.д. лампочек.

Требуется определить: сколько горящих лампочек останется после реализации всех заданных линейных инверсий?

Входные данные
В первой строке входного файла INPUT.TXT заданны числа N и K – число лампочек и число линейных инверсий. Вторая строка
состоит из K целых чисел Pi, задающих период данных инверсий. (1 <= N <= 109, 1<=K<=100, 1 <= Pi <= 50)

Выходные данные
В выходной файл OUTPUT.TXT следует вывести ответ на задачу.
 */

object lampochka{
  def lampochka(len: Int, arr: Array[Int]): Int ={
    var count = 0
    for (x <- 1 to len){
      var counter = false
      for(y <- arr){
        if(x%y == 0)
          counter = !counter
      }
      if (counter) {
        count += 1
      }
    }
    count
  }
  def main (args: Array[String]) {
    //print(lampochka(20, Array(2, 3, 8)))
    var t = Calendar.getInstance().getTimeInMillis()
    println(lampochka(99999, Array(19, 2, 7, 13, 40, 23, 16, 1, 45, 9)))
    t = Calendar.getInstance().getTimeInMillis() - t
    println(t)
  }
}
