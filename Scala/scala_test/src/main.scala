/**
 * Created by Антон on 27.04.2015.
 */
object dragon{
  def dragonkiller(sword: Int, dragon: Int, regen: Int): Any ={
    if (sword - regen > 0) {
      (dragon - sword)/(sword - regen) + 1
    }
    else if (dragon <= sword){
      1
    }
    else "NO"
  }
  def main(args:Array[String]){
    println(dragonkiller(3, 10, 2))
  }
}