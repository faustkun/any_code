/**
Необходимо вывести все простые числа от M до N включительно.
Входные данные:
Входной файл INPUT.TXT содержит два натуральных числа M и N, разделенных пробелом (2 <= M <= N <= 106)
Выходные данные:
В выходной файл OUTPUT.TXT выведите в одной строке через пробел все простые числа от M до N в порядке возрастания.
Если таковых чисел нет, то следует вывести «Absent».
 */

object numbers{
  def prime(x: Int): Boolean = {
    for ( i <-2  to math.sqrt(x).round.toInt){
      if(x%i == 0 & i != 1)
        return false
    }
    true
  }
  def numbers(a: Int, b: Int): Any = {
    val list = a.to(b).filter(prime _)
    if (list.length == 0) {
      "Absent"
    }else {
      list
    }
  }
  def main (args: Array[String]) {
    println(numbers(2, 106))
  }
}
