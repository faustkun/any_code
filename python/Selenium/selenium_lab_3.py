# -*- coding: utf-8 -*-
__author__ = 'faustkun'
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


def get_all_urls(browser):
    temp = browser.find_elements_by_class_name('r')
    while len(temp) == 0:
        time.sleep(0.2)
        temp = browser.find_elements_by_class_name('r')
    temp2 = []
    for i in temp:
        temp2.append(i.find_element_by_css_selector('a[href]').get_attribute('href'))
    # print(len(temp2))
    if len(temp) == len(temp2):
        return temp2
    else:
        return []


def first_search(browser, str, sleep_seconds):
    browser.get("https://www.google.ru/")
    element = browser.find_element_by_name("q")

    element.send_keys(str + Keys.RETURN)
    time.sleep(sleep_seconds)

    urls = get_all_urls(browser)

    current_url = browser.current_url

    browser.get(current_url + '&start=10')
    time.sleep(sleep_seconds)

    urls = urls + get_all_urls(browser)

    browser.get(current_url + '&start=20')
    time.sleep(sleep_seconds)

    urls = urls + get_all_urls(browser)
    return urls


def second_search(browser, str, sleep_seconds):
    browser.get("https://www.google.ru/")
    element = browser.find_element_by_name("q")

    element.send_keys(str + Keys.RETURN)
    time.sleep(sleep_seconds)

    urls = get_all_urls(browser)
    return urls



# data
str1 = '"Эффективное тестирование программ"'
str2 = "Эффективное тестирование программ"
sleep_seconds = 1
######

browser = webdriver.Firefox()

urls_first_search = first_search(browser, str1, sleep_seconds)
urls_second_search = second_search(browser, str2, sleep_seconds)

for i in urls_second_search:
    if i in urls_first_search:
        q = 0
        for j in urls_first_search:
            if i == j:
                q += 1
        print(i + "   " + str(q) + "  раз")
browser.close()
