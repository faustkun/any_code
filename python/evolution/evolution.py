__author__ = 'faustkun'
import generator
import reproduse
import mutate
import time
from individ import *

population_count = 200
otbor_count = 190
individ_len = 10
etalon = '42'

result = []
generation = 1
#первое поколение
arr = [generator.generator(individ_len) for i in range(population_count)]

t = time.time()
while len(result) == 0:
    #скрещивание
    arr = reproduse.reproduse(arr)
    #мутация
    arr = mutate.mutate(arr, 0.3)
    #отбор
    res = [Individ(i, etalon) for i in arr]
    res.sort(key=lambda x: x.fitness)
    res = res[:otbor_count] + res[:(population_count - otbor_count)]
    #ищем соответсвие эталону
    result = [i.data for i in res if i.data == etalon]
    #печатаем результаты лучших
    if generation%10 == 0:
        print("Поколение ", generation, '\n', 'Лучшие в поколении:')
        for i in range(3):
            print(i+1, ".", 'res:', res[i].result, 'fitness:', res[i].fitness)

        print("Времени затрачено на 10 поколений", time.time() - t, 'секунд')
        t = time.time()
    #копипастим исходники программ обратно в arr
    arr = [i.data for i in res]
    generation += 1


print("таки дела малята")
for i in result:
    print(i)