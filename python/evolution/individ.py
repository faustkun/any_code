__author__ = 'faustkun'
import brainfuck
import math

class Individ:
    data =''
    result = ''
    etalon = ''
    fitness = float('inf')
    def __init__(self, data, etalon):
        self.data = data
        self.etalon = etalon
        try:
            self.result = brainfuck.run(data, 0.001)[:-1]
            self.__fitness__()
        except:
            return

    def __fitness__(self):
        if len(self.result) < len(self.etalon):
            self.fitness = 0
            for i in range(len(self.result)):
                self.fitness += math.fabs(ord(self.result[i]) - ord(self.etalon[i])) * (10 ** (i + 1))
            for i in range(len(self.result), len(self.etalon)):
                self.fitness += ord(self.etalon[i]) * (10 ** (i + 1))
        else:
            self.fitness = 0
            for i in range(len(self.etalon)):
                self.fitness += math.fabs(ord(self.result[i]) - ord(self.etalon[i])) * (10 ** (i + 1))
            for i in range(len(self.etalon), len(self.result)):
                self.fitness += ord(self.result[i]) * (10 ** (i + 1))

'''
n = Individ("++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.", 'Hello World!')
print(n.result)
print(n.fitness)

for i in n.etalon: print(ord(i), end = " ")
print(" ")
for i in n.result: print(ord(i), end = " ")
'''