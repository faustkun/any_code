__author__ = 'faustkun'
import random

def mutate(arr, mutation):
    #slovar = '+-><.,[]'
    slovar = '+-><.[]'
    for i in range(int((len(arr) - 1) * mutation)):
        ind = random.randint(0, len(arr) - 1)
        #print(arr[ind])
        sp = random.randint(0, len(arr[ind]) - 1)
        type = random.randint(1, 3)
        str = arr[ind]
        #замена
        if type == 1:
            str = str[:sp - 1] + slovar[random.randint(0, len(slovar) - 1)] + str[sp:]
        #вставка
        elif type == 2:
            str = str[:sp] + slovar[random.randint(0, len(slovar) - 1)] + str[sp:]
        #цикл
        elif type == 3:
            sp2 = random.randint(sp, len(arr[ind]) - 1)
            str = str[:sp] + '[' + str[sp:sp2] + ']' + str[sp2:]
        arr[ind] = str
    return arr


'''
arr = ['+++++++++++',
       '+++++++++++',
       '+++++++++++',
       '+++++++++++',
       '+++++++++++',
       '+++++++++++',
       ]
arr = mutate(arr, 1)
for i in arr: print(i)
'''