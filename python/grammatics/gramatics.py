# -*- coding: utf-8 -*-
__author__ = 'Антон'
import re
#нетерминальные, терминальные, правила, стартовый символ
ruleStart = re.compile(u"\s*->\s*")
ruleRule = re.compile(u"\s*\\|\s*")

def recurRuleGo(rules, input, output):
    flag = 1
    for i in range(len(rules)):

        temp = input.find(rules[i][0])
        #print temp
        if temp > -1:
            flag = 0
            for j in range(len(rules[i]) - 1):
                newInput = input.replace(rules[i][0], rules[i][j + 1], 1)
                recurRuleGo(rules, newInput, output)
    if len(output) > 10000:
        return 1
    elif flag == 1:
        output.add(input)

#основной код программы
rules = [[u"@", ""]]
output = set([])
q = "Введите правило и нажмите ENTER или ничего не вводите для завершения ввода\n" \
    "правило вводить в форме S -> a|b|c|@ (@ - зарезервировано  под эпсилон)\n"
c = 'ololo'
#читаем правила
while (c):
   c = raw_input(q)
   if c:
    q = ""
    rul = ruleStart.split(c)
    start = rul[0]
    rule = ruleRule.split(rul[1])
    rules.append([start] + rule)

#читаем стартовую комбинацию
recurRuleGo(rules, unicode(raw_input(u"Введите пожалуйста стартовую строку\n")), output)
print output, len(output)