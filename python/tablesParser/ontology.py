import re
from tkinter import *
from tkinter.filedialog import *
from tkinter.messagebox import *

declaration = re.compile(r"<Declaration>.+?<\/Declaration>")
re_classes = re.compile(r"<Class IRI=.+?/>")
re_relation = re.compile(r"<ObjectProperty IRI=.+?/>")
re_properties = re.compile(r"<DataProperty IRI=.+?/>")
re_individual = re.compile(r"<NamedIndividual IRI=.+?/>")
re_assertion = re.compile(r"<ClassAssertion>.+?</ClassAssertion>")
re_obj_relation = re.compile(r"<ObjectPropertyAssertion>.+?</ObjectPropertyAssertion>")
re_data_prop = re.compile(r"<DataPropertyAssertion>.+?</DataPropertyAssertion>")
re_literal = re.compile(r'<Literal datatypeIRI=.+?</Literal>')
re_inverse = re.compile(r"<InverseObjectProperties>.+?</InverseObjectProperties>")

def work(path):
    file = open(path, 'r')
    text = file.read().replace('\n', "")
    q = declaration.findall(text)
    #классы
    global classes
    classes = [re_classes.findall(i)[0][13:-3].replace("_", " ") for i in q if re_classes.findall(i)]
    #отношения
    global relation
    relation = [re_relation.findall(i)[0][22:-3].replace("_", " ") for i in q if re_relation.findall(i)]
    #свойства
    global  properties
    properties = [re_properties.findall(i)[0][20:-3].replace("_", " ") for i in q if re_properties.findall(i)]
    #индивиды
    global individual
    individual = [re_individual.findall(i)[0][23:-3].replace("_", " ") for i in q if re_individual.findall(i)]
    #присвоение классов
    global assertion
    q = re_assertion.findall(text)
    assertion = [(re_classes.findall(i)[0][13:-3].replace("_", " "), re_individual.findall(i)[0][23:-3].replace("_", " ")) for i in q]
    #проименованные связи между объектами
    global obj_relation
    q = re_obj_relation.findall(text)
    obj_relation = [[re_relation.findall(i)[0][22:-3].replace("_", " "), re_individual.findall(i)[0][23:-3].replace("_", " "),
                     re_individual.findall(i)[1][23:-3].replace("_", " ")] for i in q]
    #вывод свойства
    global data_prop
    q = re_data_prop.findall(text)
    data_prop = [[re_properties.findall(i)[0][20:-3].replace("_", " "), re_individual.findall(i)[0][23:-3].replace("_", " "),
                  re_literal.findall(i)[0].split(">")[1][:-9].replace("_", " ")] for i in q]
    #Выделение обратных отношений
    global inverses
    q = re_inverse.findall(text)
    inverses = [[re_relation.findall(i)[0][22:-3].replace("_", " "), re_relation.findall(i)[1][22:-3].replace("_", " ")] for i in q]
    return


def fd(event):
    op = askopenfilename()
    if op != "":
        ent.delete(0, END)
        ent.insert(1, op)


def ft(event):
    if ent.get().split(".")[-1] != 'owl':
        showerror("Ошибка", "Неверное расширение файла!")
        return
    try:
        work(ent.get())
        classes_list.delete(0, END)
        for i in classes:
            classes_list.insert(END, i)
        individs_list.delete(0, END)
        for i in individual:
            individs_list.insert(END, i)

    except:
        showerror("Ошибка", "Файл не найден!")

def tt(event):
    print(classes)

def class_individ(event):
    individs_list.delete(0, END)
    t = classes_list.get(classes_list.curselection())
    for i in assertion:
        if i[0] == t:
            individs_list.insert(END, i[1])

def individ_stats(event):
    temp = obj_relation[:]
    temp2 = obj_relation[:]
    text.delete(1.0, END)
    t = individs_list.get(individs_list.curselection())
    counter = 1

    inv = inverses[:]
    for i in obj_relation:
        if i in temp and i[1] == t:
            second = ""
            for k in inv:
                if k[0] == i[0]:
                    second = k[1]
                if k[1] == i[0]:
                    second = k[0]
            print(second)
            text.insert(END, "Связь " + str(counter) + '\n')
            text.insert(END, "Субъект: " + i[1] + '\n')
            text.insert(END, "Отношение: " + i[0])
            if second != "": text.insert(END, " & " + second + '\n')
            text.insert(END, "Объекты: " + "\n")
            for j in obj_relation:
                if j in temp and j[0] == i[0] and j[1] == i[1]:
                    text.insert(END, j[2] + '\n')
                    #print(temp.index(j))
                    temp.pop(temp.index(j))
            for j in obj_relation:
                if j in temp2 and j[0] == second and j[1] == i[1]:
                    print(j)
                    text.insert(END, j[2] + '\n')
                    #print(temp.index(j))
                    temp2.pop(temp2.index(j))
            counter += 1


root = Tk()
root.title("Анализ текстов бесплатно и без смс")
root.minsize(width=1024, height=768)

button1 = Button(root, text="Обзор", font="Arial 14")
button1.bind("<ButtonRelease-1>", fd)
button2 = Button(root, text="Загрузить", font="Arial 14")
button2.bind("<ButtonRelease-1>", ft)
button3 = Button(root, text="запуск", font="Arial 14")
button3.bind("<ButtonRelease-1>", tt)

classes_list = Listbox(root, selectmode=SINGLE, font="Arial 14", height=25)
individs_list = Listbox(root, selectmode=SINGLE, font="Arial 14", width=30, height=25)

classes_list.bind('<<ListboxSelect>>', class_individ)
individs_list.bind('<<ListboxSelect>>', individ_stats)
lable0 = Label(root, text="Выберите файл с онтологией", font="Arial 14")
lable1 = Label(root, text="Классы", font="Arial 14")
lable2 = Label(root, text="Индивиды", font="Arial 14")



ent = Entry(root, width=40, font="Arial 14")
text = Text(root, width=40, wrap=WORD, font="Arial 14", height=26)
text.insert(1.0, "asasd asdas asdasd a dadfgdfgdfgdfgdfgdfgdfgdfgdfgdfgdfgdfgdfgsd")
#text.config(state=DISABLED)



lable0.place(x=10, y=14)
ent.place(x=10, y=44)
button1.place(x=430, y=40)
button2.place(x=527, y=40)
lable1.place(x=10, y=85)
lable2.place(x=230, y=85)
classes_list.place(x=10, y=120)
individs_list.place(x=230, y=120)
text.place(x=550, y=120)
button3.place(x=230, y=700)


root.mainloop()