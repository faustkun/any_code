__author__ = 'faustkun'
# import re
import time

# "’"|"'"|"["|"]"|"("|")"|"{"|"}"|"⟨"|"⟩"|":"|","|"‒"|"–"|"—"|"―"|"„"|"“"|"«"|"»"|"“"|"”"|"‘"|"’"|"‹"|"›"|"/"|"⁄"|"&"|"@"|"*"|"\\\\"|"•"|"^"|"†"|"‡"|"°"|"¡"|"¿"|"#"|"÷"|"¶"|"′"|"″"|"‴"|"§"|"~"|"_"|"¦"|"|"|""|""|""|""|
#p = re.compile(",’'\[\]\(\)\{\}⟨⟩:,‒–—―„“«»“”‘’‹›/⁄&@\*\\\\•^†‡°¡¿#÷¶′″‴§~_¦\|")
#p = re.compile("(, | \")")

words = []
hum = dict([])
persents = []


def GetKey(item):
    return [item[2]]


def read_from_file(file):
    f = open(file, "r")
    string = ""
    for line in f:
        string += line
    string = string.replace("\n", " ").lower()
    #знаки препинания
    string = string.replace("’", "")
    string = string.replace("'", "")
    string = string.replace("[", "")
    string = string.replace("]", "")
    string = string.replace("(", "")
    string = string.replace(")", "")
    string = string.replace("{", "")
    string = string.replace("}", "")
    string = string.replace("⟨", "")
    string = string.replace("⟩", "")
    string = string.replace(":", "")
    string = string.replace(",", "")
    string = string.replace("‒", " ")
    string = string.replace("–", " ")
    string = string.replace("—", " ")
    string = string.replace("―", " ")
    string = string.replace("„", "")
    string = string.replace("“", "")
    string = string.replace("«", "")
    string = string.replace("»", "")
    string = string.replace("“", "")
    string = string.replace("”", "")
    string = string.replace("‘", "")
    string = string.replace("’", "")
    string = string.replace("‹", "")
    string = string.replace("›", "")
    string = string.replace("/", " ")
    string = string.replace("⁄", " ")
    string = string.replace("&", " ")
    string = string.replace("@", " ")
    string = string.replace("*", " ")
    string = string.replace("\\\\", "")
    string = string.replace("•", "")
    string = string.replace("^", " ")
    string = string.replace("†", "")
    string = string.replace("‡", "")
    string = string.replace("°", "")
    string = string.replace("¡", "")
    string = string.replace("¿", "")
    string = string.replace("#", "")
    string = string.replace("÷", " ")
    string = string.replace("¶", "")
    string = string.replace("′", "")
    string = string.replace("″", "")
    string = string.replace("‴", "")
    string = string.replace("§", "")
    string = string.replace("~", " ")
    string = string.replace("_", " ")
    string = string.replace("¦", " ")
    string = string.replace("|", " ")
    string = string.replace('"', "")


    #знаки окончания предложения
    string = string.replace("·", ".")
    string = string.replace("…", ".")
    string = string.replace("...", ".")
    string = string.replace(". . .", ".")
    string = string.replace("!", ".")
    string = string.replace(";", ".")
    string = string.replace("?", ".")

    string = string.split(".")

    q = [x for x in string if x != '']
    print("Чтение из файла завершено")
    return q


def to_words_to_links2(words, links, data):
    print("Линковка начата")
    temp = len(data)
    for i in data:
        print("Предложение №", data.index(i), "/", temp)
        #разрезаем строку на слова
        q = i.split(" ")
        q = [x for x in q if x != '']
        q = list(set(q))

        #добавляем новые слова в список слов
        for ii in q:
            if ii not in words:
                words.append(ii)

        #добавляем связи между словами
        for ii in q:
            for jj in q:
                if ii != jj:
                    pair = (words.index(ii), words.index(jj))
                    if pair not in links:
                        links.append(pair)
    print("Линковка завершена")


def to_words(words, data):
    print("Линковка начата")
    temp = len(data)
    for i in data:
        print("Предложение №", data.index(i), "/", temp)
        #разрезаем строку на слова
        q = i.split(" ")
        q = [x for x in q if x != '']
        q = list(set(q))

        #добавляем новые слова в список слов
        for ii in q:
                words.append(ii)
    words = list(set(words))


def humanisation(words, hum):
    print("Начато очеловечивание данных")
    for i in words:
        hum[i] = set([])

    temp = len(data)
    for i in data:
        #print("Предложение №", data.index(i), "/", temp)
        #разрезаем строку на слова
        q = i.split(" ")
        q = [x for x in q if x != '']
        q = list(set(q))

        for ii in q:
            for jj in q:
                if ii != jj:
                    hum[ii].add(jj)
    print("Очеловечивание данных закончено")


def persents_calc(words, hum, persents, stime):
    for i in range(len(words)):
        print("Анализ", i, "из ", len(words))
        for j in range(i + 1, len(words)):
            temp = len(hum[words[i]]) + len(hum[words[j]])
            if temp != 0:
                #print(words[i], " ", words[j])
                temp = len(hum[words[i]] ^ hum[words[j]]) / temp
                if (temp < 0.2) and (temp > 0.1): persents. append([words[i], words[j], temp])
                if (time.time() - stime > 3600): return 1
    #persents.sort(key=GetKey)

start = time.time()
data = read_from_file("1")
to_words(words, data)
humanisation(words, hum)
print(len(words))
persents_calc(words, hum, persents, start)
finish = time.time()
print(finish-start)

f = open("outpu", "w")
for i in persents:
    f.write(str(i))
    f.write("\n")

