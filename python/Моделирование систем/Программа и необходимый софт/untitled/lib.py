# -*- coding: utf-8 -*-
__author__ = 'Антон'

import math
import random
from sets import Set

def modeling(t_end, X1, buffers, nodes, working, repear, positive, negative, destroyed, statistic):
    """

    :param t_end:
    :param X1:
    :param buffers:
    :param nodes:
    :param working:
    :param repear:
    :param positive:
    :param negative:
    :param destroyed:
    :param statistic:
    """
    test = 0
    optimisation = Set([])
    t = 0 # модельное дискретное время
    t_temp = int(round(random.expovariate(0.02)))
    if t_temp == 0: t_temp += 1
    t_next = t + t_temp # время следующего появления заявки
    optimisation.add(t_next)
    # создаю объекты для хранения данных
    for i in range(X1):
        buffers.append([])
        nodes.append([])
        a = [1, int(round(random.expovariate(0.001)))]
        optimisation.add(a[1])
        working.append(a)
        repear.append([])

    while t < t_end:
        # генерируем заявку
        if (t == t_next):
            order = []
            order.append(t) # время появления заявки [0]
            order.append(random.randint(100, 300)) # информационный объем заявки [1]
            order.append(random.randint(0, X1 - 1)) # номер узла для заявки [2]

            # отправляем ее на обслуживание
            # if (len(buffers[order[2]]) < 10) and (working[order[2]][0] == 1): #если в буфере есть место, то вставляем туда заявку
            c = 0
            while c == 0:
                if working[order[2]][0] == 1:
                    if len(buffers[order[2]]) < 10:
                        buffers[order[2]].append(order)
                        c = 1
                    else:
                        #negative.append(order) # отправляем заявку в необслуженные
                        statistic['neg'] += 1
                        c = 1
                else:
                    q = 0
                    for i in working: q += i[0]
                    if q:
                        order[2] = random.randint(0, X1 - 1) # номер узла для заявки [2]
                    else:
                        #negative.append(order) # отправляем заявку в необслуженные
                        statistic['neg'] += 1
                        c = 1


            t_temp = int(round(random.expovariate(0.02)))
            if t_temp == 0: t_temp += 1
            t_next = t + t_temp# пересчитываем следущее время появления заявки
            optimisation.add(t_next)
            statistic['generated'] += 1

        # обслуживаем заявки
        for i in range(X1):
            if nodes[i] != []:
                if nodes[i][3] == t:
                    nodes[i].append(i)
                    #positive.append(nodes[i])
                    statistic['pos'] += 1
                    nodes[i] = []

        # берем заявки на обслуживание
        for i in range(X1):
            if (nodes[i] == []) and (buffers[i] != []):
                z = buffers[i].pop(0)
                #nodes[i] = z
                z.append(t + z[1])
                optimisation.add(t + z[1])
                #z.append(i)
                nodes[i] = z

        # ломаем устройства
        for i in range(X1):
            if (working[i][0] == 1) and (working[i][1] == t):
                z = working[i][:]

                repear[i].append(z)
                working[i][0] = 0
                a = t + int(round(random.normalvariate(500, 20)))
                optimisation.add(a)
                working[i][1] = a

                # теперь нужно перераспределить буфер узла и отправить в небытие обслуживаемую заявку
                statistic['crash'] += 1
                if nodes[i] != []:
                    nodes[i].append(t)
                    destroyed.append(nodes[i])# уничтожаем заявку в узле
                    nodes[i] = []

                while len(buffers[i]) > 0:
                    temp = [11, -1]
                    for q in range(X1):
                        if (working[q][0] == 1) and (len(buffers[q]) < temp[0]):
                            temp = [len(buffers[q]), q]
                    if (temp[1] != -1) and (len(buffers[temp[1]]) < 10):
                        buffers[temp[1]].append(buffers[i].pop(0))
                    else:
                        buffers[i].pop(0)
                        #negative.append(buffers[i].pop(0)) # немного непонятно куда отправлять те заявки которые некуда перераспределить
                        statistic['neg'] += 1
        # чиним устройства
        for i in range(X1):
            if (working[i][0] == 0) and (working[i][1] == t):
                z = working[i][:]
                repear[i].append(z)
                working[i][0] = 1
                a = t + int(round(math.fabs(random.expovariate(0.001))))
                optimisation.add(a)
                working[i][1] = a
                statistic['rep'] += 1

        # увеличиваем дискретное время
        #t += 1

        t = min(optimisation)
        optimisation.remove(min(optimisation))

#-----------------------------------------------------------------------------------------------------------------------




