# -*- coding: utf-8 -*-
__author__ = 'Антон'

from lib import modeling
from PyQt4 import QtCore, QtGui
import matplotlib.pyplot as plt

#X1 = 10# количество буферов, узлов

buffers = [] # массих буферов для устройств
nodes = [] # массив узлов/одноканальных устройств
working = [] # работоспособность устройства
repear = [] # журналы поломок и починок
positive = [] # обслуженные заявки, вне зависимости от того переходили ли они из буфера одного узла в другой
# записан как обслуживший узел будет, тот в чьем буфере заявка стояла перед окончательным обслуживанием
negative = [] # звявки которые не были обслужены по причине переполнености узла
destroyed = [] # заявки которые не удалось обслужить из-за поломки устройства

statistic = {'generated': 0, 'crash': 0, 'rep': 0, 'pos': 0, "neg": 0}

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(583, 398)
        Dialog.setAcceptDrops(False)
        self.X1 = QtGui.QSpinBox(Dialog)
        self.X1.setGeometry(QtCore.QRect(40, 20, 61, 31))
        self.X1.setMinimum(1)
        self.X1.setObjectName(_fromUtf8("X1"))
        self.time = QtGui.QSpinBox(Dialog)
        self.time.setGeometry(QtCore.QRect(110, 20, 61, 31))
        self.time.setMinimum(500)
        self.time.setMaximum(100000000)
        self.inter = QtGui.QSpinBox(Dialog)
        self.inter.setGeometry(QtCore.QRect(191, 20, 61, 31))
        self.inter.setMinimum(25)
        self.inter.setMaximum(50)
        self.time.setObjectName(_fromUtf8("time"))
        self.plainTextEdit = QtGui.QPlainTextEdit(Dialog)
        self.plainTextEdit.setGeometry(QtCore.QRect(40, 70, 261, 301))
        self.plainTextEdit.setObjectName(_fromUtf8("plainTextEdit"))
        self.pushButton = QtGui.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(330, 70, 101, 41))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(Dialog)
        self.pushButton_2.setGeometry(QtCore.QRect(330, 130, 131, 41))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        #self.pushButton_3 = QtGui.QPushButton(Dialog)
        #self.pushButton_3.setGeometry(QtCore.QRect(330, 190, 131, 41))
        #self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        def qqmodeling():
            buffers[:] = [] # массих буферов для устройств
            nodes[:] = [] # массив узлов/одноканальных устройств
            working[:] = [] # работоспособность устройства
            repear[:] = [] # журналы поломок и починок
            positive[
            :] = [] # обслуженные заявки, вне зависимости от того переходили ли они из буфера одного узла в другой
            # записан как обслуживший узел будет, тот в чьем буфере заявка стояла перед окончательным обслуживанием
            negative[:] = [] # звявки которые не были обслужены по причине переполнености узла
            destroyed[:] = [] # заявки которые не удалось обслужить из-за поломки устройства
            statistic = {'generated': 0, 'crash': 0, 'rep': 0, 'pos': 0, "neg": 0}

            modeling(self.time.value(), self.X1.value(), buffers, nodes, working, repear, positive, negative, destroyed,
                     statistic)

            s = '' # строка отчета
            buf = 0
            for i in range(self.X1.value()):
                buf += len(buffers[i])
                if nodes[i] != []: buf += 1
            s += u'осталось в буферах ' + str(buf) + '\n'
            s += u'удачно ' + str(statistic['pos']) + '\n'
            s += u'неудачно ' + str(statistic['neg']) + '\n'
            s += u'уничтожены ' + str(len(destroyed)) + '\n'
            s += u'сгенерировано транзактов ' + str(statistic['generated']) + '\n'
            s += u'поломок ' + str(statistic['crash']) + '\n'
            s += u'починок ' + str(statistic['rep']) + '\n'
            self.plainTextEdit.setPlainText(s)

            temp = []
            for i in range(len(repear)):
                for q in range(len(repear[i])):
                    if repear[i][q][0] == 1:
                        temp.append(repear[i][q][1])
            temp.append(0)
            temp.sort()

            temp2 = []
            for i in range(1, len(temp)):
                temp2.append(temp[i]-temp[i-1])
            #print 'temp ', len(temp), "  ", temp
            temp2.sort()
            #print 'temp2 ', len(temp2), "  ", temp2

            x = range(max(temp2)+1)
            y = []
            for i in range(len(x)):
                y.append(0)
            for i in range(len(temp2)):
                y[temp2[i]] += 1

            for i in range(len(y)):
                y[i] /= float(len(temp2))
            M = 0
            D = 0
            for i in range(len(temp2)):
                M += temp2[i]
            M = float(M)/len(temp2)
            print M
            for i in range(len(temp2)):
                D += (temp2[i] - M)**2
            D = float(D)/(len(temp2) - 1)
            print D
            plt.bar(x, y, color='blue')
            plt.show()






            #дублируем отчет в консоль
            #print  u'осталось в буферах', buf
            #print u'удачно ', len(positive)
            #print u'неудачно ', len(negative)
            #print u'уничтожены ', len(destroyed)
            ##print  u'поломки и починки \n', repear
            #print u'сгенерировано транзактов ', statistic['generated']
            ##print  u'массив времен выхода следующего транзакта \n', time
            #print u'поломок ', statistic['crash']
            #print u'починок ', statistic['rep']
            #print destroyed


        def plot():
            intervals = self.inter.value()
            width = (self.time.value() // intervals - 1) // 2
            x = range(0, self.time.value(), self.time.value() // intervals)
            x1 = [i + width for i in x]

            y = []
            y1 = []
            for i in range(len(x)):
                y.append(0)
                y1.append(0)

            temp = 0
            for i in range(len(repear)):
                for q in range(len(repear[i])):
                    if repear[i][q][0] == 1:
                        y[repear[i][q][1] // (self.time.value() // intervals)] += 1
                        temp += 1
            for i in range(intervals):
                y[i] /= float(temp)
            for i in range(len(destroyed)):
                y1[destroyed[i][4] // (self.time.value() // intervals)] += 1
            for i in range(intervals):
                y1[i] /= float(len(destroyed))

            plt.bar(x, y, width, color='red')
            plt.bar(x1, y1, width, color='blue')
            #plt.xticks(x)
            plt.show()


        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL("clicked()"), qqmodeling)
        QtCore.QObject.connect(self.pushButton_2, QtCore.SIGNAL("clicked()"), plot)
        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.pushButton.setText(_translate("Dialog", "Start", None))
        self.pushButton_2.setText(_translate("Dialog", "Постороить график", None))
        #self.pushButton_3.setText(_translate("Dialog", "Сохранить данные", None))


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())