__author__ = 'faustkun'
data = list([])
for i in (range(1, 1000000)):
#for i in {31}:
    num = i
    string = str(num)
    #print(num, end="")
    while num != 1:
        string += "-"
        #print("-", end="")
        if not (num & 1):
            num /= 2
            num = int(num)
        else:
            num *= 3
            num += 1
        string += str(num)
        #print(num, end="")
    #print(string)
    data.append(string)
    if not i % 10: print(i)

f1 = open("data.txt", "w")
for a in data:
    f1.write(a)
    f1.write("\n")
f1.close()
