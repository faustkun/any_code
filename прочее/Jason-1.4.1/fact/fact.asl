a(10).
b(15).
c(Z):-a(X)&b(Y)&Z=X+Y.
!start.
+!start <- ?a(X);
       ?b(Y);
       ?c(Z);
       .print("Summa = ",X+Y+Z).

