beginState(state(  0,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,   0,     0,    0,  0,  0)).
//		   state(Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok)

!start.

+!start <-  //.create_agent(state, "state.asl");
			//.wait(1);
			?beginState(X);
			.send(state, tell, X);
			.send(state, achieve, continue).

			/*+!printm: list2(X) & X == []<- 
					?list(Y);
					-+list2(Y);
					!printm.

			+!printm: list2([H | T]) & H\==[]<- 
						.print(H);
						-+list2(T);
						!printm.
					
*/			
			
@a11[atomic]
+!checkState([A|H]): A==1 <- .print("������ ������!").

@a12[atomic]
+!checkState([A|H]): A==0 <-//.print("�� ������"); 
							.send(state, achieve, continue).

//������� ���������������
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Korg == 1 & Zv==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, 1, Zr, Zu, Spr, Snark, Snat, Os, Ok)));
	.print("������� �������������� � ������").
	
	
//������������ �����
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Ts == 1 & Ng==1 & Nu==1 & Lu == 1 & Gg == 0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, 1, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok)));
	.print("���� ����������").

//������������ ����
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Dv == 1 & Dg==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, 1, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok)));
	.print("���� �����������").

//������������ �������
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Gg == 1 & Dg==1 & Gs ==1 & Korg == 0<-
	.send(state, achieve, changeState(state(Git, 1, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok)));
	.print("������ ����������").

//������������ ��������
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Nv == 1 & Ps==1 & Ng==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, Gg, Gs, Ts, 1, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok)));
	.print("�������� �����������").

//������������ �����
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Korg == 1 & Spr==1 & Ku==1 & Snark==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, 1, Snat, Os, Ok)));
	.print("������ ���������").

//����������� �����
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Korg == 1 & Spr==1 & Ku==1 & Snark==1 & Snat==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, 1, Os, Ok)));
	.print("������ ��������").

//������������ �����
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Korg == 1 & Os==1 & Zu==1 & Spr==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, 1, Snark, Snat, Os, Ok)));
	.print("������ ���������").

//������� ������
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Korg == 1 & Snat==1 & Zu==1 & Ku==1 & Git==0<-
	.send(state, achieve, changeState(state(1, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok)));
	.print("������ �������").

//���������� ����� � �����
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Dg == 1 & Gg==1 & Gs==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, Gg, 1, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok)));
	.print("���� � ����� ���������").

//��������� ���������������
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Zv == 1 & Zr==1 & Korg==1 & Zu==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, 1, Spr, Snark, Snat, Os, Ok)));
	.print("�������������� �����������").

//��������� �����
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Lo == 1 & Ng==1 & Lu==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, 1, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok)));
	.print("���� �����������").

//��������� ��������
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Lu == 1 & Ts==1 & Nu==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, 1, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok)));
	.print("�������� �����������").

//��������� ������
+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok]):Ok == 1 & Korg==1 & Ku==0<-
	.send(state, achieve, changeState(state(Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, 1, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok)));
	.print("����� �����������").

+!serveMe([Git, Korg, Dv, Dg, Gg, Gs, Ts, Ng, Nu, Nv, Ps, Lu, Lo, Ku, Zv, Zr, Zu, Spr, Snark, Snat, Os, Ok])<-
	.send(state, achieve, continue).
	

+!_.
