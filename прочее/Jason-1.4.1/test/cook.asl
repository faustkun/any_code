timeOfProduce(3000).
!produceMeals.

@cc1
+!produceMeals <-
       ?timeOfProduce(TOP);
 .wait(TOP);
.send(store, achieve, takeNewMeal);
.send(store, askOne, mealsInStore(Rest), mealsInStore(Rest));
!setProduceSpeed(Rest);
!!produceMeals.

@cc2
 +!setProduceSpeed(Rest): Rest >5 <-
          -+timeOfProduce(3000).

@cc3
+!setProduceSpeed(Rest): Rest<=5 <-
              -+timeOfProduce(300).

@cc4
+!who_is_last<- true.

