sellers([seller1, seller2, seller3]).
!toPutOnQueue. 

@c1
+!toPutOnQueue <-
              .wait(200);
.print("Hello! Who is last?");
.broadcast(achieve, who_is_last).

@c2[atomic]
+!who_is_last[source(Customer)]:    
.all_names(A) & .substring(Customer, A) & inFront(_) 
& not behind(_) <-
       ?numberInQueue(N);
       .my_name(MyName);
       .print("I last, ",Customer);
       .send(Customer,tell,queue(MyName,N)).
	   
@c3
+!who_is_last <- true.

@c4[atomic]
+queue(Agent,N): .count(queue(_,_),3) <-
           !selectQueue(LastInQueue);
           !setInQueue(LastInQueue);
           .abolish(queue(_,_)).

@c5[atomic]
+!selectQueue(LastInQueue) <-
    .findall(N,queue(_,N),List);
             .min(List,MinQueueSize);
             ?queue(LastInQueue,MinQueueSize).

@c6[atomic]
+!setInQueue(LastInQueue):sellers(S)&  .member(LastInQueue, S)  <-
+inFront(LastInQueue);
.print("I make a queue to ",LastInQueue,".");
-+numberInQueue(1).

@c7[atomic]
+!setInQueue(LastInQueue):.all_names(A)&
.substring(LastInQueue,A)&sellers(S)& 
 not .member(LastInQueue, S) <-  
          .my_name(MyName);
?queue(LastInQueue,N);
          .print("I select a queue behind ",LastInQueue,". The queue size is ",N); 
           .send(LastInQueue,tell,behind(MyName));
           +inFront(LastInQueue);
            -+numberInQueue(N+1).

@c8[atomic]
+!setInQueue<-
            !toPutOnQueueOnceMore.

@c9[atomic]
+!toPutOnQueueOnceMore<- 
   .drop_all_desires;
   .print("OK, I ask once again: WHO IS LAST ???");
   .broadcast(achieve, who_is_last).

@c10[atomic]
+inFront(Agent): sellers(S)& .member(Agent, S) <-
             ?orderValue(X);
             .send(Agent,achieve,serveMe(X));
             .print("Now I must be served by ",Agent,
                                                          "! My order is ",X). 

@c11[atomic]
+!behind(X): .count(behind(_), 2) <-
      .print("No, ",X," behind me occupied");
      .send(X,achieve,toPutOnQueueOnceMore);
      .abolish(behind(X)).

@c12
+!behind(X)<-
.print("OK, ",X," you are behind me...").			

@c13[atomic]
+!next:behind(X) <-	
            ?numberInQueue(N);
            -+numberInQueue(N-1);
            .send(X,achieve,next).

@c14[atomic]
+!next:not behind(_) <- 	
              ?numberInQueue(N);
              -+numberInQueue(N-1).

@c15[atomic]
+!meal(X)[source(Seller)]: behind(Y) <-
         .print("I got meal from ", Seller,". Who is next !? (",Y,")");
       .send(Y,achieve,seller(Seller));
       .send(Y,achieve,next);
  .send(generator,achieve,finishMe). 

@c16[atomic]
+!meal(X)[source(Seller)]: not behind(Y) <-
       .print("  I got meal from ", Seller,". Our queue is empty !!!");
        .send(generator,achieve,finishMe).  

@c17[atomic]
+!seller(Seller) <-
-+inFront(Seller).


