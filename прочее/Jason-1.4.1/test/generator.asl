maxDelay(500).
maxAgentCounter(10).
agentCounter(0).
completeCounter(0).
sellers([seller1, seller2, seller3]). 
!start. 

@g1
+!start: agentCounter(AC) & maxAgentCounter(MAC) &      
AC<MAC <-
       -+agentCounter(AC+1);
     ?maxDelay(MD);
?agentCounter(C);
.wait(math.round(math.random(MD)));
.concat("customer(",C,")",Name);  
.create_agent(Name, "customer.asl"); 
V=math.round(math.random(3))+1;   
.send(Name, tell, orderValue(V));
!!start.

@g2
+!start <-true.

@g3[atomic]
+!finishMe[source(Agent)] <- 
                    ?completeCounter(C);
                     -+completeCounter(C+1);
                     .kill_agent(Agent).

@g4[atomic]
+!completeCounter(C): maxAgentCounter(X)& X==C <-
 .print("SIMULATION COMPLETE");
      ?sellers(S);
      for (.member(Y,S)) 
          { .send(Y, askOne, salesProceeds(Z),   salesProceeds(Z));
    .print("The  sales proceeds of ",Y," is ",Z)
            }.

@g5
+!who_is_last<-true.


