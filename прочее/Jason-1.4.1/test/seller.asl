queue(0).
salesProceeds(0).
maxServeDelay(2000).

@s1
+!who_is_last[source(Customer)]: queue(0) <-  
             .my_name(MyName);
             .print("I HAVE NO QUEUE, ",Customer);
             .send(Customer, tell, queue(MyName,0)).

@s2
+!who_is_last<-true.


@s3
+!serveMe(X)[source(Customer)]: not capturedBy(_) <- 
          +capturedBy(Customer);
           .print("  I'm captured by ", Customer);
      .abolish(queue(0));
      ?maxServeDelay(Delay);
      .wait(Delay);
       .send(store, achieve, moreMeal(X)).

@s4
+!serveMe(X)[source(Customer)]: capturedBy(_) <- 
                 .print("I am busy, ",Customer," go to queue...");     
   .send(Customer, achieve, toPutOnQueueOnceMore).            

@s5
+!takeMeal(X) <- 
                 ?capturedBy(Customer);
   ?salesProceeds(Y);         
   K=Y+X*10;             
         -+salesProceeds(K);  
              .send(Customer, tell, meal(X));
               .abolish(capturedBy(_)).

