mealsInStore(6).
@st1
+!moreMeal(X)[source (Seller)]:
    mealsInStore(MIS) & MIS>=X<-
           -+mealsInStore(MIS-X);
           .print("  There are ", MIS-X, " meals in storage");
           .wait(1000);
           .send(Seller, achieve, takeMeal(X)).  

@st2
+!moreMeal(X)[source (Seller)]: 
          mealsInStore(MIS) & MIS<X<-  
         .wait(1000);
      !moreMeal(X)[source (Seller)]. 

@st3
+!takeNewMeal <- 
            ?mealsInStore(MIS);
           -+mealsInStore(MIS+1).

@st4
+!who_is_last<-true.

